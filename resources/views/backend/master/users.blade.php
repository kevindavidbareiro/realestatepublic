@extends('layouts.header')
@section('content')
	<div class="m-grid__item m-grid__item--fluid m-wrapper">
		<div class="m-portlet m-portlet--mobile">
			<div class="m-portlet__head">
				<div class="m-portlet__head-caption">
					<div class="m-portlet__head-title">
						<h3 class="m-portlet__head-text">
							<i class="la flaticon-user-settings"></i>
							Usuarios
						</h3>
					</div>
				</div>
			</div>
			<div class="m-portlet__body">
				<div class="m-form m-form--label-align-right m--margin-top-20 m--margin-bottom-30">
					<div class="row align-items-center">
						<div class="col-xl-8 order-2 order-xl-1">
							<div class="form-group m-form__group row align-items-center">
								<div class="col-md-4">
									<div class="m-input-icon m-input-icon--left">
										<input type="text" class="form-control m-input m-input--solid" placeholder="Buscar..." id="generalSearch">
										<span class="m-input-icon__icon m-input-icon__icon--left">
										<span>
											<i class="la la-search"></i>
										</span>
										</span>
									</div>
								</div>
							</div>
						</div>
						<div class="col-xl-4 order-1 order-xl-2 m--align-right">
							<a  class="btn btn-accent m-btn m-btn--custom m-btn--icon m-btn--air m-btn--pill" data-toggle="modal" onclick="nuevoUsuario()" data-target="#nuevo_usuario_modal">
								<span>
									<i class="la flaticon-user-settings"></i>
									<span >
										Nuevo
									</span>
								</span>
							</a>
							<div class="m-separator m-separator--dashed d-xl-none"></div>
						</div>
					</div>
				</div>
				<div class="m-section__content">
					<table class="table table-inverse" id="tblUsuario">
						<thead>
							<tr>
								<th>
									#
								</th>
								<th>
									Usuario
								</th>
								<th>
									Email
								</th>
								<th>
									Fecha creación
								</th>
								<th>
									<center>Acción</center>
								</th>
							</tr>
						</thead>
						<tbody id="listadoUser">
							
						</tbody>
					</table>
				</div>
				<!--editar usuario-->
				<div class="modal fade" id="editar_usuario_modal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
					<div class="modal-dialog" role="document">
						<div class="modal-content">
							<div class="modal-header">
								<h5 class="modal-title" id="exampleModalLabel">
									Editar usuario
								</h5>
								<button type="button" class="close" data-dismiss="modal" aria-label="Close">
									<span aria-hidden="true">
										&times;
									</span>
								</button>
							</div>
							<div class="modal-body">
								<form action="">
									<div class="form-group m-form__group row">
										<label for="example-text-input" class="col-2 col-form-label">
											Usuario
										</label>
										<div class="col-10">
											<input class="form-control m-input" type="text" value="" id="usuarioNombreEditar">
										</div>
									</div>
									<div class="form-group m-form__group row">
										<label for="example-text-input" class="col-2 col-form-label">
											Email
										</label>
										<div class="col-10">
											<input class="form-control m-input" type="text" value="" id="usuarioEmailEditar">
										</div>
									</div>
									<div class="form-group m-form__group row">
										<label for="example-text-input" class="col-2 col-form-label">
											Password
										</label>
										<div class="col-10">
											<input class="form-control m-input" type="password" value="" id="usuarioPasswordEditar" placeholder="Nueva...">
										</div>
									</div>
								</form>
							</div>
							<div id="mensajeActualizar"></div>
							<div class="modal-footer">
								<button type="button" class="btn btn-secondary" data-dismiss="modal">
									Salir
								</button>
								<button type="button" class="btn btn-primary" onclick="guardarEditarUsuario()">
									Guardar cambios								
								</button>
							</div>
						</div>
					</div>
				</div>
				<!--editar usuario-->
				<!--editar usuario-->
				<div class="modal fade" id="nuevo_usuario_modal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
					<div class="modal-dialog" role="document">
						<div class="modal-content">
							<div class="modal-header">
								<h5 class="modal-title" id="exampleModalLabel">
									Nuevo usuario
								</h5>
								<button type="button" class="close" data-dismiss="modal" aria-label="Close">
									<span aria-hidden="true">
										&times;
									</span>
								</button>
							</div>
							<div class="modal-body">
								<form action="">
									<div class="form-group m-form__group row">
										<label for="example-text-input" class="col-2 col-form-label">
											Usuario
										</label>
										<div class="col-10">
											<input class="form-control m-input" type="text" value="" id="usuario">
										</div>
									</div>
									<div class="form-group m-form__group row">
										<label for="example-text-input" class="col-2 col-form-label">
											Email
										</label>
										<div class="col-10">
											<input class="form-control m-input" type="text" value="" id="email">
										</div>
									</div>
									<div class="form-group m-form__group row">
										<label for="example-text-input" class="col-2 col-form-label">
											Rol
										</label>
										<div class="col-10">
											<select class="form-control m-input" id="roles">
												<option value="0">Elegir rol</option>
											</select>
										</div>
									</div>
									<div class="form-group m-form__group row">
										<label for="example-text-input" class="col-2 col-form-label">
											Password
										</label>
										<div class="col-10">
											<input class="form-control m-input" type="password" value="" id="usuarioPasswordNuevo" placeholder="">
										</div>
									</div>
								</form>
							</div>
							<div id="mensajeNuevo"></div>
							<div class="modal-footer">
								<button type="button" class="btn btn-secondary" data-dismiss="modal">
									Salir
								</button>
								<button type="button" class="btn btn-primary" onclick="guardarNuevoUsuario()">
									Guardar cambios								
								</button>
							</div>
						</div>
					</div>
				</div>
				<!--editar usuario-->
			</div>
		</div>
	</div>
@endsection
@section('script')

<script>
	//datos para usuarios
	var idUsuario = "";
	var usuario = "";
	var emailUsuario ="";
	var password ="";

	function editar(idUser,name,email){
		$('#usuarioPasswordEditar').val('');
		$('#mensajeActualizar').html('');
		idUsuario = idUser;
		usuario = $('#usuarioNombreEditar').val(name);
	    emailUsuario   = $('#usuarioEmailEditar').val(email);
		password     = "";
	}

	function nuevoUsuario(){
		$('#usuario').val('');
		$('#email').val('');
		$('#usuarioPasswordNuevo').val('');
		$('#mensajeNuevo').html('');
	}

	function getRolesUser(){
		$.ajax({
			type: "GET",
			url: "{{ route('system.roles') }}",
			data: {
				"_token": "{{ csrf_token() }}",
			},
			success: function (data) {
				//console.log(data);
				$.each(data,function(key, rol) {
        			$("#roles").append('<option value='+rol.id+'>'+rol.name+'</option>');
      			});
			},
			error: function (data) {
				console.log(data);
			}
		});	
	}

	function getUsers(){
		$.ajax({
			type: "GET",
			url: "{{ route('system.users') }}",
			data: {
				"_token": "{{ csrf_token() }}",
			},
			success: function (data) {
				
				$("#listadoUser").html("");
				$.each(data,function(key, user) {
					var onclick = 'editar("'+user.id+'","'+user.name+'","'+user.email+'")';
        			$("#listadoUser").append(
							'<tr>'+
									'<th scope="row">'+
										user.id
									+'</th>'
									+'<td>'+
										user.name
									+'</td>'
									+'<td>'+
										user.email
									+'</td>'
									+'<td>'+
										user.created_at
									+'</td>'
									+'<td>'
										+"<center><a id='editarUsuario'  onclick='"+onclick+" '"+ 
											+'style="cursor:pointer"'+
											+'data-name="'+user.name+'"'+ 
											+'class="m-portlet__nav-link btn m-btn m-btn--hover-accent m-btn--icon m-btn--icon-only m-btn--pill btnUpdateUser"' 
											+'title="Editar "'
											+'data-toggle="modal"' 
											+'data-target="#editar_usuario_modal">'
											+'<i class="la la-edit"></i>'
										+'</a></center>'
									+'</td>'
								+'</tr>');
      			});
			},
			error: function (data) {
				console.log(data);
			}
		});	
	}


	function guardarEditarUsuario(){
		
		var usuario_editar       = usuario[0].value; 
		var usuario_email_editar = emailUsuario[0].value;
		password =  $('#usuarioPasswordEditar').val();

		if(password.length <6){
			alert("La password debe ser mayor a 6 caracteres");
		}else{
			$.ajax({
				type: "PUT",
				url: "{{ route('system.update') }}",
				data: {
					"_token": "{{ csrf_token() }}",
					"_method": 'put',
					"id" :idUsuario,
					"user":usuario_editar,
					"email":usuario_email_editar,
					"password":password
				},
				success: function (data) {
					if(data.status == 'ok'){
						$('#mensajeActualizar').html(
							'<div class="alert alert-success" role="alert" style="margin-left:5px;margin-right:5px">'+
								'<strong>'+
									'Mensaje :'+ 
								'</strong>'+
								'El usuario se actualizo correctamente.'+
						'</div>');
					}
				},
				error: function (data) {
					console.log(data);
				}
			});	
		}
	}

	function guardarNuevoUsuario(){
		var user     = $('#usuario').val();
		var email    = $('#email').val();
		var password = $('#usuarioPasswordNuevo').val();
		var rol      = $("#roles").val();

		if(rol == '0'){
			alert("Debes asignar un rol al nuevo usuario");
		}else{
			$.ajax({
				type: "POST",
				url: "{{ route('system.users.save') }}",
				data: {
					"_token": "{{ csrf_token() }}",
					"_method": 'post',
					"user":user,
					"email":email,
					"password":password,
					"rol_id":rol
				},
				success: function (data) {
					if(data.status == 'ok'){
						$('#mensajeNuevo').html(
							'<div class="alert alert-success" role="alert" style="margin-left:5px;margin-right:5px">'+
								'<strong>'+
									'Mensaje :'+ 
								'</strong>'+
								'El usuario se actualizo correctamente.'+
						'</div>');
						getUsers();
					}
				},
				error: function (data) {
					console.log(data);
				}
			});	
		}
	}


	getRolesUser();
	getUsers();

</script>
@endsection
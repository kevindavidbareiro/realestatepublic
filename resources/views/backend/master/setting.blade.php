@extends('layouts.header')
@section('content')
	<div class="m-grid__item m-grid__item--fluid m-wrapper">
		<div class="m-subheader ">
			<div class="d-flex align-items-center">
				<div class="mr-auto">
					<h3 class="m-subheader__title m-subheader__title--separator">
						Configuración
					</h3>
				</div>
			</div>
		</div>
		<div class="m-content">
			<div class="row">
				<div class="col-xl-6">
					<div class="m-portlet">
						<div class="m-portlet__head">
							<div class="m-portlet__head-caption" style="width:500px;">
								<div class="m-portlet__head-title">
									<h3 class="m-portlet__head-text">
										Barrio
									</h3>
									<div style="display:none;margin-left:10px;margin-top:15px;width:380px;" id="buscadorBarrio">
										<input class="form-control m-input" placeholder="Buscar barrio" type="text" value=""  onkeypress="if(event.key === 'Enter') searchBarrio()" id="barrioBusqueda">
									</div>	
								</div>
							</div>
							<div class="m-portlet__head-tools">
								<ul class="nav nav-pills nav-pills--brand m-nav-pills--align-right m-nav-pills--btn-pill m-nav-pills--btn-sm" role="tablist">
									<li class="nav-item m-tabs__item" style="margin-right:10px;">
										<a onclick="showBuscadorBarrio()"  class="btn btn-outline-info m-btn m-btn--icon btn-lg m-btn--icon-only m-btn--pill m-btn--air">
											<i class="la la-search"></i>
										</a>	
									</li>
									<li class="nav-item m-tabs__item">
										<button type="button" class="btn btn-outline-info m-btn m-btn--custom" onclick="nuevoBarrio()" data-toggle="modal"  data-target="#nuevo_barrio_modal">
											Nuevo 
										</button>
									</li>
								</ul>
							</div>
						</div>	
						<strong style="color:green;margin-left:10px;margin-top:10px;" id="loadingBusquedaBarrio">Buscando...</strong>
						<div class="m-portlet__head" id="mensajeBusquedaNoBarrio" style="display:none;">
							<div style="margin-top:10px;" class="m-alert m-alert--outline m-alert--square m-alert--outline-2x alert alert-danger alert-dismissible fade show" role="alert">
								<button type="button" class="close"  onclick="closeBusquedaBarrio()"></button>
								<div id="mensajeNoEncontroBarrio"></div>
							</div>
						</div>
						<div class="m-portlet__head" id="mensajeBusquedaEncontroBarrio" style="display:none;">
							<div style="margin-top:10px;" class="m-alert m-alert--outline m-alert--square m-alert--outline-2x alert alert-success alert-dismissible fade show" role="alert" >
								<button type="button" class="close"  onclick="closeBusquedaBarrio()"></button>
								<ul id="mensajeEncontroBarrio"></ul>
							</div>
						</div>
						<div class="m-portlet__body">
							<!--begin::Section-->
							<div class="m-section__content">
								<table class="table">
									<thead>
										<tr>
											<th>
												ID
											</th>
											<th>
												Barrio
											</th>
											<th>
												Localidad
											</th>
											<th>
												<center>Accion</center>
											</th>
										</tr>
									</thead>
									<tbody id="listadoBarrio">
									</tbody>
								</table>			
							</div>
						</div>
					</div>
				</div>
				<div class="col-xl-6">
					<div class="m-portlet">
						<div class="m-portlet__head">
							<div class="m-portlet__head-caption">
								<div class="m-portlet__head-title">
									<h3 class="m-portlet__head-text">
										Localidades
									</h3>
									<div style="display:none;margin-left:10px;margin-top:15px;width:380px;" id="buscadorLocalidad">
										<input class="form-control m-input" placeholder="Buscar localidad" type="text" value=""  onkeypress="if(event.key === 'Enter') searchLocalidad()" id="localidadBusqueda">
									</div>
								</div>
							</div>
							<div class="m-portlet__head-tools">
								<ul class="nav nav-pills nav-pills--brand m-nav-pills--align-right m-nav-pills--btn-pill m-nav-pills--btn-sm" role="tablist">
									<li class="nav-item m-tabs__item" style="margin-right:10px;margin-top:9px;">
										<a onclick="showBuscadorLocalidad()"  class="btn btn-outline-info m-btn m-btn--icon btn-lg m-btn--icon-only m-btn--pill m-btn--air">
											<i class="la la-search"></i>
										</a>	
									</li>
									<li class="nav-item m-tabs__item">
										<button type="button" class="btn btn-outline-info m-btn m-btn--custom" style="float:right;margin-top:10px;" onclick="nuevaLocalidad()" data-toggle="modal"  data-target="#nuevo_localidad_modal">
											Nuevo 
										</button>
									</li>
								</ul>
							</div>
						</div>
						<strong style="color:green;margin-left:10px;margin-top:10px;" id="loadingBusquedaLocalidad">Buscando...</strong>
						<div class="m-portlet__head" id="mensajeBusquedaNoLocalidad" style="display:none;">
							<div style="margin-top:10px;" class="m-alert m-alert--outline m-alert--square m-alert--outline-2x alert alert-danger alert-dismissible fade show" role="alert">
								<button type="button" class="close"  onclick="closeBusquedaLocalidad()"></button>
								<div id="mensajeNoEncontroLocalidad"></div>
							</div>
						</div>
						<div class="m-portlet__head" id="mensajeBusquedaEncontroLocalidad" style="display:none;">
							<div style="margin-top:10px;" class="m-alert m-alert--outline m-alert--square m-alert--outline-2x alert alert-success alert-dismissible fade show" role="alert" >
								<button type="button" class="close"  onclick="closeBusquedaLocalidad()"></button>
								<ul id="mensajeEncontroLocalidad"></ul>
							</div>
						</div>
						<div class="m-portlet__body">
								<!--begin::Section-->
							<div class="m-section__content">
								<table class="table">
									<thead>
										<tr>
											<th>
												ID
											</th>
											<th>
												Localidad
											</th>
											<th>
												Provincia
											</th>
											<th>
												<center>Accion</center>
											</th>
										</tr>
									</thead>
									<tbody id="listadoLocalities">
										
									</tbody>
								</table>			
							</div>
						</div>
					</div>
				</div>
				<div class="col-xl-6">
					<div class="m-portlet">
						<div class="m-portlet__head">
							<div class="m-portlet__head-caption">
								<div class="m-portlet__head-title">
									<h3 class="m-portlet__head-text">
									Paises
									</h3>
								</div>
							</div>
							<button type="button" class="btn btn-outline-info m-btn m-btn--custom" onclick="nuevoPais()" data-toggle="modal"  data-target="#nuevo_pais_modal" style="float:right;margin-top:10px;">
								Nuevo 
							</button>
						</div>
						<div class="m-portlet__body">
							<!--begin::Section-->
							<div class="m-section__content">
								<table class="table">
									<thead>
										<tr>
											<th>
												ID
											</th>
											<th>
												Pais
											</th>
										</tr>
									</thead>
									<tbody id="listadoPaises">
									</tbody>
								</table>		
							</div>
								<!--end::Section-->
						</div>
					</div>
				</div>
				<div class="col-xl-6">
					<div class="m-portlet">
						<div class="m-portlet__head">
							<div class="m-portlet__head-caption">
								<div class="m-portlet__head-title">
									<h3 class="m-portlet__head-text">
									Provincia
									</h3>
								</div>
							</div>
							<button type="button" class="btn btn-outline-info m-btn m-btn--custom"  data-toggle="modal" onclick="nuevoProvincia()" data-target="#nuevo_provincia_modal" style="float:right;margin-top:10px;">
								Nuevo
							</button>
						</div>
						<div class="m-portlet__body">
							<div class="m-section__content">
								<table class="table">
									<thead>
										<tr>
											<th>
												ID
											</th>
											<th>
												Pais
											</th>
											<th>
												Provincia
											</th>
											<th>
												<center>Accion</center>
											</th>
										</tr>
									</thead>
									<tbody id="listadoProvince">
										
									</tbody>
								</table>			
							</div>
						</div>
					</div>
				</div>
				<!--nuevo barrio-->
				<div class="modal fade" id="nuevo_barrio_modal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
					<div class="modal-dialog" role="document">
						<div class="modal-content">
							<div class="modal-header">
								<h5 class="modal-title" id="exampleModalLabel">
									Nuevo barrio
								</h5>
							</div>
							<div class="modal-body">
								<form action="">
									<div class="form-group m-form__group row">
										<label for="example-text-input" class="col-2 col-form-label">
											Localidad
										</label>
										<div class="col-10">
											<select class="form-control m-input" id="localidadListado">
												<option value="0">Elegir </option>
											</select>
										</div>
									</div>
									<div class="form-group m-form__group row">
										<label for="example-text-input" class="col-2 col-form-label">
											Barrio
										</label>
										<div class="col-10">
											<input class="form-control m-input" type="text" value="" id="barrio">
										</div>
									</div>
								</form>
							</div>
							<div id="mensajeNuevo"></div>
							<div class="modal-footer">
								<button type="button" class="btn btn-secondary" data-dismiss="modal">
									Salir
								</button>
								<button type="button" class="btn btn-primary" onclick="guardarBarrio('nuevo')">
									Guardar 								
								</button>
							</div>
						</div>
					</div>
				</div>
				<!--fin barrio-->
				<div class="modal fade" id="editar_barrio_modal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
					<div class="modal-dialog" role="document">
						<div class="modal-content">
							<div class="modal-header">
								<h5 class="modal-title" id="exampleModalLabel">
									Editar barrio
								</h5>
							</div>
							<div class="modal-body">
								<form action="">
									<div class="form-group m-form__group row">
										<label for="example-text-input" class="col-2 col-form-label">
											Actual
										</label>
										<div class="col-10">
											<div id="localidadActual" style="margin-top:10px;"></div>
										</div>
									</div>
									<div class="form-group m-form__group row">
										<label for="example-text-input" class="col-2 col-form-label">
											Cambiar localidad
										</label>
										<div class="col-10">
											<select class="form-control m-input" id="localidadListadoEditar">
												<option value="0">Elegir </option>
											</select>
										</div>
									</div>
									<div class="form-group m-form__group row">
										<label for="example-text-input" class="col-2 col-form-label">
											Barrio
										</label>
										<div class="col-10">
											<input class="form-control m-input" type="text" value="" id="barrioEditar">
										</div>
									</div>
								</form>
							</div>
							<div id="mensajeNuevo"></div>
							<div class="modal-footer">
								<button type="button" class="btn btn-secondary" data-dismiss="modal">
									Salir
								</button>
								<button type="button" class="btn btn-primary" onclick="guardarBarrio('editar')">
									Guardar 								
								</button>
							</div>
						</div>
					</div>
				</div>
				<!--Nuevo pais-->
				<div class="modal fade" id="nuevo_pais_modal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
					<div class="modal-dialog" role="document">
						<div class="modal-content">
							<div class="modal-header">
								<h5 class="modal-title" id="exampleModalLabel">
									Nuevo Pais
								</h5>
							</div>
							<div class="modal-body">
								<form action="">
									<div class="form-group m-form__group row">
										<label for="example-text-input" class="col-2 col-form-label">
											Pais
										</label>
										<div class="col-10">
											<input class="form-control m-input" type="text" value="" id="pais">
										</div>
									</div>
								</form>
							</div>
							<div id="mensajeNuevo"></div>
							<div class="modal-footer">
								<button type="button" class="btn btn-secondary" data-dismiss="modal">
									Salir
								</button>
								<button type="button" class="btn btn-primary" onclick="guardarNuevoPais()">
									Guardar pais								
								</button>
							</div>
						</div>
					</div>
				</div>
				<!--End Nuevo pais-->
				<!--Nuevo provincia-->
				<div class="modal fade" id="nuevo_provincia_modal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
					<div class="modal-dialog" role="document">
						<div class="modal-content">
							<div class="modal-header">
								<h5 class="modal-title" id="exampleModalLabel">
									Nuevo provincia
								</h5>
							</div>
							<div class="modal-body">
								<form action="">
									<div class="form-group m-form__group row">
										<label for="example-text-input" class="col-2 col-form-label">
											Pais
										</label>
										<div class="col-10">
											<select class="form-control m-input" id="paises">
												<option value="0">Elegir </option>
											</select>
										</div>
									</div>
									<div class="form-group m-form__group row">
										<label for="example-text-input" class="col-2 col-form-label">
											Provincia
										</label>
										<div class="col-10">
											<input class="form-control m-input" type="text" value="" id="provincia">
										</div>
									</div>
								</form>
							</div>
							<div id="mensajeNuevo"></div>
							<div class="modal-footer">
								<button type="button" class="btn btn-secondary" data-dismiss="modal">
									Salir
								</button>
								<button type="button" class="btn btn-primary" onclick="guardarProvincia('nuevo')">
									Guardar provincia								
								</button>
							</div>
						</div>
					</div>
				</div>
				<!--End Nuevo provincia-->
				<!--Nuevo provincia-->
				<div class="modal fade" id="nuevo_localidad_modal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
					<div class="modal-dialog" role="document">
						<div class="modal-content">
							<div class="modal-header">
								<h5 class="modal-title" id="exampleModalLabel">
									Nuevo localidad
								</h5>
							</div>
							<div class="modal-body">
								<form action="">
									<div class="form-group m-form__group row">
										<label for="example-text-input" class="col-2 col-form-label">
											Provincia
										</label>
										<div class="col-10">
											<select class="form-control m-input" id="provinciaLocalidad">
												<option value="0">Elegir </option>
											</select>
										</div>
									</div>
									<div class="form-group m-form__group row">
										<label for="example-text-input" class="col-2 col-form-label">
											Localidad
										</label>
										<div class="col-10">
											<input class="form-control m-input" type="text" value="" id="localidad">
										</div>
									</div>
								</form>
							</div>
							<div id="mensajeNuevo"></div>
							<div class="modal-footer">
								<button type="button" class="btn btn-secondary" data-dismiss="modal">
									Salir
								</button>
								<button type="button" class="btn btn-primary" onclick="guardarLocalidad('nuevo')">
									Guardar								
								</button>
							</div>
						</div>
					</div>
				</div>
				<!--End Nuevo provincia-->
				<div class="modal fade" id="eliminar_barrio_modal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
					<div class="modal-dialog" role="document">
						<div class="modal-content">
							<div class="modal-header">
								<h5 class="modal-title" id="exampleModalLabel">
									¿Estas seguro que deseas eliminar?
								</h5>
							</div>
							<div class="modal-body">
								<div id="mensajeEliminarBarrio"></div>
							</div>
							<div id="mensajeNuevo"></div>
							<div class="modal-footer">
								<button type="button" class="btn btn-secondary" data-dismiss="modal">
									Salir
								</button>
								<button type="button" class="btn btn-danger" onclick="eliminarBarrioData()">
									Eliminar								
								</button>
							</div>
						</div>
					</div>
				</div>
				<div class="modal fade" id="eliminar_localidad_modal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
					<div class="modal-dialog" role="document">
						<div class="modal-content">
							<div class="modal-header">
								<h5 class="modal-title" id="exampleModalLabel">
									¿Estas seguro que deseas eliminar?
								</h5>
							</div>
							<div class="modal-body">
								<div id="mensajeEliminarLocalidad"></div>
							</div>
							<div id="mensajeNuevo"></div>
								<div class="modal-footer">
									<button type="button" class="btn btn-secondary" data-dismiss="modal">
										Salir
									</button>
									<button type="button" class="btn btn-danger" onclick="eliminarLocalidadData()">
										Eliminar								
									</button>
								</div>
							</div>
						</div>
				</div>
				<div class="modal fade" id="eliminar_province_modal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
					<div class="modal-dialog" role="document">
						<div class="modal-content">
							<div class="modal-header">
								<h5 class="modal-title" id="exampleModalLabel">
									¿Estas seguro que deseas eliminar?
								</h5>
							</div>
							<div class="modal-body">
								<div id="mensajeEliminarProvince"></div>
							</div>
							<div id="mensajeNuevo"></div>
								<div class="modal-footer">
									<button type="button" class="btn btn-secondary" data-dismiss="modal">
										Salir
									</button>
									<button type="button" class="btn btn-danger" onclick="eliminarProvinceData()">
										Eliminar								
									</button>
								</div>
							</div>
						</div>
				</div>
				<!--editar localidad-->
				<div class="modal fade" id="editar_localidad_modal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
					<div class="modal-dialog" role="document">
						<div class="modal-content">
							<div class="modal-header">
								<h5 class="modal-title" id="exampleModalLabel">
									Editar localidad
								</h5>
							</div>
							<div class="modal-body">
								<form action="">
									<div class="form-group m-form__group row">
										<label for="example-text-input" class="col-2 col-form-label">
											Provincia
										</label>
										<div class="col-10">
											<div id="provinciaEditarActual" style="margin-top:10px;"></div>
										</div>
									</div>
									<div class="form-group m-form__group row">
										<label for="example-text-input" class="col-2 col-form-label">
											Cambiar provincia
										</label>
										<div class="col-10">
											<select class="form-control m-input" id="provinciaLocalidadEditar">
												<option value="0">Elegir </option>
											</select>
										</div>
									</div>
									<div class="form-group m-form__group row">
										<label for="example-text-input" class="col-2 col-form-label">
											Localidad
										</label>
										<div class="col-10">
											<input class="form-control m-input" type="text" value="" id="localidadEditar">
										</div>
									</div>
								</form>
							</div>
							<div id="mensajeNuevo"></div>
							<div class="modal-footer">
								<button type="button" class="btn btn-secondary" data-dismiss="modal">
									Salir
								</button>
								<button type="button" class="btn btn-primary" onclick="guardarLocalidad('editar')">
									Guardar 								
								</button>
							</div>
						</div>
					</div>
				</div>
				<!--fin editar localidad-->
				<div class="modal fade" id="editar_province_modal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
					<div class="modal-dialog" role="document">
						<div class="modal-content">
							<div class="modal-header">
								<h5 class="modal-title" id="exampleModalLabel">
									Editar provincia
								</h5>
							</div>
							<div class="modal-body">
								<form action="">
									<div class="form-group m-form__group row">
										<label for="example-text-input" class="col-2 col-form-label">
											Pais
										</label>
										<div class="col-10">
											<div id="paiseEditarActual" style="margin-top:10px;"></div>
										</div>
									</div>
									<div class="form-group m-form__group row">
										<label for="example-text-input" class="col-2 col-form-label">
											Cambiar pais
										</label>
										<div class="col-10">
											<select class="form-control m-input" id="paisesEditar">
												<option value="0">Elegir </option>
											</select>
										</div>
									</div>
									<div class="form-group m-form__group row">
										<label for="example-text-input" class="col-2 col-form-label">
											Provincia
										</label>
										<div class="col-10">
											<input class="form-control m-input" type="text" value="" id="provinciaEditar">
										</div>
									</div>
								</form>
							</div>
							<div id="mensajeNuevo"></div>
							<div class="modal-footer">
								<button type="button" class="btn btn-secondary" data-dismiss="modal">
									Salir
								</button>
								<button type="button" class="btn btn-primary" onclick="guardarProvincia('editar')">
									Guardar 								
								</button>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>	
	</div>
@endsection
@section('script')
<script>
	$('#inputBuscadorBarrio').hide();
	$('#loadingBusquedaBarrio').hide();
	$('#loadingBusquedaLocalidad').hide();
	var estado_buscador_barrio = false;
	var estado_buscador_localidad = false;
	//pais
	var pais_id_global='';
	//data barrios
	var barrio_id = '';
	var barrio = '';
	//datos localidad
	var localidad_id = '';
	var localidad_id_global = '';
	var localidad = '';

	//datos provincia
	var provincia_id_global = '';

	function getPaises(){
		$.ajax({
			type: "GET",
			url: "{{ route('system.country') }}",
			data: {
				"_token": "{{ csrf_token() }}",
			},
			success: function (data) {
				//console.log(data.country);
				$("#listadoPaises").html("");
				$.each(data,function(key, pais) {
					//console.log(pais);
					var onclick = 'editar("'+pais.id+'","'+pais.name+'")';
        			$("#listadoPaises").append(
						'<tr>'+
							'<th scope="row">'+
								pais.id
							+'</th>'
							+'<td>'+
								pais.name
							+'</td>'
						+'</tr>');
      			});
			},
			error: function (err) {
				console.log(err)
			}
		});
	}

	function getBarrios(){
		$.ajax({
			type: "GET",
			url: "{{ route('system.cities') }}",
			data: {
				"_token": "{{ csrf_token() }}",
			},
			success: function (data) {
				//console.log(data);
				$("#listadoBarrio").html("");
				$.each(data,function(key, barrio) {
					//console.log(pais);
					var onclickEditar = 'editarBarrio("'+barrio.id+'","'+barrio.name+'","'+barrio.locality_id+'","'+barrio.localities+'")';
					var onclickEliminar = 'eliminarBarrio("'+barrio.id+'","'+barrio.name+'")';
        			$("#listadoBarrio").append(
						'<tr>'+
							'<th scope="row">'+
								barrio.id
							+'</th>'
							+'<td>'+
								barrio.name
							+'</td>'
							+'<td>'+
								barrio.localities
							+'</td>'		
							+'<td>'
								+"<center>"
								+"<a id='editarUsuario'  onclick='"+onclickEditar+" '"+ 
									+'style="cursor:pointer"'+
									+'data-name="'+barrio.name+'"'+ 
									+'class="m-portlet__nav-link btn m-btn m-btn--hover-accent m-btn--icon m-btn--icon-only m-btn--pill btnUpdateUser"' 
									+'title="Editar "'
									+'data-toggle="modal"' 
									+'data-target="#editar_barrio_modal">'
									+'<i class="la la-edit"></i>'
								+'</a>'
								+"<a id='barrioEliminar'  onclick='"+onclickEliminar+" '"+ 
									+'style="cursor:pointer;color:"red"'+
									+'data-name="'+barrio.name+'"'+ 
									+'class="m-portlet__nav-link btn m-btn m-btn--hover-accent m-btn--icon m-btn--icon-only m-btn--pill btnUpdateUser"' 
									+'title="Editar "'
									+'data-toggle="modal"' 
									+'data-target="#eliminar_barrio_modal">'
									+'<i class="la la-close"></i>'
								+'</a>'
								+'</center>'
							+'</td>'
						+'</tr>');
      			});
			},
			error: function (err) {
				console.log(err)
			}
		});
	}

	function getProvince(){
		$.ajax({
			type: "GET",
			url: "{{ route('system.province') }}",
			data: {
				"_token": "{{ csrf_token() }}",
			},
			success: function (data) {
				//console.log(data);
				$("#listadoProvince").html("");
				$.each(data,function(key, province) {
					//console.log(pais);
					var onclickEditarProvince = 'editarProvince("'+province.id+'","'+province.name+'","'+province.country_id+'","'+province.countries+'")';
					var onclickEliminarProvince = 'eliminarProvince("'+province.id+'","'+province.name+'")';
        			$("#listadoProvince").append(
						'<tr>'+
							'<th scope="row">'+
								province.id
							+'</th>'
							+'<td>'+
								province.countries
							+'</td>'
							+'<td>'+
								province.name
							+'</td>'		
							+'<td>'
								+"<center>"
								+"<a id='editarUsuario'  onclick='"+onclickEditarProvince+" '"+ 
									+'style="cursor:pointer"'+
									+'data-name="'+province.name+'"'+ 
									+'class="m-portlet__nav-link btn m-btn m-btn--hover-accent m-btn--icon m-btn--icon-only m-btn--pill btnUpdateUser"' 
									+'title="Editar "'
									+'data-toggle="modal"' 
									+'data-target="#editar_province_modal">'
									+'<i class="la la-edit"></i>'
								+'</a>'
								+"<a id='barrioEliminar'  onclick='"+onclickEliminarProvince+" '"+ 
									+'style="cursor:pointer;color:"red"'+
									+'data-name="'+province.name+'"'+ 
									+'class="m-portlet__nav-link btn m-btn m-btn--hover-accent m-btn--icon m-btn--icon-only m-btn--pill btnUpdateUser"' 
									+'title="Editar "'
									+'data-toggle="modal"' 
									+'data-target="#eliminar_province_modal">'
									+'<i class="la la-close"></i>'
								+'</a>'
								+'</center>'
							+'</td>'
						+'</tr>');
      			});
			},
			error: function (err) {
				console.log(err)
			}
		});
	}
	function getLocalities(){
		$.ajax({
			type: "GET",
			url: "{{ route('system.localities') }}",
			data: {
				"_token": "{{ csrf_token() }}",
			},
			success: function (data) {
				//console.log(data);
				$("#listadoLocalities").html("");
				$.each(data,function(key, localities) {
					//console.log(pais);
					var onclickEditarLocalidad = 'editarLocalidad("'+localities.id+'","'+localities.name+'","'+localities.province_id+'","'+localities.province+'")';
					var onclickEliminarLocalidad = 'eliminarLocalidad("'+localities.id+'","'+localities.name+'","'+localities.province+'")';
        			$("#listadoLocalities").append(
						'<tr>'+
							'<th scope="row">'+
								localities.id
							+'</th>'
							+'<td>'+
								localities.name
							+'</td>'
							+'<td>'+
								localities.province
							+'</td>'		
							+'<td>'
								+"<center>"
								+"<a id='editarUsuario'  onclick='"+onclickEditarLocalidad+" '"+ 
									+'style="cursor:pointer"'+
									+'data-name="'+localities.name+'"'+ 
									+'class="m-portlet__nav-link btn m-btn m-btn--hover-accent m-btn--icon m-btn--icon-only m-btn--pill btnUpdateUser"' 
									+'title="Editar "'
									+'data-toggle="modal"' 
									+'data-target="#editar_localidad_modal">'
									+'<i class="la la-edit"></i>'
								+'</a>'
								+"<a id='barrioEliminar'  onclick='"+onclickEliminarLocalidad+" '"+ 
									+'style="cursor:pointer;color:"red"'+
									+'data-name="'+localities.name+'"'+ 
									+'class="m-portlet__nav-link btn m-btn m-btn--hover-accent m-btn--icon m-btn--icon-only m-btn--pill btnUpdateUser"' 
									+'title="Editar "'
									+'data-toggle="modal"' 
									+'data-target="#eliminar_localidad_modal">'
									+'<i class="la la-close"></i>'
								+'</a>'
								+'</center>'
							+'</td>'
						+'</tr>');
      			});
			},
			error: function (err) {
				console.log(err)
			}
		});
	}


	function eliminarLocalidad(idLocalidad,Localidad,Provincia){
		localidad_id = idLocalidad;
		$('#mensajeEliminarLocalidad').html('Estas por eliminar la localidad <strong>'+Localidad+'</strong> que pertenece a la provincia <strong>'+Provincia+'<strong>');
	}


	function eliminarProvince(idprovince,province){
		provincia_id_global = idprovince;
		$('#mensajeEliminarProvince').html('Estas por eliminar el estado <strong>'+province+'</strong>, tambien se eliminaran los barrios correspondientes.');
	}

	function getComboPaises(){
		$("#paises").html('<option value=0>Elegir</option>');
		$('#paisesEditar').html('<option value=0>Elegir</option>');
		$.ajax({
			type: "GET",
			url: "{{ route('system.country') }}",
			data: {
				"_token": "{{ csrf_token() }}",
			},
			success: function (data) {
				//console.log(data);
				$.each(data,function(key, pais) {
        			$("#paises").append('<option value='+pais.id+'>'+pais.name+'</option>');
					$("#paisesEditar").append('<option value='+pais.id+'>'+pais.name+'</option>');
      			});
			},
			error: function (data) {
				console.log(data);
			}
		});	
	}

	function getComboProvince(){
		$('#provinciaLocalidad').html('<option value="0">Elegir</option>');
		$('#provinciaLocalidadEditar').html('<option value="0">Elegir</option>');
		$.ajax({
			type: "GET",
			url: "{{ route('system.province.list') }}",
			data: {
				"_token": "{{ csrf_token() }}",
			},
			success: function (data) {
				//console.log(data);
				$.each(data,function(key, provincie) {
        			$("#provinciaLocalidad").append('<option value='+provincie.id+'>'+provincie.name+'</option>');
					$("#provinciaLocalidadEditar").append('<option value='+provincie.id+'>'+provincie.name+'</option>');
      			});
			},
			error: function (data) {
				console.log(data);
			}
		});	
	}

	function getComboLocalidades(){
		$("#localidadListado").html('<option value="0">Elegir</option>');
		$("#localidadListadoEditar").html('<option value="0">Elegir</option>');
		$.ajax({
			type: "GET",
			url: "{{ route('system.localities.list') }}",
			data: {
				"_token": "{{ csrf_token() }}",
			},
			success: function (data) {
				//console.log(data);
				$.each(data,function(key, localidad) {
        			$("#localidadListado").append('<option value='+localidad.id+'>'+localidad.name+'</option>');
					$("#localidadListadoEditar").append('<option value='+localidad.id+'>'+localidad.name+'</option>');
      			});
			},
			error: function (data) {
				console.log(data);
			}
		});	
	}

	function nuevoPais(){
		$('#pais').val('');
	}

	function nuevoProvincia(){
		$('#provincia').val('');
		getComboPaises();
	}

	function nuevoBarrio(){
		$('#barrio').val('');
		getComboLocalidades();
	}

	function nuevaLocalidad(){
		$('#localidad').val('');
		getComboProvince();
	}

	

	function editarBarrio(idBarrio,Barrio,localidadId,localities){
		barrio_id = idBarrio;
		localidad_id_global = localidadId;
		$('#barrioEditar').val(Barrio);
		$('#localidadActual').html('<strong>'+localities+'</strong>');
		$('#localidadListadoEditar').val(localidadId);
		//alert(idBarrio+" "+Barrio+" "+localidadId);
		getComboLocalidades();
	}

	function editarLocalidad(idlocalidad,localidad,provinciaId,province){
		localidad_id = idlocalidad;
		provincia_id_global = provinciaId;
		$('#localidadEditar').val(localidad);
		$('#provinciaEditarActual').html('<strong>'+province+'</strong>');
		getComboProvince();
		//alert(idlocalidad+" "+localidad+" "+provinciaId+" "+province);
	}


	function editarProvince(idprovincia,province,idpais,pais){
		provincia_id_global = idprovincia;
		pais_id_global =idpais;
		$('#provinciaEditar').val(province);
		$('#paiseEditarActual').html('<strong>'+pais+'</strong>');
		getComboPaises();
	}

	function guardarNuevoPais(){
		var pais = $('#pais').val();
		if(pais == ''){
			alert('Debes completar el nombre del pais');
		}else{
			$.ajax({
				type:"POST",
				url : "{{ route('system.country.save')}}",
				data:{
					"_token":"{{csrf_token()}}",
					"_method": "post",
					"pais":pais
				},
				success:function(data){
					$('#nuevo_pais_modal').modal('hide');
					getPaises();
				},
				error:function(error){
					console.log(error);
				}
			})
		}
	}

	function showBuscadorBarrio(){
		if(estado_buscador_barrio){
			estado_buscador_barrio = false;
		}else{
			estado_buscador_barrio = true;
		}	
			
		if(estado_buscador_barrio){
			$('#buscadorBarrio').show('slow');
			$('#barrioBusqueda').val('');
		}else{
			$('#buscadorBarrio').hide('slow');
			$('#barrioBusqueda').val('');
		}
	}

	function showBuscadorLocalidad(){

		if(estado_buscador_localidad){
			estado_buscador_localidad = false;
		}else{
			estado_buscador_localidad = true;
		}

		if(estado_buscador_localidad){
			$('#buscadorLocalidad').show('slow');
			$('#localidadBusqueda').val('');
		}else{
			$('#buscadorLocalidad').hide('slow');
			$('#localidadBusqueda').val('');
		}
		
	}

	function searchLocalidad(){
		var localidad = '';
		if($('#localidadBusqueda').val() !== ''){
			$('#loadingBusquedaLocalidad').show();
			localidad =MaysPrimera($('#localidadBusqueda').val().toLowerCase());  
			//alert(localidad);
			$.ajax({
				type:"get",
				url : "{{ route('system.localities.search')}}",
				data:{
					"_token":"{{csrf_token()}}",
					"_method": "get",
					"localities":localidad
				},
				success:function(data){
					//console.log(data.length);
					if(data.length === 0){
						$('#loadingBusquedaLocalidad').hide();
						$('#mensajeBusquedaNoLocalidad').show();
						$('#mensajeNoEncontroLocalidad').html("No se encontraron resultados para la localidad <strong>"+localidad+"</strong>");
						$('#mensajeEncontroLocalidad').html("");
						$('#mensajeBusquedaEncontroLocalidad').hide();
					}else{
						$('#mensajeBusquedaEncontroLocalidad').show('slow');
						$('#loadingBusquedaLocalidad').hide();
						$('#mensajeBusquedaNoLocalidad').hide();
						$('#mensajeNoEncontroLocalidad').html("");
						$('#mensajeEncontroLocalidad').html("");
						//$.each(data,function(key, localidad) {
        				//$("#localidadListado").append('<option value='+localidad.id+'>'+localidad.name+'</option>');
						//$("#localidadListadoEditar").append('<option value='+localidad.id+'>'+localidad.name+'</option>');
						//console.log(data);
						$.each(data,function(key,localidad){
							$('#mensajeEncontroLocalidad').append('<li>Se encontro la localidad de <strong>'+localidad.name+'</strong> que pertenece a la provincia de <strong>'+localidad.province+'</strong></li>')
						});					
					}
				},
				error:function(error){
					$('#loadingBusquedaBarrio').html('<strong>Ocurrio un error vuelva a intentar</strong>');
					console.log(error);
				}
			})
		}else{
			$('#loadingBusquedaBarrio').hide();
		}
	}

	function MaysPrimera(string){
  		return string.charAt(0).toUpperCase() + string.slice(1);
	}
	
	function searchBarrio(){
		var barrio = '';
		if($('#barrioBusqueda').val() !== ''){
			$('#loadingBusquedaBarrio').show();
			barrio =MaysPrimera($('#barrioBusqueda').val().toLowerCase());  ;
			$.ajax({
				type:"get",
				url : "{{ route('system.cities.search')}}",
				data:{
					"_token":"{{csrf_token()}}",
					"_method": "get",
					"city":barrio
				},
				success:function(data){
					//console.log(data.length);
					if(data.length === 0){
						$('#loadingBusquedaBarrio').hide();
						$('#mensajeBusquedaNoBarrio').show();
						$('#mensajeNoEncontroBarrio').html("No se encontraron resultados para el barrio <strong>"+barrio+"</strong>");
						$('#mensajeEncontroBarrio').html("");
						$('#mensajeBusquedaEncontroBarrio').hide();
					}else{
						$('#mensajeBusquedaEncontroBarrio').show('slow');
						$('#loadingBusquedaBarrio').hide();
						$('#mensajeBusquedaNoBarrio').hide();
						$('#mensajeNoEncontroBarrio').html("");
						$('#mensajeEncontroBarrio').html("");
						//$.each(data,function(key, localidad) {
        				//$("#localidadListado").append('<option value='+localidad.id+'>'+localidad.name+'</option>');
						//$("#localidadListadoEditar").append('<option value='+localidad.id+'>'+localidad.name+'</option>');
						//console.log(data);
						$.each(data,function(key,barrio){
							$('#mensajeEncontroBarrio').append('<li>Se encontro el barrio <strong>'+barrio.name+'</strong> que pertenece a la localidad de <strong>'+barrio.localities+'</strong></li>')
						});					
					}
				},
				error:function(error){
					$('#loadingBusquedaBarrio').html('<strong>Ocurrio un error vuelva a intentar</strong>');
					console.log(error);
				}
			})
		}else{
			$('#loadingBusquedaBarrio').hide();
		}
	}

	function closeBusquedaBarrio(){
		estado_buscador_barrio = false;
		$('#buscadorBarrio').hide('slow');
		$('#loadingBusquedaBarrio').hide();
		$('#mensajeBusquedaNoBarrio').hide('slow');
		$('#mensajeBusquedaEncontroBarrio').hide();
		$('#mensajeNoEncontroBarrio').html("");
	}

	function closeBusquedaLocalidad(){
		estado_buscador_localidad = false;
		$('#buscadorLocalidad').hide('slow');
		$('#loadingBusquedaLocalidad').hide();
		$('#mensajeBusquedaNoLocalidad').hide('slow');
		$('#mensajeBusquedaEncontroLocalidad').hide();
		$('#mensajeNoEncontroLocalidad').html("");
	}

	function guardarProvincia(accion){
		var pais_id = '';
		var provincia = '';

		if(accion === 'nuevo'){
			pais_id = $('#paises').val();
			provincia = $('#provincia').val();

			if(pais_id == 0){
				alert("Debes seleccionar un pais");
			}else if(provincia == ''){
				alert('Debes completar el nombre de la provincia')
			}else{
				$.ajax({
					method:'post',
					url : "{{ route('system.province.save')}}",
					data:{
						"_token":"{{csrf_token()}}",
						"_method": "post",
						"pais_id":pais_id,
						"provincia":provincia
					},
					success:function(data){
						//console.log(data);
						$('#nuevo_provincia_modal').modal('hide');
						getProvince();
					},
					error:function(error){
						console.log(error);
					}
				})
			}
		}else if(accion === 'editar'){
			pais_id   = $('#paisesEditar').val();
			provincia = $('#provinciaEditar').val();

			if(pais_id == 0){
				pais_id = pais_id_global;
			}

			if(provincia == ''){
				alert('Por favor , completar el campo provincia');
			}else{
				$.ajax({
					method:'put',
					url : "{{ route('system.province.update')}}",
					data:{
						"_token":"{{csrf_token()}}",
						"_method": "put",
						"idprovincia":provincia_id_global,
						"pais_id":pais_id,
						"provincia":provincia
					},
					success:function(data){
						//console.log(data);
						$('#editar_province_modal').modal('hide');
						getProvince();
					},
					error:function(error){
						console.log(error);
					}
				});
			} 
		}
	}

	function guardarBarrio(accion){
		var localidad_id = '';
		var barrio = '';

		if(accion === 'nuevo'){
			localidad_id = $('#localidadListado').val();
			barrio = $('#barrio').val();
			if(localidad_id == 0){
				alert("Por favor debes seleccionar una localidad");
			}else if(barrio == ''){
				alert("Por favor debes completar el barrio");
			}else{
				$.ajax({
					method:'post',
					url : "{{ route('system.cities.save')}}",
					data:{
						"_token":"{{csrf_token()}}",
						"_method": "post",
						"localidad_id":localidad_id,
						"barrio":barrio
					},
					success:function(data){
						$('#nuevo_barrio_modal').modal('hide');
						getBarrios();
					},
					error:function(error){
						console.log(error);
					}
				})
			}
		}else if(accion === 'editar'){
			localidad_id = $('#localidadListadoEditar').val();
			barrio = $('#barrioEditar').val();
			
			if(localidad_id == 0){
				localidad_id  = localidad_id_global;
			}
			
			if(barrio == ''){
				alert("Por favor debes completar el barrio");
			}else{
				//alert("aca editar "+barrio+" "+" localidad_id"+localidad_id);
				$.ajax({
					method:'put',
					url : "{{ route('system.cities.update')}}",
					data:{
						"_token":"{{csrf_token()}}",
						"_method": "put",
						"localidad_id":localidad_id,
						"barrio":barrio,
						"idbarrio":barrio_id
					},
					success:function(data){
						$('#editar_barrio_modal').modal('hide');
						getBarrios();
						//console.log(data);
					},
					error:function(error){
						console.log(error);
					}
				})
			}
		}
		
	}

	function eliminarBarrio(idBarrio,Barrio){
		barrio_id = idBarrio;
		barrio    = Barrio;
		$('#mensajeEliminarBarrio').html('<p>Estas por eliminar el barrio <strong>'+barrio+'</strong></p>');
	}

	function eliminarBarrioData(){
		$.ajax({
			method:'delete',
			url : "{{route('system.cities.delete')}}",
			data:{
				"_token":"{{csrf_token()}}",
				"_method": "delete",
				"idbarrio":barrio_id
			},
			success:function(data){
				$('#eliminar_barrio_modal').modal('hide');
				getBarrios();
			},
			error:function(error){
				console.log(error);
			}
		})
	}

	function eliminarProvinceData(){
		//alert(provincia_id_global);
		$.ajax({
			method:'delete',
			url : "{{route('system.province.delete')}}",
			data:{
				"_token":"{{csrf_token()}}",
				"_method": "delete",
				"idprovincia":provincia_id_global
			},
			success:function(data){
				$('#eliminar_province_modal').modal('hide');
				getProvince();
			},
			error:function(error){
				console.log(error);
			}
		});
	}

	function eliminarLocalidadData(){
		$.ajax({
			method:'delete',
			url : "{{route('system.localities.delete')}}",
			data:{
				"_token":"{{csrf_token()}}",
				"_method": "delete",
				"idlocalidad":localidad_id
			},
			success:function(data){
				$('#eliminar_localidad_modal').modal('hide');
				getLocalities();
			},
			error:function(error){
				console.log(error);
			}
		})
	}

	//localidad
	function guardarLocalidad(accion){
		var provincia_id="";
		var localidad =  "";
		//alert();
		if(accion === 'nuevo'){
			provincia_id = $('#provinciaLocalidad').val();
			localidad = $('#localidad').val();
			if(provincia_id == 0){
				alert("Por favor debes seleccionar una provincia");
			}else if(localidad == ''){
				alert("Por favor debes completar el campo localidad");
			}else{
				$.ajax({
					method:'post',
					url: "{{route('system.localities.save')}}",
					data:{
						"_token":"{{csrf_token()}}",
						"_method": "post",
						"idprovincia":provincia_id,
						"localities" :localidad
					},
					success:function(data){
						$('#nuevo_localidad_modal').modal('hide');
						getLocalities();
					},
					error:function(err){
						console.log(err);
					}
				});
			}
		}else if(accion === 'editar'){
			provincia_id = $('#provinciaLocalidadEditar').val();
			localidad    = $('#localidadEditar').val();
			
			if(provincia_id == 0){
				provincia_id = provincia_id_global;
			}

			if(localidad == ''){
				alert("Debes completar la localidad");
			}else{
				//alert(localidad_id);
				//alert(provincia_id+" "+localidad);
				$.ajax({
					method:'put',
					url: "{{route('system.localities.update')}}",
					data:{
						"_token":"{{csrf_token()}}",
						"_method": "put",
						"idprovincia":provincia_id,
						"localities" :localidad,
						'idlocalities':localidad_id
					},
					success:function(data){
						//console.log(data);
						$('#editar_localidad_modal').modal('hide');
						getLocalities();
					},
					error:function(err){
						console.log(err);
					}
				});
			}
		}
	}

	getPaises();	
	getProvince();
	getLocalities();
	getBarrios();
</script>
@endsection
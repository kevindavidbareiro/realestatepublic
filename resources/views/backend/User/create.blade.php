
@extends('master')

@section('stylus')


@endsection
@section('content_admin')

       <div class="m-portlet">
                <div class="m-portlet__head">
                    <div class="m-portlet__head-caption">
                        <div class="m-portlet__head-title">
                            <span class="m-portlet__head-icon m--hide">
                                <i class="la la-gear"></i>
                            </span>
                            <h3 class="m-portlet__head-text">
                                Nuevo Usuario
                            </h3>
                        </div>
                    </div>
                </div>
                
            
                <form class="m-form m-form--fit m-form--label-align-right m-form--group-seperator-dashed m-form--state" method = "POST" action = "{{url('User')}}">
                    <input type="hidden" name="_token" value="{{ csrf_token() }}">
                    <div class="m-portlet__body">
                        @if (session('typemsg'))
                            @if (session('typemsg') == 'success')
                                <div class="alert alert-success">
                                <strong><p>{{ session('message') }}</p></strong>
                                </div>
                            @endif
                            @if (session('typemsg') == 'error')
                                <div class="alert alert-danger">
                                <strong><p>{{ session('message') }}</p></strong>
                                </div>
                            @endif	
                        @endif		
                        <div class="form-group m-form__group row">
                            <div class="col-lg-6 @if($errors->has('name')) has-danger @endif">
                                <label>
                                    <strong> Nombre: </strong> 
                                </label>
                                <input type="text" name="name" id="Name" class="form-control m-input @if($errors->has('name')) form-control-danger @endif" placeholder="Nombre" value="{{ old('name') }}">
                                
                                @if ($errors->any())
                                    @if($errors->has('name'))
                                    <div class="form-control-feedback">
                                        Por favor ingrese un nombre
								    </div>
                                    @endif
                                @endif
                            </div>
                            <div class="col-lg-6 @if($errors->has('lastname')) has-danger @endif">
                                <label>
                                <strong> Apellido: </strong>
                                    
                                </label>
                                <input type="text" name="lastname" id="LastName" class="form-control m-input form-control-danger @if($errors->has('lastname')) form-control-danger @endif" placeholder="Apellido" value="{{ old('lastname') }}">
                                @if ($errors->any())
                                    @if($errors->has('lastname'))
                                    <div class="form-control-feedback">
                                        Por favor ingrese un apellido
								    </div>
                                    @endif
                                @endif
                            </div>
                            
                        </div>
                        <div class="form-group m-form__group row">
                            <div class="col-lg-6 @if($errors->has('email')) has-danger @endif">
                                <label>
                                    <strong> Email: </strong> 
                                </label>
                                <input type="email" name="email" id="Email" class="form-control m-input @if($errors->has('email')) form-control-danger @endif" placeholder="Email" value="{{ old('email') }}">
                                @if ($errors->any())
                                    @if($errors->has('email'))
                                    <div class="form-control-feedback">
                                        Por favor ingrese un correo electronico
								    </div>
                                    @endif
                                @endif
                            </div>
                            <div class="col-lg-6 @if($errors->has('password')) has-danger @endif">
                                <label>
                                <strong> Contraseña: </strong>
                                    
                                </label>
                                <input type="password" name="password" id="Password" class="form-control m-input @if($errors->has('password')) form-control-danger @endif" placeholder="Contraseña" value="{{ old('password') }}">
                                @if ($errors->any())
                                    @if($errors->has('password'))
                                    <div class="form-control-feedback">
                                        Por favor ingrese una contraseña
								    </div>
                                    @endif
                                @endif
                            </div>
                            
                        </div>
                        
                        <div class="form-group m-form__group row">
                            <div class="col-lg-6">
                            <label for="exampleSelect1">
                                <strong> Perfil: </strong>
                                    
                                </label>
                                <select name="profile" class="form-control m-input" id="Profile">

                                    @foreach ($profiles as $profile)
                                            <option value="{{ $profile->id }}" @if ($profile->id == 2) selected @endif
                                                @if (old('profile'))
                                                    @if (old('profile') == $profile->id)
                                                        selected
                                                    @endif
                                                @endif
                                                >
                                                {{ $profile->name }}
                                            </option>
                                    @endforeach

                                </select>

                            </div>
                            
                        </div>

                       
                    <div class="m-portlet__foot m-portlet__no-border m-portlet__foot--fit">
                        <div class="m-form__actions m-form__actions--solid">
                            <div class="row">
                                <div class="col-lg-4"></div>
                                <div class="col-lg-8">
                                    <button type="submit" id="Save" class="btn btn-primary">
                                        Guardar
                                    </button>
                                    <a href="{{url('User')}}" class="btn btn-secondary">
                                       <span>
                                            Volver
                                        </span>
                                    </a>
                                </div>
                            </div>
                        </div>
                    </div>

                 </div>
                </form>
                <!--end::Form-->
               
        </div>
@endsection


@section('script')

@endsection
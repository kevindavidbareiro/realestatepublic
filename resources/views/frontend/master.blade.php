<!DOCTYPE html>
<html lang="zxx">
<head>
    <title>Imperio Inmobiliairo</title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta charset="utf-8">

    <!-- External CSS libraries -->
    <link rel="stylesheet" type="text/css" href="{{asset('templante/HTML/css/bootstrap.min.css')}}">
    <link rel="stylesheet" type="text/css" href="{{asset('templante/HTML/css/animate.min.css')}}">
    <link rel="stylesheet" type="text/css" href="{{asset('templante/HTML/css/bootstrap-submenu.css')}}">

    <link rel="stylesheet" type="text/css" href="{{asset('templante/HTML/css/bootstrap-select.min.css')}}">
    <link rel="stylesheet" type="text/css" href="{{asset('templante/HTML/css/magnific-popup.css')}}">
    <link rel="stylesheet" href="{{asset('templante/HTML/css/leaflet.css')}}" type="text/css">
    <link rel="stylesheet" href="{{asset('templante/HTML/css/map.css')}}" type="text/css">
    <link rel="stylesheet" type="text/css" href="{{asset('templante/HTML/fonts/font-awesome/css/font-awesome.min.css')}}">
    <link rel="stylesheet" type="text/css" href="{{asset('templante/HTML/fonts/flaticon/font/flaticon.css')}}">
    <link rel="stylesheet" type="text/css" href="{{asset('templante/HTML/fonts/linearicons/style.css')}}">
    <link rel="stylesheet" type="text/css"  href="{{asset('templante/HTML/css/jquery.mCustomScrollbar.css')}}">
    <link rel="stylesheet" type="text/css"  href="{{asset('templante/HTML/css/dropzone.css')}}">
    <link rel="stylesheet" type="text/css"  href="{{asset('templante/HTML/css/slick.css')}}">

    <!-- Custom stylesheet -->
    <link rel="stylesheet" type="text/css" href="{{asset('templante/HTML/css/style.css')}}">
    <link rel="stylesheet" type="text/css" id="style_sheet" href="{{asset('templante/HTML/css/skins/default.css')}}">
    <link rel="stylesheet" type="text/css" href="{{asset('css/style.css')}}">
    <!-- Favicon icon -->
    <link rel="shortcut icon" href="{{asset('templante/HTML/img/favicon.ico')}}" type="image/x-icon" >

    <!-- Google fonts -->
    <link rel="stylesheet" type="text/css" href="https://fonts.googleapis.com/css?family=Raleway:300,400,500,600,300,700">

    <!-- IE10 viewport hack for Surface/desktop Windows 8 bug -->
    <link rel="stylesheet" type="text/css" href="{{asset('templante/HTML/css/ie10-viewport-bug-workaround.css')}}">

    <!-- Just for debugging purposes. Don't actually copy these 2 lines! -->
    <!--[if lt IE 9]><script  src="js/ie8-responsive-file-warning.js"></script><![endif]-->
    <script  src="{{asset('templante/HTML/js/ie-emulation-modes-warning.js')}}"></script>
    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
    <script  src="js/html5shiv.min.js"></script>
    <script  src="js/respond.min.js"></script>
    <![endif]-->
    @yield('stylus')
</head>
<body>
<div class="page_loader"></div>

<!-- Main header start -->
<!-- <header class="main-header header-transparent sticky-header">Comente la clase que hace negro el nav en el index para que se aprecie el logo-->
<header class="main-header sticky-header">
    <div class="container">
        <nav class="navbar navbar-expand-lg navbar-light">
            <a class="navbar-brand " href="{{ url('/') }}">
                <img class="logo-realestate" src="{{asset('img/logo.png')}}" alt="logo Augusto Imperio Inmobiliairo">
            </a>
            <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
                <span class="navbar-toggler-icon"></span>
            </button>
            <div class="collapse navbar-collapse" id="navbarSupportedContent">
                <ul class="navbar-nav ml-auto">
                @if (!auth()->check())
                    <li class="nav-item sp">
                        <a class="nav-link " href="{{ url('loginUser') }}" id="login" >
                            Iniciar Sesión
                        </a>
                    </li>
                    <li class="nav-item sp">
                        <a class="nav-link " href="{{ url('register') }}" id="register" >
                            Registrarse
                        </a>
                    </li>
                @endif
                @if (auth()->check())
                    <li class="nav-item dropdown">
                        
                            <a class="nav-link dropdown-toggle" href="#" id="navbarDropdownMenuLink7" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                {{ auth()->user()->name }}
                            </a>

                        <ul class="dropdown-menu" aria-labelledby="navbarDropdownMenuLink">
                            
                               <li><a class="dropdown-item" href="{{ url('panel') }}">Dashboard</a></li>
                                <li><a class="dropdown-item" href="{{ url('usuario/perfil') }}">Mi Perfil</a></li>
                                <li><a class="dropdown-item" href="{{ url('propiedades') }}">Mis Propiedades</a></li>
                                <li> <a class="dropdown-item" href="my-invoices.html">Mis Compras</a></li>
                                <li><a class="dropdown-item" href="favorited-properties.html">Propiedades Favoritas</a></li>
                                <li><a class="dropdown-item" href="messages.html">Mensajes</a></li>
                                <li><a class="dropdown-item" href="bookings.html">Reservas</a></li>
                                <li><a class="dropdown-item" href="{{ url('propiedades/create') }}">Cargar Propiedad</a></li> 
                                <li><a class="dropdown-item" href="{{ url('logout') }}">Cerrar Sesion</a></li> 
                            
                        </ul>
                    </li>
                
                    <li class="nav-item sp">
                        <a href="{{ url('propiedades/create') }}" class="nav-link link-color"><i class="fa fa-plus"></i> Cargar Propiedad</a>
                    </li>
                @endif
                </ul>
            </div>
        </nav>
    </div>
</header>
<!-- Main header end -->


@yield('content_frontend')


<!-- Footer start -->
<footer class="footer">
    <div class="container footer-inner">
        <div class="row">
            <div class="col-xl-4 col-lg-4 col-md-6 col-sm-6">
                <div class="footer-item clearfix">
                    <img src="{{asset('img/logo.png')}}" alt="logo Agusto Inmobiliaria" class="f-logo">
                    <div class="text">
                        <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Maecenas in pulvinar neque. Nulla finibus lobortis pulvinar. Donec a consectetur nulla. Nulla posuere sapien vitae.</p>
                    </div>
                </div>
            </div>
            <div class="col-xl-3 col-lg-3 col-md-6 col-sm-6">
                <div class="footer-item">
                    <h4>Contacta Con Nosotros</h4>
                    <div class="f-border"></div>
                    <ul class="contact-info">
                        <li>
                            <i class="flaticon-pin"></i> Direccion Inmobiliaria
                        </li>
                        <li>
                            <i class="flaticon-mail"></i><a href="mailto:ventas@inmobiliaria.com">Informacio@inmobiliaria.com</a>
                        </li>
                        <li>
                            <i class="flaticon-phone"></i><a href="tel:+55-417-634-7071">Telefono 1</a>
                        </li>
                        <li>
                            <i class="flaticon-fax"></i>Telefono Fax
                        </li>
                    </ul>
                </div>
            </div>
            <div class="col-xl-2 col-lg-2 col-md-6 col-sm-6">
                <div class="footer-item">
                    <h4>
                        Links Utiles
                    </h4>
                    <div class="f-border"></div>
                    <ul class="links">
                        <li>
                            <a href="#">Home</a>
                        </li>
                        <li>
                            <a href="about.html">Acerca de Nosotros</a>
                        </li>
                        <li>
                            <a href="services.html">Servicios</a>
                        </li>
                        <li>
                            <a href="contact.html">Contactanos</a>
                        </li>
                        <li>
                            <a href="dashboard.html">Dashboard</a>
                        </li>
                        <li>
                            <a href="properties-details.html">Detalles de las Propiedades</a>
                        </li>
                    </ul>
                </div>
            </div>
            <div class="col-xl-3 col-lg-3 col-md-6 col-sm-6">
                <div class="footer-item clearfix">
                    <h4>Suscribete a Nosotros</h4>
                    <div class="f-border"></div>
                    <div class="Subscribe-box">
                        <p>Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit.</p>
                        <form class="form-inline" action="#" method="GET">
                            <input type="text" class="form-control mb-sm-0" id="inlineFormInputName3" placeholder="Email Address">
                            <button type="submit" class="btn"><i class="fa fa-paper-plane"></i></button>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</footer>
<!-- Footer end -->

<!-- Sub footer start -->
<div class="sub-footer">
    <div class="container">
        <div class="row">
            <div class="col-lg-8 col-md-8">
                <p class="copy">© 2019 <a href="http://www.jumpfsa.com.ar"> Jump Diseño.</a>  En Jump Diseño estamos comprometidos con brindar soluciones creativas e innovadoras enfocadas al logro de los objetivos empresariales de nuestros clientes.


</p>
            </div>
            <div class="col-lg-4 col-md-4">
                <ul class="social-list clearfix">
                    <li><a href="#" class="facebook"><i class="fa fa-facebook"></i></a></li>
                    <li><a href="#" class="twitter"><i class="fa fa-twitter"></i></a></li>
                    <li><a href="#" class="google"><i class="fa fa-google-plus"></i></a></li>
                    <li><a href="#" class="rss"><i class="fa fa-rss"></i></a></li>
                    <li><a href="#" class="linkedin"><i class="fa fa-linkedin"></i></a></li>
                </ul>
            </div>
        </div>
    </div>
</div>
<!-- Sub footer end -->

<!-- Full Page Search -->
<div id="full-page-search">
    <button type="button" class="close">×</button>
    <form action="index.html#">
        <input type="search" value="" placeholder="type keyword(s) here" />
        <button type="submit" class="btn btn-sm button-theme">Buscar</button>
    </form>
</div>

<!-- Modals register and login -->
@include('frontend.modals.modal-registro')

<script src="{{asset('templante/HTML/js/jquery-2.2.0.min.js')}}"></script>
<script src="{{asset('templante/HTML/js/popper.min.js')}}"></script>
<script src="{{asset('templante/HTML/js/bootstrap.min.js')}}"></script>
<script  src="{{asset('templante/HTML/js/bootstrap-submenu.js')}}"></script>
<script  src="{{asset('templante/HTML/js/rangeslider.js')}}"></script>
<script  src="{{asset('templante/HTML/js/jquery.mb.YTPlayer.js')}}"></script>
<script  src="{{asset('templante/HTML/js/bootstrap-select.min.js')}}"></script>
<script  src="{{asset('templante/HTML/js/jquery.easing.1.3.js')}}"></script>
<script  src="{{asset('templante/HTML/js/jquery.scrollUp.js')}}"></script>
<script  src="{{asset('templante/HTML/js/jquery.mCustomScrollbar.concat.min.js')}}"></script>
<script  src="{{asset('templante/HTML/js/leaflet.js')}}"></script>
<script  src="{{asset('templante/HTML/js/leaflet-providers.js')}}"></script>
<script  src="{{asset('templante/HTML/js/leaflet.markercluster.js')}}"></script>
<script  src="{{asset('templante/HTML/js/dropzone.js')}}"></script>
<script  src="{{asset('templante/HTML/js/slick.min.js')}}"></script>
<script  src="{{asset('templante/HTML/js/jquery.filterizr.js')}}"></script>
<script  src="{{asset('templante/HTML/js/jquery.magnific-popup.min.js')}}"></script>
<script  src="{{asset('templante/HTML/js/jquery.countdown.js')}}"></script>
<script  src="{{asset('templante/HTML/js/maps.js')}}"></script>
<script  src="{{asset('templante/HTML/js/app.js')}}"></script>

<!-- IE10 viewport hack for Surface/desktop Windows 8 bug -->
<script  src="{{asset('templante/HTML/js/ie10-viewport-bug-workaround.js')}}"></script>
<!-- Custom javascript -->
<script  src="{{asset('templante/HTML/js/ie10-viewport-bug-workaround.js')}}"></script>
@yield('scripts')
</body>
</html>
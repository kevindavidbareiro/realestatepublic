<!-- Featured Properties start -->
<div class="featured-properties content-area">
    <div class="container">
        <!-- Main title -->
        <div class="main-title">
            <h1>Propiedades Destacadas</h1>
            <p>Encuentra tu Propiedad en tu Ciudad</p>
        </div>
        <!-- Slick slider area start -->
      
        <div class="slick-slider-area">
            <div class="row slick-carousel" data-slick='{"slidesToShow": 3, "responsive":[{"breakpoint": 1024,"settings":{"slidesToShow": 2}}, {"breakpoint": 768,"settings":{"slidesToShow": 1}}]}'>
           
           
                <!-- Div de los inmuebles Destacados -->
            @foreach($realestate ->slice(0,3) as $realestate)                

                <div class="slick-slide-item">
                    <div class="property-box">
                        <div class="property-thumbnail">
                            <a href="properties-details.html" class="property-img">
                                <div class="listing-badges">
                                    <span class="featured">Destacados</span>
                                </div>
                                <div class="price-box"><span>{{$realestate->price}}</span> Por Mes</div>
                                <img class="d-block w-100" src="http://placehold.it/330x220" alt="properties">
                            </a>
                        </div>
                        <div class="detail">
                            <h1 class="title">
                                <a href="properties-details.html">{{$realestate->name}}</a>
                            </h1>

                            <div class="location">
                                <a href="properties-details.html">
                                    <i class="flaticon-pin"></i>{{$realestate->addres}}
                                </a>
                            </div>
                        </div>
                        <ul class="facilities-list clearfix">
                            <li>
                                <span>Area</span>{{$realestate->area}} m2
                                
                            </li>
                            <li>
                                <span>Habitaciones</span> {{$realestate->bedrooms}}
                            </li>
                            <li>
                                <span>Baños</span> {{$realestate->bathrooms}}
                            </li>
                            <!-- <li>
                                <span>Garage</span> 1
                            </li> -->
                        </ul>
                        <div class="footer">
                            <a href="#">
                                <i class="flaticon-people"></i> 
                            </a>
                            <span> 
                                <i class="flaticon-calendar"></i>Hace <!--realestate->created_at--> Días
                            </span>
                        </div>
                    </div>
                </div>
            @endforeach
            <!-- Div de los inmuebles Destacados -->
            </div>
            <div class="slick-prev slick-arrow-buton">
                <i class="fa fa-angle-left"></i>
            </div>
            <div class="slick-next slick-arrow-buton">
                <i class="fa fa-angle-right"></i>
            </div>
        </div>
    </div>
</div>
<!-- Featured Properties end -->

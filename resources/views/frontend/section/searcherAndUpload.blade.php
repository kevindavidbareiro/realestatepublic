<!-- Services start -->
<div class="services content-area bg-grea-3">
    <div class="container">
        <!-- Main title -->
        <div class="main-title text-center">
            <h1 id="titleMain">¿Queres Alquilar , Vender o Comprar?</h1>
            <p>Nosotros lo tenemos</p>
        </div>
        
        <div id="rowSearchUpload" class="row">
            <div class="boxHover col-xl-6 col-lg-6 col-md-6 col-sm-6">
                <a id="searchRealestate" href="#">
                    <div class="service-info boxMain">
                        <div class="icon">
                            <i class="fa fa-search"></i>
                        </div>
                        <h3>Buscar Propiedad</h3>
                    </div>
                </a>
            </div>
            <div class="col-xl-6 col-lg-6 col-md-6 col-sm-6 ">
                <a href="#">
                    <div class="service-info boxMain">
                        <div class="icon">
                            <i class="fa fa-cloud-upload"></i>
                        </div>
                        <h3>Subir Propiedad</h3>
                    </div>
                </a>
            </div>
        </div>
        <div id="divForm" class="row">
            <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 ">
                <div class="service-info">
                    
                    <h3>Buscar Propiedad</h3>
                    <button type="btn" id="backFrm" class="btn btn-primary btnBack">Atrás</button>
                    <form action="{{ url('searches-list') }}" enctype="multipart/form-data" method="POST">
                            @csrf
                            <div class="search-contents-sidebar">
                                <div class="row pad-20">
                                    <div class="col-lg-4 col-md-4 col-sm-12">
                                        <div class="form-group">
                                            <label for="country">Tipo de Inmueble</label>
                                            <select class="form-control search-fields" id="typeOfRealestate" name="typeOfRealestate_id">
                                                @foreach ($typeOfRealestates as $typeOfRealestate)
                                                    <option value="{{ $typeOfRealestate->id }}"
                                                        @if (old('typeOfRealestate_id'))
                                                            @if (old('typeOfRealestate_id') == $typeOfRealestate->id)
                                                                selected
                                                            @endif
                                                        @elseif ($typeOfRealestate->id == 1)
                                                            selected
                                                        @endif
                                                        >
                                                        {{ $typeOfRealestate->name }}
                                                    </option>
                                                @endforeach
                                            </select>
                                        </div>
                                    </div>
                                    <div class="col-lg-4 col-md-4 col-sm-12">
                                        <div class="form-group">
                                            <label for="state">Condición</label>
                                            <select class="form-control search-fields" id="state" name="state_id">
                                                @foreach ($states as $state)
                                                    @if($state->name != 'Alquilado' and $state->name != 'Vendido')
                                                        <option value="{{ $state->id }}"
                                                            @if (old('state_id'))
                                                                @if (old('state_id') == $state->id)
                                                                    selected
                                                                @endif
                                                            @elseif ($state->id == 1)
                                                                selected
                                                            @endif
                                                            >
                                                        
                                                               en: {{ $state->name }}
                                                            
                                                        </option>
                                                    @endif
                                                @endforeach
                                            </select>
                                        </div>
                                    </div>
                                    <div class="col-lg-4 col-md-4 col-sm-12">
                                        <div class="form-group">
                                            <label for="state">Condición</label>
                                            <select class="form-control search-fields" id="rooms" name="rooms_id"> 
                                                <option value="1">1 Habitación</option>
                                                <option value="2">2 Habitaciónes</option>
                                                <option value="3">3 Habitaciónes</option>
                                                <option value="4">4 Habitaciónes</option>
                                                <option value="masHabitaciones">mas Habitaciónes</option>
                                            </select>
                                        </div>
                                    </div>
                                    <div class="col-lg-4 col-md-4 col-sm-12">
                                        <div class="form-group">
                                            <label for="country">Pais</label>
                                                <select class="form-control search-fields" id="country" name="country_id">
                                                    @foreach ($countries as $country)
                                                        <option value="{{ $country->id }}"
                                                            @if (old('country_id'))
                                                                @if (old('country_id') == $country->id)
                                                                    selected
                                                                @endif
                                                            @elseif ($country->id == 1)
                                                                selected
                                                            @endif
                                                            >
                                                            {{ $country->name }}
                                                        </option>
                                                    @endforeach
                                                </select>
                                        </div>
                                    </div>
                                    <div class="col-lg-4 col-md-4 col-sm-12">
                                        <div class="form-group">
                                            <label for="province">Provincia</label>
                                            <select class="form-control search-fields" id="province" name="province_id">
                                                @foreach ($provinces as $province)
                                                    @if (old('country_id') && old('country_id') == $province->country_id)
                                                        <option value="{{ $province->id }}"
                                                            @if (old('province_id') == $province->id)
                                                                selected
                                                            @endif
                                                            >
                                                            {{ $province->name }}
                                                        </option>
                                                    @elseif ($province->country_id == 1)
                                                        <option value="{{ $province->id }}" @if ($province->id == 1) selected @endif>
                                                            {{ $province->name }}
                                                        </option>
                                                    @endif
                                                @endforeach
                                            </select>
                                        </div>
                                    </div>
                                    <div id="divLocalitie" class="col-lg-4 col-md-4 col-sm-12">
                                        <div class="form-group">
                                            <label for="state">Estado/Localidad</label>
                                            <select class="form-control search-fields" id="localitie" name="localitie_id"> 
                                                    <option value="all" >Todas las localidades</option>
                                                @foreach ($localities as $localitie)
                                                    @if (old('province_id') && old('province_id') == $localitie->province_id)
                                                        <option value="{{ $localitie->id }}"
                                                            @if (old('localitie_id') == $localitie->id)
                                                                selected
                                                            @endif
                                                            >
                                                            {{ $localitie->name }}
                                                        </option>
                                                    @elseif ($localitie->province_id == 1)
                                                        <option value="{{ $localitie->id }}" @if ($localitie->id == 1) selected @endif>
                                                            {{ $localitie->name }}
                                                        </option>
                                                    @endif
                                                @endforeach
                                            </select>
                                        </div>
                                    </div>
                                    <div id="divCity" class="col-lg-4 col-md-4 col-sm-12">
                                        <div class="form-group">
                                            <label for="city">Barrio</label>
                                            <select class="form-control search-fields" id="city" name="city_id">    
                                                            <option value="all" selected>Todos lo Barrios</option>
                                                @foreach ($cities as $city)
                                                        @if (old('localitie_id') && old('localitie_id') == $city->localitie_id)
                                                            <option value="{{ $city->id }}"
                                                                @if (old('city_id') == $city->id)
                                                                    selected
                                                                @endif
                                                                >
                                                                {{ $city->name }}
                                                            </option>
                                                        @elseif ($city->locality_id == 1)
                                                            <option value="{{ $city->id }}">
                                                                {{ $city->name }}
                                                            </option>
                                                        @endif
                                                @endforeach
                                            </select>
                                        </div>
                                    </div>  

                                </div>
                            </div>
                        
                            <div class="col-lg-4 col-md-12 col-sm-12">
                                <button type="submit" id="submitAll" class="btn btn-md button-theme btnSubmit">Buscar</button>
                            </div>
                       
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- Services end -->
@section('scripts')
        <script>
            $(document).ready(function() {
    
                $('#divForm').hide();
            

            });
            $('#backFrm').click(function(){
                $('#rowSearchUpload').show();
                $('#divForm').hide();
            });
            

            
            $('#searchRealestate').click(function(){
                $('#rowSearchUpload').hide();
                $('#divForm').show();
            });
        
            $('#country').change(function(){
                    var id = $(this).val();
                    var listItems = '<option selected="selected" value="">Seleccionar</option>';
                    $.get( "/"+id+"/get_provinces", function( data ) {
                    
                        var list = data.provinces;
                        for (var i = 0; i < list.length; i++) {
                            listItems += "<option value='" + list[i].id + "'>" + list[i].name + "</option>";
                        }
                    
                        $('#province').html(listItems);
                        $('#divLocalitie').hide();
                        $('#divCity').hide();
                    });
                });

                $('#province').change(function(){
                    var id = $(this).val();
                    var listItems = '<option selected="selected" value="">Seleccionar</option>';
                    $.get( "/"+id+"/get_localities", function( data ) {
                    
                        var list = data.localities;
                        console.log(data.localities,list.length);
                        if (list.length != 0) {
                            for (var i = 0; i < list.length; i++) {
                            listItems += "<option value='" + list[i].id + "'>" + list[i].name + "</option>";
                            }
                            $('#localitie').html(listItems);
                            $('#divLocalitie').show();
                            $('#divCity').hide();
                        }else{
                            $('#divLocalitie').hide();
                            $('#divCity').hide();
                        }
                        
                    });
                });

                $('#localitie').change(function(){
                    var id = $(this).val();
                    console.log(id,'id de barrios')
                    var listItems = '<option selected="selected" value="">Seleccionar</option>';
                    $.get( "/"+id+"/get_cities", function( data ) {
                        console.log(data,'barrios')
                        var list = data.cities;
                        if (list.length != 0) {
                            for (var i = 0; i < list.length; i++) {
                                listItems += "<option value='" + list[i].id + "'>" + list[i].name + "</option>";
                            }
                            $('#city').html(listItems);
                            $('#divCity').show();
                        }else{
                            $('#divCity').hide();
                        }
                        
                    
                        
                    });
                });


        </script>

@endsection


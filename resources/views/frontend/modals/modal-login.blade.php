<div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog " role="document">
    <div class="modal-content">
      <div class="modal-header">
        <!--<div class="btn-group btn-group-lg  ">
          <button type="button" class="btn btn-outline-secondary ">Quiero Buscar</button>
          <button type="button" class="btn btn-outline-warning  ">Quiero Publicar</button>
        </div>-->
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <div class="form-content-box">
          <strong><h3> Iniciar Sesion</h3></strong>
          <div class="btn-group">
            <a class='btn btn-danger disabled '><i class="fa fa-google-plus" style="width:20px; height:20px"></i></a>
            <a class='btn btn-danger' href='' style="width:18em;"> Continuar con Google</a>
          </div>
          <br /><br />
          <div class="btn-group">
            <a class='btn btn-primary disabled '><i class="fa fa-facebook" style="width:16px; height:20px"></i></a>
            <a class='btn btn-primary ' href='' style="width:18em"> Continuar con Facebook</a>
          </div>  
          <br /><br />
            <!-- Form start -->
          <h7 align="center">Registrarme con Email</h7>
          <form action="index.html" method="GET">
            <div class="form-group">
              <label> Email: </label>
              <input type="email" name="email" class="input-text" placeholder="nombre@mail.com">
            </div>
            <div class="form-group">
              <input type="password" name="Password" class="input-text" placeholder="Usa de 6 a 20 Caracteres">
            </div>
            <div class="checkbox">
              <a href="forgot-password.html" class="link-not-important pull-right">Olvido su Contraseña</a>
              <div class="clearfix"></div>
            </div>
            <div class="form-group mb-0">
                    <button type="submit" class="btn-md button-theme btn-block">Registrarme</button>
                </div>
          </form>
                        
        </div>      
    </div>
</div>
  

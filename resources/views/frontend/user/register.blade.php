<!DOCTYPE html>
<html lang="zxx">
<head>
    <title>Inmobiliaria Augusto</title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta charset="utf-8">

    <!-- External CSS libraries -->
    <link rel="stylesheet" type="text/css" href="{{asset('templante/HTML/css/bootstrap.min.css')}}">
    <link rel="stylesheet" type="text/css" href="{{asset('templante/HTML/fonts/font-awesome/css/font-awesome.min.css')}}">
    <link rel="stylesheet" type="text/css" href="{{asset('templante/HTML/fonts/linearicons/style.css')}}">

    <!-- Custom stylesheet -->
    <link rel="stylesheet" type="text/css" href="{{asset('templante/HTML/css/style.css')}}">
    <link rel="stylesheet" type="text/css" id="style_sheet" href="{{asset('templante/HTML/css/skins/default.css')}}">

    <!-- Favicon icon -->
    <link rel="shortcut icon" href="{{asset('templante/HTML/img/favicon.ico')}}" type="image/x-icon" >

    <!-- Google fonts -->
    <link rel="stylesheet" type="text/css" href="https://fonts.googleapis.com/css?family=Raleway:300,400,500,600,300,700">

    <!-- IE10 viewport hack for Surface/desktop Windows 8 bug -->
    <link rel="stylesheet" type="text/css" href="{{asset('templante/HTML/css/ie10-viewport-bug-workaround.css')}}">

    <!-- Just for debugging purposes. Don't actually copy these 2 lines! -->
    <!--[if lt IE 9]><script  src="js/ie8-responsive-file-warning.js"></script><![endif]-->
    <script  src="{{asset('templante/HTML/js/ie-emulation-modes-warning.js')}}"></script>

    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
    <script  src="js/html5shiv.min.js"></script>
    <script  src="js/respond.min.js"></script>
    <![endif]-->
</head>
<body>

<!-- Contact section start -->
<div class="contact-section overview-bgi" style="padding-top: 30px;">
    <div class="container">
        <div class="row">
            <div class="col-lg-12">
                <!-- Form content box start -->
                <div class="form-content-box">
                    <!-- details -->
                    <div class="details">
                        <!-- Logo-->
                        <a href="/">
                            <img src="{{asset('img/logo.png')}}" class="cm-logo" alt="LOGO INMOBILIARIA">
                        </a>
                        <!-- Name -->
                        <h3>Crear Nueva Cuenta</h3>
                        @if ($errors->any())
                            <div class="alert alert-danger">
                                <ul>
                                    @foreach ($errors->all() as $error)
                                        <li>{{ $error }}</li>
                                    @endforeach
                                </ul>
                            </div>
                        @endif
                        <!-- Form start-->
                        <form action="{{ url('register') }}" method="POST">
                            @csrf
                            <div class="form-group">
                                <input type="text" name="name" class="input-text" placeholder="Nombre Completo">
                            </div>
                            <div class="form-group">
                                <input type="email" name="email" class="input-text" placeholder="Correo Electronico">
                            </div>
                            <div class="form-group">
                                <input type="password" name="password" class="input-text" placeholder="Contraseña">
                            </div>
                            <div class="form-group">
                                <input type="password" name="password_confirmation" class="input-text" placeholder="Repetir Contraseña">
                            </div>
                            <div class="form-group mb-0">
                                <button type="submit" class="btn-md button-theme btn-block">Registrarse</button>
                            </div>
                        </form>
                        <!-- Social List -->
                        
                    </div>
                    <!-- Footer -->
                    <div class="footer">
                        <span>Ya Posees una Cuenta? <a href="{{ url('login') }}">Iniciar Sesion</a></span>
                    </div>
                </div>
                <!-- Form content box end -->
            </div>
        </div>
    </div>
</div>
<!-- Contact section end -->

<!-- Full Page Search -->
<div id="full-page-search">
    <button type="button" class="close">×</button>
    <form action="index.html#">
        <input type="search" value="" placeholder="type keyword(s) here" />
        <button type="submit" class="btn btn-sm button-theme">Search</button>
    </form>
</div>

<script src="{{asset('templante/HTML/js/jquery-2.2.0.min.js')}}"></script>
<script src="{{asset('templante/HTML/js/bootstrap.min.js')}}"></script>
<script  src="{{asset('templante/HTML/js/app.js')}}"></script>

<!-- IE10 viewport hack for Surface/desktop Windows 8 bug -->
<script  src="{{asset('templante/HTML/js/ie10-viewport-bug-workaround.js')}}"></script>
<!-- Custom javascript -->
<script  src="{{asset('templante/HTML/js/ie10-viewport-bug-workaround.js')}}"></script>
</body>
</html>
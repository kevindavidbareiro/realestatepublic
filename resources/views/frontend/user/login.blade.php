
<!DOCTYPE html>
<html lang="zxx">
<head>
    <title>Inmobiliaria Augusto</title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta charset="utf-8">

    <!-- External CSS libraries -->
    <link rel="stylesheet" type="text/css" href="{{asset('templante/HTML/css/bootstrap.min.css')}}">
    <link rel="stylesheet" type="text/css" href="{{asset('templante/HTML/fonts/font-awesome/css/font-awesome.min.css')}}">
    <link rel="stylesheet" type="text/css" href="{{asset('templante/HTML/fonts/linearicons/style.css')}}">
    <link rel="stylesheet" type="text/css" href="{{asset('css/style.css')}}">

    <!-- Custom stylesheet -->
    <link rel="stylesheet" type="text/css" href="{{asset('templante/HTML/css/style.css')}}">
    <link rel="stylesheet" type="text/css" id="style_sheet" href="{{asset('templante/HTML/css/skins/default.css')}}">

    <!-- Favicon icon -->
    <link rel="shortcut icon" href="{{asset('templante/HTML/img/favicon.ico')}}" type="image/x-icon" >

    <!-- Google fonts -->
    <link rel="stylesheet" type="text/css" href="https://fonts.googleapis.com/css?family=Raleway:300,400,500,600,300,700">

    <!-- IE10 viewport hack for Surface/desktop Windows 8 bug -->
    <link rel="stylesheet" type="text/css" href="{{asset('templante/HTML/css/ie10-viewport-bug-workaround.css')}}">

    <!-- Just for debugging purposes. Don't actually copy these 2 lines! -->
    <!--[if lt IE 9]><script  src="js/ie8-responsive-file-warning.js"></script><![endif]-->
    <script  src="{{asset('templante/HTML/js/ie-emulation-modes-warning.js')}}"></script>

    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
    <script  src="js/html5shiv.min.js"></script>
    <script  src="js/respond.min.js"></script>
    <![endif]-->
</head>
<body  class="text-center">
<div class="page_loader"></div>


<!-- Contact section start -->
<div class="contact-section overview-bgi" style="padding-top: 30px;">
        <div class="row">
            <div class="col-lg-12">
                <!-- Form content box start -->
                <div class="form-content-box">
                    <!-- details -->
                    <div class="details">
                        <!-- Logo -->
                        <a href="/">
                            <img class="logo-realestate" src="{{asset('img/logo.png')}}" class="cm-logo" alt="lOGO INMOBILIARIA">
                        </a>
                        <!-- Name -->
                        <h3>Inciar Sesion</h3>
                         @if (session('errors'))
                            <div class="alert alert-danger">
                                {{ session('errors') }}
                            </div>      
                        @endif
                        <!-- Form start -->
                        <form action="{{ url('login') }}" method="POST">
                            @csrf
                            <div class="form-group">
                                <input type="email" name="email" class="input-text" placeholder="correo@email.com">
                            </div>
                            <div class="form-group">
                                <input type="password" name="password" class="input-text" placeholder="Password">
                            </div>
                            <div class="checkbox">
                                <div class="ez-checkbox pull-left">
                                    <label>
                                        <input type="checkbox" class="ez-hide">Recordar
                                    </label>
                                </div>
                                <a href="forgot-password.html" class="link-not-important pull-right">Recuperar Contraseña</a>
                                <div class="clearfix"></div>
                            </div>
                            <div class="form-group mb-0">
                                <button type="submit" class="btn-md button-theme btn-block">Iniciar Sesion</button>
                            </div>
                        </form>
                        <!-- Social List -->
                        <ul class="social-list clearfix">
                            <li><a href="#" class="facebook-bg"><i class="fa fa-facebook"></i></a></li>
                            <li><a href="#" class="twitter-bg"><i class="fa fa-twitter"></i></a></li>
                            <li><a href="#" class="google-bg"><i class="fa fa-google-plus"></i></a></li>
                            <li><a href="#" class="linkedin-bg"><i class="fa fa-linkedin"></i></a></li>
                        </ul>
                    </div>
                    <!-- Footer -->
                    <div class="footer">
                        <span>Todavia No Te Registraste? <a href="{{ url('register') }}">Crear Cuenta</a></span>
                    </div>
                </div>
                <!-- Form content box end -->
            </div>
        </div>
</div>
<!-- Contact section end -->

<!-- Full Page Search -->
<div id="full-page-search">
    <button type="button" class="close">×</button>
    <form action="index.html#">
        <input type="search" value="" placeholder="type keyword(s) here" />
        <button type="submit" class="btn btn-sm button-theme">Search</button>
    </form>
</div>


<script src="{{asset('templante/HTML/js/jquery-2.2.0.min.js')}}"></script>
<script src="{{asset('templante/HTML/js/bootstrap.min.js')}}"></script>
<script  src="{{asset('templante/HTML/js/app.js')}}"></script>

<!-- IE10 viewport hack for Surface/desktop Windows 8 bug -->
<script  src="{{asset('templante/HTML/js/ie10-viewport-bug-workaround.js')}}"></script>
<!-- Custom javascript -->
<script  src="{{asset('templante/HTML/js/ie10-viewport-bug-workaround.js')}}"></script>
</body>
</html>
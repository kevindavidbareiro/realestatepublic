@extends('frontend.dashboard.masterDashboard')

@section('stylus')


@endsection


@section('content_dashboard')
        
                        <div class="dashboard-header clearfix">
                            <div class="row">
                                <div class="col-sm-12 col-md-6"><h4>Mi Perfil</h4></div>
                                <div class="col-sm-12 col-md-6">
                                    <div class="breadcrumb-nav">
                                        <ul>
                                            <li>
                                                <a href="{{ url('/') }}">Inicio</a>
                                            </li>
                                            <li>
                                                <a href="{{ url('panel') }}">Panel</a>
                                            </li>
                                            <li class="active">Mi Perfil</li>
                                        </ul>
                                    </div>
                                </div>
                            </div>
                        </div>
                        @if ($errors->any())
                            <div class="alert alert-danger">
                                <ul>
                                    @foreach ($errors->all() as $error)
                                        <li>{{ $error }}</li>
                                    @endforeach
                                </ul>
                            </div>
                        @endif
                            @if (session('success')) 
                                <div class="alert alert-success">
                                    {{ session('success') }}
                                </div>
                            @endif    
                            @if (session('error'))
                                <div class="alert alert-danger">
                                    {{ session('error') }}
                                </div>
                            @endif    
                        <div class="dashboard-list">
                            <h3 class="heading">Detalles</h3>
                            <div class="dashboard-message contact-2 bdr clearfix">
                                <div class="row">
                                    <div class="col-lg-3 col-md-3">
                                        <!-- Edit profile photo -->
                                        <form action="{{ url('/usuario/perfil') }}" method="POST" enctype="multipart/form-data">
                                            @csrf
                                        <div class="edit-profile-photo">
                                            <img src="@if ($user[0]->imgProfile)
                                                 {{ asset('img/user/'.$user[0]->imgProfile.'/') }}
                                            @else
                                                {{ asset('img/user/noavatar.png') }} 
                                            @endif" id="avatarmuestra" alt="profile-photo" class="img-fluid">
                                            <div class="change-photo-btn">
                                                <div class="photoUpload">
                                                    <span><i class="fa fa-upload"></i></span>
                                                    <input type="file" class="upload"  id="imagen" name="avatar">
                                                </div>
                                            </div>
                                        </div>

                                    </div>
                                    <div class="col-lg-9 col-md-9">
                                        
                                            <div class="row">
                                                <div class="col-lg-6 col-md-6">
                                                    <div class="form-group name">
                                                        <label>Nombre</label>
                                                        <input type="text" name="name" class="form-control input-text " id="name"  @if (old('name')) value="{{ old('name') }}" @else value="{{ $user[0]->name }}"@endif placeholder="Nombre">
                                                         <p class="text-danger">{{ $errors->first('name') }}</p>
                                                    </div>
                                                </div>
                                                <div class="col-lg-6 col-md-6">
                                                    <div class="form-group email">
                                                        <label>Email</label>
                                                        <input type="email" name="email" class="form-control"  placeholder="Email" value="{{ $user[0]->email }}" readonly="readonly">
                                                    </div>
                                                </div>
                                                <div class="col-lg-6 col-md-6">
                                                    <div class="form-group number">
                                                        <label>Celular</label>
                                                        <input type="text" name="celular" class="form-control" placeholder="Celular" value="@if ( count($userCelular) > 0){{ $userCelular[0]->contact }}@endif">
                                                        <p class="text-danger">{{ $errors->first('celular') }}</p>
                                                    </div>
                                                </div>
                                                <div class="col-lg-6 col-md-6">
                                                    <div class="form-group number">
                                                        <label>Telefono</label>
                                                        <input type="text" name="phone" class="form-control" placeholder="Telefono" value="@if ( count($userTelephone) > 0){{ $userTelephone[0]->contact }} @endif">
                                                        <p class="text-danger">{{ $errors->first('phone') }}</p>
                                                    </div>
                                                </div>
                                                
                                                <div class="col-lg-12 col-md-12 col-sm-12">
                                                    <div class="form-group message">
                                                        <label>Informacion Personal</label>
                                                        <textarea class="form-control" name="infoPersonal" placeholder="Informacion Personal">@if ($userData){{ $userData }} @endif</textarea>
                                                        <p class="text-danger">{{ $errors->first('infoPersonal') }}</p>
                                                    </div>
                                                </div>
                                                <div class="col-lg-12">
                                                    <div class="send-btn">
                                                        <button type="submit" class="btn btn-md button-theme">Guardar Cambios</button>
                                                    </div>
                                                </div>
                                            </div>
                                        </form>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-6">
                                <div class="dashboard-list">
                                    <h3 class="heading">Cambiar contraseña</h3>
                                    <div class="dashboard-message contact-2">
                                        <form action="{{ url('usuario/changepass') }}" method="POST" enctype="multipart/form-data">
                                            @csrf
                                            <div class="row">
                                                <div class="col-lg-12">
                                                    <div class="form-group name">
                                                        <label>Contraseña Actual</label>
                                                        <input type="password" name="current_password" class="form-control" placeholder="Contraseña Actual">
                                                        <p class="text-danger">{{ $errors->first('current_password') }}</p>
                                                    </div>
                                                </div>
                                                <div class="col-lg-12">
                                                    <div class="form-group email">
                                                        <label>Nueva Contraseña</label>
                                                        <input type="password" name="new_password" class="form-control" placeholder="Nueva Contraseña">
                                                    </div>
                                                </div>
                                                <div class="col-lg-12">
                                                    <div class="form-group subject">
                                                        <label>Confirmar Nueva Contraseña</label>
                                                        <input type="password" name="new_password_confirmation" class="form-control" placeholder="Confirmar Nueva Contraseña">
                                                    </div>
                                                </div>
                                                <div class="col-lg-12">
                                                    <div class="send-btn">
                                                        <button type="submit" class="btn btn-md button-theme">Cambiar Contraseña</button>
                                                    </div>
                                                </div>
                                            </div>
                                        </form>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="dashboard-list">
                                    <h3 class="heading">Redes Sociales</h3>
                                    <div class="dashboard-message contact-2">
                                        <form action="{{ url('usuario/social') }}" method="POST" enctype="multipart/form-data">
                                            @csrf
                                            <div class="row">
                                                <div class="col-lg-12">
                                                    <div class="form-group name">
                                                        <label>Facebook</label>
                                                        <input type="text" name="facebook" class="form-control" placeholder="https://www.facebook.com" value="@if ( count($userFacebook) > 0){{ $userFacebook[0]->contact }} @endif">
                                                    </div>
                                                </div>
                                                <div class="col-lg-12">
                                                    <div class="form-group subject">
                                                        <label>Instagram</label>
                                                        <input type="text" name="instagram" class="form-control" placeholder="https://instagram.com" value="@if ( count($userInstagram) > 0){{ $userInstagram[0]->contact }} @endif">
                                                    </div>
                                                </div>
                                                <div class="col-lg-12">
                                                    <div class="form-group number">
                                                        <label>Whatsapp</label>
                                                        <input type="text" name="whatsapp" class="form-control" placeholder="https://www.whatsapp.com" value="@if ( count($userWhatsapp) > 0){{ $userWhatsapp[0]->contact }} @endif">
                                                    </div>
                                                </div>
                                                <div class="col-lg-12">
                                                    <div class="send-btn">
                                                        <button type="submit" class="btn btn-md button-theme">Guardar Cambios</button>
                                                    </div>
                                                </div>
                                            </div>
                                        </form>
                                    </div>
                                </div>
                            </div>
                        </div>

@endsection


@section('script')
<script>
    function avatarPreview(input) {
      if (input.files && input.files[0]) {
        var reader = new FileReader();
        reader.onload = function(e) {
          // Asignamos el atributo src a la tag de imagen
          $('#avatarmuestra').attr('src', e.target.result);
        }
        reader.readAsDataURL(input.files[0]);
      }
    }

    $("#imagen").change(function() {
      avatarPreview(this);
    });


</script>

@endsection
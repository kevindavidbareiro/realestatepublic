@extends('frontend.master')

@section('stylus')
<style>
/* Color Negro letra Menu del Menu */
.sticky-header .navbar-expand-lg .navbar-nav .nav-link {
    color: #515151;
}

</style>

@endsection


@section('content_frontend')
<!-- Properties details page start -->
<div class="properties-details-page content-area">
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                    @if (session('typemsg'))
                        @if (session('typemsg') == 'success')
                            <div class="alert alert-success" role="alert">
                             <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                <span aria-hidden="true">&times;</span>
                             </button>
                                <strong>{{ session('message') }}</strong>
                            </div>
                        @endif
                        @if (session('typemsg') == 'error' )
                            <div class="alert alert-danger" role="alert">
                                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                    <span aria-hidden="true">&times;</span>
                                 </button>
                                <strong>{{ session('message') }}</strong>
                            </div>
                        @endif  
                    @endif 
                <!-- Heading properties 3 start -->
                <div class="heading-properties-3">
                    <h1>{{ $myProperty->name }}</h1>
                    <div class="mb-30"><span class="property-price">${{ $myProperty->price }}</span> <span class="rent">{{ $myProperty->type_of_realestate->name }}</span> <span class="location"><i class="flaticon-pin"></i>{{ $myProperty->address }}</span></div>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-lg-8 col-md-12">
                <!-- main slider carousel items -->
                <div id="propertiesDetailsSlider" class="carousel properties-details-sliders slide mb-40">
                    @if ($countMultimedia  > 0)
                        <div class="carousel-inner">
                            <div class="active item carousel-item" data-slide-number="0">
                                <img src="{{ asset('img/realestates/'.$multimedia[0]->url) }}" class="img-fluid" alt="slider-properties" height="486" width="730">
                            </div>
                            @for ($i = 1; $i < $countMultimedia; $i++)
                                <div class="item carousel-item" data-slide-number="{{ $i }}">
                                    <img src="{{ asset('img/realestates/'.$multimedia[$i]->url) }}" class="img-fluid" alt="slider-properties" height="486" width="730">
                                </div>
                            @endfor
                            

                            <a class="carousel-control left" href="#propertiesDetailsSlider" data-slide="prev"><i class="fa fa-angle-left"></i></a>
                            <a class="carousel-control right" href="#propertiesDetailsSlider" data-slide="next"><i class="fa fa-angle-right"></i></a>

                        </div>
                        <!-- main slider carousel nav controls -->
                        <ul class="carousel-indicators smail-properties list-inline nav nav-justified">
                            <li class="list-inline-item active">
                                <a id="carousel-selector-0" class="selected" data-slide-to="0" data-target="#propertiesDetailsSlider">
                                    <img src="{{ asset('img/realestates/'.$multimedia[0]->url) }}" class="img-fluid" alt="properties-small" height="97" width="146">
                                </a>
                            </li>
                            @for ($i = 1; $i < $countMultimedia; $i++)
                                <li class="list-inline-item">
                                    <a id="carousel-selector-.{{ $i }}" data-slide-to="{{ $i }}" data-target="#propertiesDetailsSlider">
                                        <img src="{{  asset('img/realestates/'.$multimedia[$i]->url) }}" class="img-fluid" alt="properties-small" height="97" width="146">>
                                    </a>
                                </li>
                            @endfor 
                        </ul>
                    <!-- main slider carousel items -->
                    @else
                    {{ "No Hay Imagenes cargadas" }}
                    @endif

                    <!-- main slider carousel items -->
                </div>
                
                <!-- Properties description start -->
                <div class="properties-description mb-40">
                    <h3 class="heading-2">
                        Descipción Detallada
                    </h3>
                    <p>{{ $myProperty->detailedInformation }}.</p>
                </div>
                <!-- Properties amenities start -->
                <div class="properties-amenities mb-40">
                    <h3 class="heading-2">
                        Caracteristicas:
                        {{ $countProperties }}
                    </h3>
                    <div class="row">
                        @for ($i = 0; $i  < $countProperties; $i++)
                            @if ($i < 5 )
                                <div class="col-lg-4 col-md-4 col-sm-4 col-xs-12">
                                    <ul class="amenities">
                                        <li>
                                            <i class="fa fa-check"></i>{{ $myProperty->option[$i]->name }}
                                        </li>
                                    </ul>
                                </div>
                            @endif

                            @if ($i >= 5 && $i < 10 )      
                                <div class="col-lg-4 col-md-4 col-sm-4 col-xs-12">
                                    <ul class="amenities">
                                        <li>
                                            <i class="fa fa-check"></i>{{ $myProperty->option[$i]->name }}
                                        </li>
                                    </ul> 
                                </div> 
                            @endif    
                            
                            @if ($i >= 10 && $i < 15 )  
                                <div class="col-lg-4 col-md-4 col-sm-4 col-xs-12">
                                    <ul class="amenities">
                                        <li>
                                            <i class="fa fa-check"></i>{{ $myProperty->option[$i]->name }}
                                         </li>
                                    </ul> 
                                </div>     
                            @endif
                        @endfor 
                    </div>
                </div>
                <!-- Floor plans start -->
                <div class="floor-plans mb-50">
                    <h3 class="heading-2">Planos de la Propiedad</h3>
                    <table>
                        <tbody><tr>
                            <td><strong>Superficie</strong></td>
                            <td><strong>Habitaciones</strong></td>
                            <td><strong>Baño/s</strong></td>
                            <td><strong>Garage</strong></td>
                        </tr>
                        <tr>
                            <td>{{ $myProperty->area }}</td>
                            <td>{{ $myProperty->bedrooms }}</td>
                            <td>{{ $myProperty->bathrooms }}</td>
                            <td>1</td>
                        </tr>
                        </tbody>
                    </table>
                    {{-- PLANOS <img src="http://placehold.it/730x379" alt="floor-plans" class="img-fluid"> --}}
                </div>
                <!-- Location start -->
                <div class="location mb-50">
                    <div class="map">
                        <h3 class="heading-2">Ubicación de la propiedad</h3>
                        <div id="map" style="height: 200px" class="contact-map" ></div>
                    </div>
                </div>
                <!-- Inside properties start -->
                <div class="inside-properties mb-50">
                    
                    @if (count($propertyVideo) > 0)
                        <h3 class="heading-2">
                            Video de la propiedad
                        </h3>
                        {{-- <iframe src="https://www.youtube.com/embed/5e0LxrLSzok" allowfullscreen=""></iframe> --}}
                        {{ $propertyVideo[0]->url  }}
                    @endif
                </div>
                @if (count($Comments) > 0)
                <h3 class="heading-2">Tus Comentarios:</h3>
                    <!-- Comments start -->
                    @foreach ($Comments as $Comment)
                        {{-- expr --}}
                    
                    <ul class="comments">
                        <li>
                            <div class="comment">
                                <div class="comment-author">
                                    <a href="#">
                                        <img src="{{ asset('img/user/'.$Comment->user->imgProfile.'/') }}" alt="comments-user">
                                    </a>
                                </div>
                                <div class="comment-content">
                                    <div class="comment-meta">
                                        <h3>
                                            {{ $Comment->user->name }}
                                        </h3>
                                        <div class="comment-meta">
                                            {{ $Comment->created_at }}
                                        </div>
                                    </div>
                                    <p>{{ $Comment->comment }}</p>
                                </div>
                            </div>
                            @if (count($Comment->reply) > 0)
                            <ul>
                                <li>
                                    <div class="comment">
                                        <div class="comment-author">
                                            <a href="#">
                                                <img src="{{ asset('img/user/'.$Comment->reply[0]->user->imgProfile.'/') }}" alt="comments-user">
                                            </a>
                                        </div>
                                        <div class="comment-content">
                                            <div class="comment-meta">
                                                <h3>
                                                    {{ $Comment->reply[0]->user->name }}
                                                </h3>
                                                <div class="comment-meta">
                                                    {{ $Comment->reply[0]->created_at }}
                                                </div>
                                            </div>
                                            <p>{{ $Comment->reply[0]->reply }}</p>
                                        </div>
                                    </div>
                                </li>
                            </ul>
                            @endif
                        </li>
                    </ul>    
                    @endforeach        
                @endif       
               <!-- Contact 2 start -->
               @if (auth()->check())
                <div class="contact-2 ca mtb-50">
                    <h3 class="heading">Enviar un Mensaje</h3>
                    <form action="{{ url('propiedades/comment/'.$myProperty->id.'/') }}" method="POST" enctype="multipart/form-data">
                        @csrf
                        <div class="row">
                            <div class="form-group message col-md-12">
                                <textarea class="form-control" name="comment" placeholder="Escribir mensaje"></textarea>
                            </div>
                            <div class="send-btn col-md-12">
                                <button type="submit" class="btn btn-md button-theme">Enviar Mensaje</button>
                            </div>
                        </div>
                    </form>
                </div>
               @endif 

            </div>
            <!-- Our agent sidebar right -->
            <div class="col-lg-4 col-md-12">
                <div class="sidebar-right">
                    <!-- Our agent sidebar start -->
                    <div class="our-agent-sidebar">
                        <div class="p-20">
                            <h3 class="sidebar-title"> Datos Personales </h3>
                            <div class="s-border"></div>
                        </div>
                        <div id="carouselExampleIndicators5" class="carousel slide text-center" data-ride="carousel">
                            <div class="carousel-inner">
                                <div class="carousel-item active">
                                    <div class="team-1">
                                        <div class="team-photo">
                                            <a href="">
                                                <img  src="@if ($informationPersonal[0]->imgProfile)
                                                 {{ asset('img/user/'.$informationPersonal[0]->imgProfile.'/') }} @else {{ asset('img/black-logo.png') }} @endif" alt="agent-2" class="img-fluid">
                                            </a>
                                            <ul class="social-list clearfix">
                                            @if (count($userFacebook) > 0)
                                                <li><a href="{{$userFacebook[0]->contact }}" class="facebook"><i class="fa fa-facebook"></i></a></li>
                                            @endif
                                            @if (count($userInstagram) > 0)     
                                                <li><a href="{{ $userInstagram[0]->contact }}" class="instagram"><i class="fa fa-instagram"></i></a></li>
                                            @endif    
                                            </ul>
                                        </div>
                                        <div class="team-details">
                                            <h5><a href="agent-detail.html">{{ $informationPersonal[0]->name }}</a></h5>
                                            <h6>
                                                @if (count($informationPersonal) > 0) {{ $informationPersonal[0]->data }}  
                                                @endif
                                            </h6>
                                            <h4>@if (count($userCelular) > 0) {{ $userCelular[0]->contact }}  
                                                @endif
                                            </h4>
                                            
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- Recent properties start -->
                    <div class="widget recent-properties">
                        <h3 class="sidebar-title">Propiedades Recientes</h3>
                        <div class="s-border"></div>
                            @foreach (array_reverse($lastVisitedRealEstates) as $key => $lastvisited)
                             @if($key < 3)
                                @if($lastvisited['id'] != $myProperty->id )
                                    <div class="media mb-4">
                                        <a class="pr-3" href="{{ url('propiedades/'.$lastvisited['id'].'') }}">
                                            @foreach ($lastvisited['imageCover'] as $imageCover)    
                                                <img class="media-object" src="@if($imageCover) {{ asset('img/realestates/main/'.$imageCover->url.'') }} @endif" alt="small-properties">
                                            @endforeach    
                                        </a>
                                        <div class="media-body align-self-center">
                                            <h5>
                                                <a href="{{ url('propiedades/'.$lastvisited['id'].'') }}">{{ $lastvisited['name'] }}</a>
                                            </h5>
                                            <div class="listing-post-meta">
                                                ${{ $lastvisited['price']}} | <a href="#"><i class="fa fa-calendar"></i> {{ substr($lastvisited['created_at'],0,10) }} </a>
                                            </div>
                                        </div>
                                     </div>
                                @endif     
                            @endif     
                            @endforeach

                <!-- Similar Properties start -->
                    <div class="similar-properties">
                        <h3 class="sidebar-title">Propiedades Similares</h3>
                        <div class="s-border"></div>
                        @php
                            {{$count = 0;}}
                             {{ $idBedroom = 0; }}
                        @endphp
                        @foreach ($similarProperties as $key => $similarProperty)
                        @if($count < 3)
                            {{-- Compara que la propiedad tenga el mismo numero de habitaciones y sea de la misma provincia --}}
                            @if ($similarProperty->bedrooms == $myProperty->bedrooms and $similarProperty->id != $myProperty->id and $count < 1)
                                <div class="property-box">
                                    <div class="property-thumbnail">
                                        <a href="{{ url('propiedades/'.$similarProperty->id.'') }}" class="property-img">
                                            <div class="listing-badges">
                                                {{-- <span class="featured">Featured</span> --}}
                                            </div>
                                            @foreach ($similarProperty->multimedia as $multimedia)
                                              @if ($multimedia->main == 1)
                                                  <img class="d-block w-100" src="{{ asset('img/realestates/main/'.$multimedia->url.'') }}" alt="properties"> 
                                              @endif
                                            @endforeach
                                            
                                        </a>
                                    </div>
                                    <div class="detail">
                                        <h1 class="title">
                                            <a href="{{ url('propiedades/'.$similarProperty->id.'') }}">{{ $similarProperty->name }}</a>
                                            <br>
                                            <span>${{ number_format($similarProperty->price, 0, ',', '.') }} </span> 
                                        </h1>

                                        <div class="location">
                                            <a href="properties-details.html">
                                                <i class="flaticon-pin"></i>{{ $similarProperty->address }},
                                            </a>
                                        </div>
                                    </div>
                                    <ul class="facilities-list clearfix">
                                        <li>
                                            <span>Area</span>{{ $similarProperty->area }},m2
                                        </li>
                                        <li>
                                            <span>Beds</span> {{ $similarProperty->bedrooms }}
                                        </li>
                                        <li>
                                            <span>Baths</span> {{ $similarProperty->bathrooms }}
                                        </li>
                                        <li>
                                            <span>Garage</span> 1
                                        </li>
                                    </ul>
                                </div>
                                @php
                                    {{ $idBedroom = $similarProperty->id; }}
                                    {{ $count = $count + 1; }}
                                @endphp
                            @endif
                            {{-- Compara que la propiedad este en un rango de precio (25% menor o mayor al precio y sea de la misma provincia --}}
                            @if ($similarProperty->price >= ($myProperty->price * 0.75) && $similarProperty->price <= ($myProperty->price * 1.25) and $count < 2)
                                @if($similarProperty->id != $myProperty->id and $similarProperty->id != $idBedroom)
                                    @php
                                        {{ $count = $count + 1; }}
                                    @endphp
                                    <div class="property-box">
                                        <div class="property-thumbnail">
                                            <a href="{{ url('propiedades/'.$similarProperty->id.'') }}" class="property-img">
                                                <div class="listing-badges">
                                                    {{-- <span class="featured">Featured</span> --}}
                                                </div>
                                                @foreach ($similarProperty->multimedia as $multimedia)
                                                  @if ($multimedia->main == 1)
                                                      <img class="d-block w-100" src="{{ asset('img/realestates/main/'.$multimedia->url.'') }}" alt="properties"> 
                                                  @endif
                                                @endforeach
                                                
                                            </a>
                                        </div>
                                        <div class="detail">
                                            <h1 class="title">
                                                <a href="{{ url('propiedades/'.$similarProperty->id.'') }}">{{ $similarProperty->name }}</a>
                                                <br>
                                                <span>${{ number_format($similarProperty->price, 0, ',', '.') }} </span> 
                                            </h1>

                                            <div class="location">
                                                <a href="properties-details.html">
                                                    <i class="flaticon-pin"></i>{{ $similarProperty->address }},
                                                </a>
                                            </div>
                                        </div>
                                        <ul class="facilities-list clearfix">
                                            <li>
                                                <span>Area</span>{{ $similarProperty->area }},m2
                                            </li>
                                            <li>
                                                <span>Beds</span> {{ $similarProperty->bedrooms }}
                                            </li>
                                            <li>
                                                <span>Baths</span> {{ $similarProperty->bathrooms }}
                                            </li>
                                            <li>
                                                <span>Garage</span> 1
                                            </li>
                                        </ul>
                                    </div>
                                @endif
                            @endif
                        @endif      
                        @endforeach    
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
    <!-- Properties details page end -->

@endsection

@section('scripts')
    <script async defer src="https://maps.googleapis.com/maps/api/js?key=AIzaSyDw86gsWyn7-EcV5m3fSqjs8hkfSWOZCwI&libraries=places&callback=initMap">
    </script>
    
<script>

        var markers = [];
        // var latitud = '';
        // var longitud = '';
        function initMap() {
            //Inicio del mapa y colocando en una direccion determinada
            var map = new google.maps.Map(document.getElementById('map'), {
            center: {lat: {{ $myProperty->latitude }}, lng: {{ $myProperty->longitude }} },
            zoom: 15
            });
            var marker = new google.maps.Marker({
                    map: map,
                    position: {lat: {{ $myProperty->latitude }}, lng: {{ $myProperty->longitude }} },
                });
            markers.push(marker);
       

            //Seccion para autocompletar las direcciones
            var input = document.getElementById('address');
            var autocomplete = new google.maps.places.Autocomplete(input);
            autocomplete.bindTo('bounds', map);
            autocomplete.setComponentRestrictions({'country': ['ar']});


            //Inicio del geocoding
            var geocoder = new google.maps.Geocoder();
            

            document.getElementById('submit').addEventListener('click', function() {
            geocodeAddress(geocoder, map);
            });
        }

        function geocodeAddress(geocoder, resultsMap) {
            
            var address = document.getElementById('address').value;
            geocoder.geocode({'address': address}, function(results, status) {
           
            if (status === 'OK') {
                resultsMap.setCenter(results[0].geometry.location);
                //ELimino los marcadores
                deleteMarkers();
                //Creo un marcador de la direccion que tipeo el usuario
                var marker = new google.maps.Marker({
                map: resultsMap,
                position: results[0].geometry.location
                });
                markers.push(marker);
                // Guardo el valor de latitud y longitud en sus respectivas variables
                // latitud = results[0].geometry.location.lat();
                // longitud = results[0].geometry.location.lng();
                // convertirGpsADireccion(latitud, longitud, mostrarDireccion);
                //Muestro las cordenadas de latitud y longitud en los inpus correspondientes
                document.getElementById("latitude").value = results[0].geometry.location.lat();
                document.getElementById("longitude").value = results[0].geometry.location.lng();
            } else {
                alert('Geocode no tuvo éxito por la siguiente razón: ' + status);
            }
            });
        }
        function setMapOnAll(map) {
            for (var i = 0; i < markers.length; i++) {
            markers[i].setMap(map);
            }
        }
        function clearMarkers() {
            setMapOnAll(null);
        } 
        function deleteMarkers() {
            clearMarkers();
            markers = [];
        }

        function convertirGpsADireccion(lat, lon, callback) {
            $.getJSON("http://nominatim.openstreetmap.org/reverse?" +
                      "format=json&amp;addressdetails=0&zoom=18&" +
                      "lat=" + lat + "&lon=" + lon + "&json_callback=?",
                callback);
        }

        /*function mostrarDireccion(response) {
            console.log(response);
            let message = "Por favor revisa que los datos sean correctos. De lo contrario corregilos"
            $('#alert_placeholder').html('<div class="alert alert-warning" role="alert"><button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>'+message+'</div>')
            document.getElementById("country").value = response.address.country;
            document.getElementById("province").value = response.address.state;
            document.getElementById("state").value = response.address.state_district;
            document.getElementById("neighbourhood").value = response.address.neighbourhood;
        }*/


</script>
@endsection 
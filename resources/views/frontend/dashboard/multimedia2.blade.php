<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <link rel="stylesheet" type="text/css" href="{{asset('templante/HTML/css/bootstrap.min.css')}}">
    <link rel="stylesheet" type="text/css" href="{{asset('plugin/dropzone/dropzone511.css')}}"/>
	<link rel="stylesheet" type="text/css" href="{{asset('plugin/gallery/galleryimages.css')}}"/>
    <link rel="stylesheet" type="text/css" href="{{asset('css/style2.css')}}"/>
    <link href="{{asset('templanteBack/metronic/default/assets/vendors/base/vendors.bundle.css')}}" rel="stylesheet" type="text/css" />
		<link href="{{asset('templanteBack/metronic/default/assets/demo/default/base/style.bundle.css')}}" rel="stylesheet" type="text/css" />
		<link rel="shortcut icon" href="{{asset('templanteBack/metronic/default/assets/demo/default/media/img/logo/favicon.ico')}}" />
    <title>Imperio Inmoviliario</title>
</head>
<body>
        <div class="m-portlet">
                <div class="m-portlet__head">
                    <div class="m-portlet__head-caption">
                        <div class="m-portlet__head-title">
                            <span class="m-portlet__head-icon m--hide">
                                <i class="la la-gear"></i>
                            </span>
                            <h3 class="m-portlet__head-text">
                                Multimedia
                            </h3>
                        </div>
                        
                    </div>
                </div>
            <div class="m-portlet__body">
                <div class="m-form m-form--label-align-right">
                    <div class="row align-items-center">
                        <div class="col-xl-12 order-1 order-xl-2 m--align-right">
                             <a href="{{ url('propiedades') }}" class="btn btn-danger">
                                <span>
                                    <i class="fa fa-list-alt"></i> Volver a la lista
                                </span>
                            </a> 
                        </div>
                    </div>
                               
                </div>
                <div class="portlet-title">
                    <div class="caption">
                        <i class="fa fa-picture-o"></i> Imagen de Portada
                    </div>
                </div>
                <div class="portlet-body form">
                    
                    @if(count($main) != 0)
                         <img class="portada" src="{{ asset('img/realestates/main/' . $main[0]->url .'/') }}" alt="">      
                    @else
                        <center>
                            <div class="notImage">
                                <i class="fa fa-frown-o iconoNotImg"></i>
                                <p class="text-primary">No contiene foto de portada</p>
                            </div>
                        </center>
                    @endif
                </div>
                   		
                <div class="portlet-title">
                    <div class="caption">
                        <i class="fa fa-pencil-square-o"></i> Carga de Imagenes
                    </div>
                </div>
                <div class="portlet-body form">
                    <div id="mensaje_afirmativo" class="alert alert-success" style="display:none">
                        <p>Se cargó correctamente. Actualizando en 3s.</p>
                    </div>
                    <div id="mensaje_error" class="alert alert-danger" style="display:none">
                        <p>Hubo un problema al cargar la imagen.</p>
                    </div>
                    <div class="form-body">
                            
                        <form action="{{url('Multimedia')}}" class="dropzone" method="post" id="my-dropzone" enctype="multipart/form-data">
                                <input type="hidden" name="_token" value="{{ csrf_token() }}">
                                <input type="hidden" name="idRealestate" value="{{ $idRealestate }}">
                                <div class="dz-message">
                                    <p class="text-info">Arrastra la/las imagen/s aquí, o haz click.</p>
                                </div>
                                <div class="fallback">
                                    <input type="file" name="file" multiple>
                                </div>
                                <div id="previews"></div>
                                <center>
                                    <button type="submit" class="btn btn-success" id="submit-all"><i class="fa fa-arrow-circle-o-up"></i> Enviar archivos</button>
                                </center>
                        </form>
                        
                    </div>
                </div>
            </div>
                
                <ul class="grid cs-style-3">
                    @if (isset($images))
                        @foreach ($images as $img)
                            <li id="element{{ $img[0] }}">
                                
                                @if(count($main) != 0)
                                    @if($main[0]->id == $img[0])
                                        <figure>
                                            <img src="{{ asset('img/realestates/' . $img[1] .'/') }}" alt="{{ $img[1] }}">
                                            <figcaption>
                                                <center>
                                                    <strong>
                                                        <p>No se puede eliminar la imagen que esta de portada</p>
                                                    </strong>
                                                    
                                                </center>
                                                
                                            </figcaption>
                                        </figure>
                                        <center>
                                            <button type="button" id="img{{ $img[0] }}"data-id="{{ $img[0] }}"  data-name="{{ $img[1] }}" class="btn btn-warning">
                                                <i class="fa fa-smile-o">-</i>Portada Actual    
                                            </button>
                                        </center>
                                    @endif
                                    @if($main[0]->id != $img[0])
                                        <figure>
                                            <img src="{{ asset('img/realestates/' . $img[1] .'/') }}" alt="{{ $img[1] }}">
                                            <figcaption>
                                                <a class="btn red btnDeleteImage" data-id="{{ $img[0] }}" data-name="{{ $img[1] }}" href="#"><i class="fa fa-trash-o"></i> Eliminar</a>
                                            </figcaption>
                                        </figure>
                                        <center>
                                            <button type="button" id="img{{ $img[0] }}"data-id="{{ $img[0] }}"  data-name="{{ $img[1] }}" class="btn btn-primary btnMainImage">
                                                <i class="fa fa-check-square-o"></i> Elegir Como Portada    
                                            </button>
                                        </center>
                                    @endif
                                @else
                                        <figure>
                                            <img src="{{ asset('img/realestates/' . $img[1] .'/') }}" alt="{{ $img[1] }}">
                                            <figcaption>
                                                <a class="btn red btnDeleteImage" data-id="{{ $img[0] }}" data-name="{{ $img[1] }}" href="#"><i class="fa fa-trash-o"></i> Eliminar</a>
                                            </figcaption>
                                        </figure>
                                        <center>
                                            <button type="button" id="img{{ $img[0] }}"data-id="{{ $img[0] }}"  data-name="{{ $img[1] }}" class="btn btn-primary btnMainImage">
                                                <i class="fa fa-check-square-o"></i> Elegir Como Portada    
                                            </button>
                                        </center>
                                @endif
                          
                                
                            </li>
                        @endforeach
                    @endif
                </ul>
                

                <!-- MODAL-->
            <div class="modal fade" id="mdDeleteImage" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
                <div class="modal-dialog">
                    <div class="modal-content">
                        <div class="modal-header">
                            <h4 class="modal-title">Eliminar imagen</h4>
                            <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
                            
                        </div>
                        <div class="modal-body">
                            ¿Estas seguro/a de querer eliminar esta imagen?
                            <input type="hidden" name="nameImage" id="idImage">
                        </div>
                        <div class="modal-footer">
                            <button id="btnMdDeleteImg" type="button" class="btn red"><i class="fa fa-trash-o"></i> Eliminar</button>
                            <button type="button" class="btn default" data-dismiss="modal"><i class="fa fa-times-circle-o"></i> Cancelar</button>
                        </div>
                    </div>
                </div>
            </div>
            <!-- FIN MODAL-->

                          
        </div>
    <script src="{{asset('templante/HTML/js/jquery-2.2.0.min.js')}}"></script>
    <script src="{{asset('templante/HTML/js/bootstrap.min.js')}}"></script>
    <script type="text/javascript" src="{{asset('plugin/dropzone/dropzone511.js')}}"></script>
	<script type="text/javascript" src="{{asset('plugin/gallery/modernizr.custom.js')}}"></script>
    <script type="text/javascript" src="{{asset('plugin/gallery/toucheffects.js')}}"></script>
    
    
    
    <script>
            Dropzone.options.myDropzone = {
            autoProcessQueue: false,
            paramName: "file",
			uploadMultiple: true,
			maxFilesize: 10, //MB
            parallelUploads: 6,
			maxFiles: 6,
			previewsContainer: "#previews",
			params: {
					 token: '{{ csrf_token() }}'
				  },
			init: function() {
				var submitButton = document.querySelector('#submit-all');
				myDropzone = this;

				submitButton.addEventListener('click', function(e){
					e.preventDefault();
					e.stopPropagation();
					myDropzone.processQueue();
				});

				this.on('addedfile', function(file){
					//alert('agregó un archivo');
				});

				this.on('complete', function(file){
					myDropzone.removeFile(file);
				});

			},
			success: function(data) {
				if (data.accepted) {
					$('#mensaje_afirmativo').show();
					$('#mensaje_error').hide();
					setTimeout(function(){ location.reload(); }, 3000);
				} else {
                    console.log(data)
					$('#mensaje_afirmativo').hide();
					$('#mensaje_error').show();
				}
			}
		}



                $('.btnDeleteImage').on('click', function(e){
                    e.preventDefault();
                    $('#nameImage').val($(this).data('name'));
                    $('#idImage').val($(this).data('id'));
                    $('#mdDeleteImage').modal('show');
                });

                $('#btnMdDeleteImg').on('click', function(){
                    var id   = $('#idImage').val()
                    console.log('id',id)
                    $.ajax({
                        method: "DELETE",
                        url: "/Multimedia/" + id,
                        data: { '_token': '{{ csrf_token() }}' },
                        })
                        .done(function( data ) {
                            console.log('data',data)
                            if (data == 'ok') {
                                window.location ="/Multimedia/{{$idRealestate}}";
                            }
                        });
                });

                $('.btnMainImage').on('click', function(){
                    var id   = $(this).data('id');
                    var name = $(this).data('name');
                    $.ajax({
                        method: "POST",
                        url: "/imageMain/" + id +"/"+ name,
                        data: { '_token': '{{ csrf_token() }}' },
                        })
                        .done(function( data ) {
                            console.log('data',data)
                            if (data == 'ok') {
                                window.location ="/Multimedia/{{$idRealestate}}";
                            }
                        });
                });
    </script>
</body>
</html>


       
    

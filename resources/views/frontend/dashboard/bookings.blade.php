@extends('frontend.dashboard.masterDashboard')

@section('stylus')


@endsection


@section('content_dashboard')
        
                    <div class="dashboard-header clearfix">
                        <div class="row">
                            <div class="col-sm-12 col-md-6"><h4>Reservas</h4></div>
                            <div class="col-sm-12 col-md-6">
                                <div class="breadcrumb-nav">
                                    <ul>
                                        <li>
                                            <a href="index.html">Inicio</a>
                                        </li>
                                        <li>
                                            <a href="dashboard.html">Panel</a>
                                        </li>
                                        <li class="active">Reservas</li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="submit-address dashboard-list">
                        <form method="GET">
                            <h4>Lista de Reservas</h4>
                            <div class="row pad-20">
                                <div class="col-lg-12">
                                    <div class="comment">
                                        <div class="comment-author">
                                            <a href="#">
                                                <img src="http://placehold.it/60x60" alt="comments-user">
                                            </a>
                                        </div>
                                        <div class="comment-content">
                                            <div class="comment-meta">
                                                <h5>Melina</h5>
                                                <div class="comment-meta">
                                                    8:42 PM 1/28/2017<a href="#">Respuesta</a>
                                                </div>
                                            </div>
                                            <ul>
                                                <li>Inmueble :<span> Casa del divino niño</span></li>
                                                <li>Fecha de visita : <span> 18 Noviembre 2019</span></li>
                                                <li>Hora :<span> 18 hs</span></li>
                                                <li>Mail : <span><a href="mailto:info@themevessel.com"> info@themevessel.com</a> </span></li>
                                                <li>Telefono : <span> <a href="tel:3704567899">3704567899</a></span></li>
                                            </ul>
                                            <div class="buttons mb-20">
                                                <a href="#" class="btn-1 btn-gray"><i class="fa fa-fw fa-check-circle-o"></i> Aprovado</a>
                                                <a href="#" class="btn-1 btn-gray"><i class="fa fa-fw fa-times-circle-o"></i> Cancelar</a>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="comment">
                                        <div class="comment-author">
                                            <a href="#">
                                                <img src="http://placehold.it/60x60" alt="comments-user">
                                            </a>
                                        </div>
                                        <div class="comment-content">
                                            <div class="comment-meta">
                                                <h5>Teseira</h5>
                                                <div class="comment-meta">
                                                    8:42 PM 1/28/2017<a href="#">Reply</a>
                                                </div>
                                            </div>
                                            <ul>
                                                <li>Inmueble :<span> Casa del divino niño</span></li>
                                                <li>Fecha de visita : <span> 18 Noviembre 2019</span></li>
                                                <li>Hora :<span> 18 hs</span></li>
                                                <li>Mail : <span><a href="mailto:info@themevessel.com"> info@themevessel.com</a> </span></li>
                                                <li>Telefono : <span> <a href="tel:3704567899">3704567899</a></span></li>
                                            </ul>
                                            <div class="buttons mb-20">
                                                <a href="#" class="btn-1 btn-gray"><i class="fa fa-fw fa-check-circle-o"></i> Aprovado</a>
                                                <a href="#" class="btn-1 btn-gray"><i class="fa fa-fw fa-times-circle-o"></i> Cancelar</a>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="comment comment-b">
                                        <div class="comment-author">
                                            <a href="#">
                                                <img src="http://placehold.it/60x60" alt="comments-user">
                                            </a>
                                        </div>
                                        <div class="comment-content">
                                            <div class="comment-meta">
                                                <h5>Emma Connor</h5>
                                                <div class="comment-meta">
                                                    8:42 PM 1/28/2017<a href="#">Reply</a>
                                                </div>
                                            </div>
                                            <ul>
                                                <li>Inmueble :<span> Casa del divino niño</span></li>
                                                <li>Fecha de visita : <span> 18 Noviembre 2019</span></li>
                                                <li>Hora :<span> 18 hs</span></li>
                                                <li>Mail : <span><a href="mailto:info@themevessel.com"> info@themevessel.com</a> </span></li>
                                                <li>Telefono : <span> <a href="tel:3704567899">3704567899</a></span></li>
                                            </ul>
                                            <div class="buttons">
                                                <a href="#" class="btn-1 btn-gray"><i class="fa fa-fw fa-check-circle-o"></i> Aprovado</a>
                                                <a href="#" class="btn-1 btn-gray"><i class="fa fa-fw fa-times-circle-o"></i> Cancelar</a>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </form>
                    </div>

@endsection


@section('script')


@endsection
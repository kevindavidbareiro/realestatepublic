  <div id="replyModal" class="modal" tabindex="-1" role="dialog">
    <div class="modal-dialog modal-dialog-centered" role="document">
      <div class="modal-content">
        <div class="modal-body">
        	<div id="comment"class="alert alert-secondary" role="alert">
        		<p></p>
			</div>
          
         	<form method="POST" action="{{ url('/mensajes/respuesta') }}">
          	@csrf
          	<input type="hidden", name="comment_id" id="commentId">
          	<textarea class="modal-textarea form-control" name="reply"></textarea>
          
        </div>
        
		<div class="modal-footer">
            <button type="submit" class="btn btn-default ">Responder</button>
         	</form>	  
        	<button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
		</div>        
      </div>      
    </div>
  </div> 
<div id="modalDelete" class="modal fade">
	<div class="modal-dialog modal-confirm">
		<div class="modal-content">
			<div class="modal-header">
				<div class="icon-box">
					<i class="material-icons"> clear </i>
				</div>				
				<h4 class="modal-title">Estas Seguro?</h4>	
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
			</div>
			<div class="modal-body">
				<p>Realmente estas seguro de borrar la propiedad? </p>
			</div>
			<div class="modal-footer">
				<form method="POST" action="{{ route('propiedades.destroy', 'test') }}">
					@csrf
					@method('DELETE')
					<input type="hidden", name="property_id" id="prop_id">
					<button type="button" class="btn btn-info" data-dismiss="modal">Cancelar</button>
					<button type="submit" class="btn btn-danger">Borrar</button>
				</form>
			</div>
		</div>
	</div>
</div> 
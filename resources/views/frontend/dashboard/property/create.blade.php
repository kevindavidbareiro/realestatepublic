@extends('frontend.dashboard.masterDashboard')

@section('stylus')
<link rel="stylesheet" type="text/css" href="{{asset('css/style.css')}}"/>

@endsection


@section('content_dashboard')
        
<div class="dashboard-header clearfix">
                        <div class="row">
                            <div class="col-sm-12 col-md-6"><h4>Subir Propiedades</h4></div>
                            <div class="col-sm-12 col-md-6">
                                <div class="breadcrumb-nav">
                                    <ul>
                                        <li>
                                            <a href="{{ url('/') }}">Inicio</a>
                                        </li>
                                        <li>
                                            <a href="{{ url('panel') }}">Panel</a>
                                        </li>
                                        <li class="active">Subir Propiedad</li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="submit-address dashboard-list">
                        @if ($errors->any())
                            <div class="alert alert-danger">
                                <ul>
                                    @foreach ($errors->all() as $error)
                                        <li>{{ $error }}</li>
                                    @endforeach
                                </ul>
                            </div>
                        @endif

                        <form action="{{ url('propiedades') }}" enctype="multipart/form-data" method="POST">
                            @csrf
                            <h4 class="bg-grea-3">Informacion Basica</h4>
                            <div class="search-contents-sidebar">
                                <div class="row pad-20">
                                    <div class="col-lg-4 col-md-4 col-sm-12">
                                        <div class="form-group">
                                            <label for="propertyName">Nombre de Propiedad</label>
                                            <input type="text" class="form-control  input-text  @if($errors->has('propertyName')) is-invalid @endif " id="propertyName" name="propertyName" placeholder="Nombre de Propiedad" value="{{ old('propertyName') }}">
                                            <div class="invalid-feedback">
                                                Porfavor cargar un nombre
                                            </div>
                                        </div>
                                    </div>
                                    
                                    <div class="col-lg-4 col-md-4 col-sm-12">
                                        <div class="form-group">
                                            <label for="country">Condición</label>
                                            <select class="form-control search-fields" id="state" name="state_id">
                                                @foreach ($states as $state)
                                                    <option value="{{ $state->id }}"
                                                        @if (old('state_id'))
                                                            @if (old('state_id') == $state->id)
                                                                selected
                                                            @endif
                                                        @elseif ($state->id == 1)
                                                            selected
                                                        @endif
                                                        >
                                                        {{ $state->name }}
                                                    </option>
                                                @endforeach
                                            </select>
                                        </div>
                                    </div>
                                    <div class="col-lg-4 col-md-4 col-sm-12">
                                        <div class="form-group">
                                            <label for="country">Tipo de Inmueble</label>
                                            <select class="form-control search-fields" id="typeOfRealestate" name="typeOfRealestate_id">
                                                @foreach ($typeOfRealestates as $typeOfRealestate)
                                                    <option value="{{ $typeOfRealestate->id }}"
                                                        @if (old('typeOfRealestate_id'))
                                                            @if (old('typeOfRealestate_id') == $typeOfRealestate->id)
                                                                selected
                                                            @endif
                                                        @elseif ($typeOfRealestate->id == 1)
                                                            selected
                                                        @endif
                                                        >
                                                        {{ $typeOfRealestate->name }}
                                                    </option>
                                                @endforeach
                                            </select>
                                        </div>
                                    </div>
                                    <div class="col-lg-4 col-md-4 col-sm-12">
                                        <div class="form-group">
                                            <label for="price">Precio</label>
                                            <input type="text" class="form-control input-text @if($errors->has('price')) is-invalid @endif" id="price" name="price" placeholder="Precio" value="{{ old('price') }}">
                                            <div class="invalid-feedback">
                                                Porfavor cargar un precio
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-lg-4 col-md-4 col-sm-12">
                                        <div class="form-group">
                                            <label for="area">Area</label>
                                            <input type="text" id="area" class="form-control input-text @if($errors->has('area')) is-invalid @endif" name="area" placeholder="m2" value="{{ old('area') }}">
                                            <div class="invalid-feedback">
                                                Porfavor cargar el area
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-lg-4 col-md-4 col-sm-12">
                                        <div class="form-group">
                                            <label for="bedrooms">Habitaciones</label>
                                            <input type="text" class="form-control input-text @if($errors->has('bedrooms')) is-invalid @endif" id="bedrooms" name="bedrooms" placeholder="N° de Habitaciones" value="{{ old('bedrooms') }}">
                                            <div class="invalid-feedback">
                                                Porfavor cargar el N° de habitaciones
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-lg-4 col-md-4 col-sm-12">
                                        <div class="form-group">
                                            <label for="bathrooms">Baño/s</label>
                                             <input type="text" class="form-control input-text @if($errors->has('bathrooms')) is-invalid @endif" id="bathrooms" name="bathrooms" placeholder="N° de Baños" value="{{ old('bathrooms') }}">
                                             <div class="invalid-feedback">
                                                Porfavor cargar el N° de baños
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <h4 class="bg-grea-3">Dirección Detallada</h4> 
                            
                            <div class="row pad-20">
                                <div class="col-lg-12" id = "alert_placeholder"></div>
                                <div class="col-lg-4 col-md-4 col-sm-12">
                                    <div class="form-group">
                                        <label for="country">Pais</label>
                                            <select class="form-control search-fields" id="country" name="country_id">
                                                @foreach ($countries as $country)
                                                    <option value="{{ $country->id }}"
                                                        @if (old('country_id'))
                                                            @if (old('country_id') == $country->id)
                                                                selected
                                                            @endif
                                                        @elseif ($country->id == 1)
                                                            selected
                                                        @endif
                                                        >
                                                        {{ $country->name }}
                                                    </option>
                                                @endforeach
                                            </select>
                                    </div>
                                </div>
                                <div class="col-lg-4 col-md-4 col-sm-12">
                                    <div class="form-group">
                                        <label for="province">Provincia</label>
                                        <select class="form-control search-fields" id="province" name="province_id">
                                            @foreach ($provinces as $province)
                                                @if (old('country_id') && old('country_id') == $province->country_id)
                                                    <option value="{{ $province->id }}"
                                                        @if (old('province_id') == $province->id)
                                                            selected
                                                        @endif
                                                        >
                                                        {{ $province->name }}
                                                    </option>
                                                @elseif ($province->country_id == 1)
                                                    <option value="{{ $province->id }}" @if ($province->id == 1) selected @endif>
                                                        {{ $province->name }}
                                                    </option>
                                                @endif
                                            @endforeach
                                        </select>
                                    </div>
                                </div>
                                <div id="divLocalitie" class="col-lg-4 col-md-4 col-sm-12">
                                    <div class="form-group">
                                        <label for="state">Estado/Localidad</label>
                                        <select class="form-control search-fields" id="localitie" name="localitie_id"> 
                                            @foreach ($localities as $localitie)
                                                @if (old('province_id') && old('province_id') == $localitie->province_id)
                                                    <option value="{{ $localitie->id }}"
                                                        @if (old('localitie_id') == $localitie->id)
                                                            selected
                                                        @endif
                                                        >
                                                        {{ $localitie->name }}
                                                    </option>
                                                @elseif ($localitie->province_id == 1)
                                                    <option value="{{ $localitie->id }}" @if ($localitie->id == 1) selected @endif>
                                                        {{ $localitie->name }}
                                                    </option>
                                                @endif
                                            @endforeach
                                        </select>
                                    </div>
                                </div>  
                                <div id="divCity" class="col-lg-4 col-md-4 col-sm-12">
                                    <div class="form-group">
                                        <label for="city">Barrio</label>
                                        <select class="form-control search-fields" id="city" name="city_id">    
                                            @foreach ($cities as $city)
                                                    @if (old('localitie_id') && old('localitie_id') == $city->localitie_id)
                                                        <option value="{{ $city->id }}"
                                                            @if (old('city_id') == $city->id)
                                                                selected
                                                            @endif
                                                            >
                                                            {{ $city->name }}
                                                        </option>
                                                    @elseif ($city->locality_id == 1)
                                                        <option value="{{ $city->id }}" @if ($city->id == 1) selected @endif>
                                                            {{ $city->name }}
                                                        </option>
                                                    @endif
                                            @endforeach
                                        </select>
                                    </div>
                                </div>
                            </div>
                            <h4 class="bg-grea-3">Ubicación</h4>
                            <div class="row pad-20">
                                <div class="col-lg-4 col-md-4 col-sm-12">
                                    <div class="form-group">
                                        <label for="address">Direccion</label>
                                        <input type="text" class="form-control input-text @if($errors->has('address')) is-invalid @endif" name="address" id="address" placeholder="Calle N°" value="{{ old('address') }}">
                                        <div class="invalid-feedback">
                                            Porfavor ingrese una Ubicación
                                        </div>
                                        <input type="hidden"  name="latitude" id="latitude" value="{{ old('latitude') }}">
                                        <input type="hidden"  name="longitude" id="longitude" value="{{ old('longitude') }}">
                                    </div>
                                     <input type="button" id="submit" class="btn-md button-theme" value="Buscar">
                                </div>
                            </div>
                            <h4 class="bg-grea-3">Ubicacion en el Mapa</h4>
                            <div class="row pad-20">
                                <div class="col-lg-12">
                                    <div id="map" style="height: 200px" ></div>
                                </div>
                            </div>
                            
                            <h4 class="bg-grea-3">Información Detallada</h4>
                            <div class="row pad-20">
                                <div class="col-lg-12">
                                    <textarea class="form-control input-text @if($errors->has('detailedInformation')) is-invalid @endif" name="detailedInformation" placeholder="Información Detallada" value="{{ old('detailedInformation') }}"></textarea>
                                    <div class="invalid-feedback">
                                        Porfavor describa detalladamente el inmueble
                                    </div>
                                </div>
                            </div>
                            <h4 class="bg-grea-3">Caracteristicas (opcional)</h4>
                            <div class="row pad-20">
                                @foreach ($options as $option)
                                <div class="col-lg-3 col-md-4 col-sm-6">
                                    <div class="checkbox checkbox-theme checkbox-circle">
                                        <input id="{{ $option->id }}" type="checkbox" name="specifics[]" value="{{ $option->id }}">
                                        <label for="{{ $option->id }}">
                                            {{ $option->name }}
                                        </label>
                                    </div>
                                </div>
                                @endforeach
                            </div>
                            
                            <h4 class="bg-grea-3">Detalles de contacto</h4>
                            <div class="row pad-20">
                                <div class="col-lg-4 col-md-4 col-sm-12">
                                    <div class="form-group">
                                        <label for="name">Nombre</label>
                                        <input type="text" class="form-control input-text @if($errors->has('name')) is-invalid @endif" name="name" placeholder="Nombre" value="{{ auth()->user()->name }}">
                                    </div>
                                </div>
                                <div class="col-lg-4 col-md-4 col-sm-12">
                                    <div class="form-group">
                                        <label for="email">Email</label>
                                        <input type="email" class="form-control input-text @if($errors->has('email')) is-invalid @endif" name="email" placeholder="Email" value="{{ auth()->user()->email }}">
                                    </div>
                                </div>
                                <div class="col-lg-4 col-md-4 col-sm-12">
                                    <div class="form-group">
                                        <label for="phone">Telefono </label>
                                        <input type="text" class="form-control input-text @if($errors->has('phone')) is-invalid @endif" name="phone"  placeholder="Telefono" value="@if ( count($celular) > 0 ) {{ $celular[0]->contact}} @endif "> 
                                    </div>
                                </div>
                                <div class="col-lg-4 col-md-12 col-sm-12">
                                    <button type="submit" id="submitAll" class="btn btn-md button-theme"> Subir Propiedad</button>
                                    
                                </div>
                            </div>
                        </form>
                    </div>

@endsection

@section('script')
    <script async defer src="https://maps.googleapis.com/maps/api/js?key=AIzaSyDw86gsWyn7-EcV5m3fSqjs8hkfSWOZCwI&libraries=places&callback=initMap">
    </script>

    <script>
        //Carga de Combos
        

        $('#country').change(function(){
            var id = $(this).val();
            var listItems = '<option selected="selected" value="">Seleccionar</option>';
            $.get( "/"+id+"/get_provinces", function( data ) {
               
                var list = data.provinces;
                for (var i = 0; i < list.length; i++) {
                    listItems += "<option value='" + list[i].id + "'>" + list[i].name + "</option>";
                }
               
                $('#province').html(listItems);
            });
	    });

        $('#province').change(function(){
            var id = $(this).val();
            var listItems = '<option selected="selected" value="">Seleccionar</option>';
            $.get( "/"+id+"/get_localities", function( data ) {
               
                var list = data.localities;
                for (var i = 0; i < list.length; i++) {
                    listItems += "<option value='" + list[i].id + "'>" + list[i].name + "</option>";
                }
               
                $('#localitie').html(listItems);
            });
	    });

        $('#localitie').change(function(){
            var id = $(this).val();
            console.log(id,'id de barrios')
            var listItems = '<option selected="selected" value="">Seleccionar</option>';
            $.get( "/"+id+"/get_cities", function( data ) {
                console.log(data,'barrios')
                var list = data.cities;
                for (var i = 0; i < list.length; i++) {
                    listItems += "<option value='" + list[i].id + "'>" + list[i].name + "</option>";
                }
               
              
                    $('#city').html(listItems);
               
                
            });
	    });
    
    </script>

    <script>
            var markers = [];
            // var latitud = '';
            // var longitud = '';
            function initMap() {
                //Inicio del mapa y colocando en una direccion determinada
                var map = new google.maps.Map(document.getElementById('map'), {
                center: {lat: -26.185164, lng: -58.174386},
                zoom: 15
                });
           

                //Seccion para autocompletar las direcciones
                var input = document.getElementById('address');
                var autocomplete = new google.maps.places.Autocomplete(input);
                autocomplete.bindTo('bounds', map);
                autocomplete.setComponentRestrictions({'country': ['ar']});


                //Inicio del geocoding
                var geocoder = new google.maps.Geocoder();
                

                document.getElementById('submit').addEventListener('click', function() {
                geocodeAddress(geocoder, map);
                });
            }

            function geocodeAddress(geocoder, resultsMap) {
                
                var address = document.getElementById('address').value;
                geocoder.geocode({'address': address}, function(results, status) {
               
                if (status === 'OK') {
                    resultsMap.setCenter(results[0].geometry.location);
                    //ELimino los marcadores
                    deleteMarkers();
                    //Creo un marcador de la direccion que tipeo el usuario
                    var marker = new google.maps.Marker({
                    map: resultsMap,
                    position: results[0].geometry.location
                    });
                    markers.push(marker);
                    // Guardo el valor de latitud y longitud en sus respectivas variables
                    // latitud = results[0].geometry.location.lat();
                    // longitud = results[0].geometry.location.lng();
                    // convertirGpsADireccion(latitud, longitud, mostrarDireccion);

                    //Muestro las cordenadas de latitud y longitud en los inpus correspondientes
                    document.getElementById("latitude").value = results[0].geometry.location.lat();
                    document.getElementById("longitude").value = results[0].geometry.location.lng();
                } else {
                    alert('Geocode no tuvo éxito por la siguiente razón: ' + status);
                }
                });
            }
            function setMapOnAll(map) {
                for (var i = 0; i < markers.length; i++) {
                markers[i].setMap(map);
                }
            }
            function clearMarkers() {
                setMapOnAll(null);
            } 
            function deleteMarkers() {
                clearMarkers();
                markers = [];
            }

            function convertirGpsADireccion(lat, lon, callback) {
                $.getJSON("http://nominatim.openstreetmap.org/reverse?" +
                          "format=json&amp;addressdetails=0&zoom=18&" +
                          "lat=" + lat + "&lon=" + lon + "&json_callback=?",
                    callback);
            }

            /*function mostrarDireccion(response) {
                console.log(response);
                let message = "Por favor revisa que los datos sean correctos. De lo contrario corregilos"
                $('#alert_placeholder').html('<div class="alert alert-warning" role="alert"><button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>'+message+'</div>')
                document.getElementById("country").value = response.address.country;
                document.getElementById("province").value = response.address.state;
                document.getElementById("state").value = response.address.state_district;
                document.getElementById("neighbourhood").value = response.address.neighbourhood;
            }*/


    </script>

@endsection

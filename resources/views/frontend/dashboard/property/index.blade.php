@extends('frontend.dashboard.masterDashboard')

@section('stylus')
<style>

    .modal-confirm {        
        color: #636363;
        width: 400px;
    }
    .modal-confirm .modal-content {
        padding: 20px;
        border-radius: 5px;
        border: none;
        text-align: center;
        font-size: 14px;
    }
    .modal-confirm .modal-header {
        border-bottom: none;   
        position: relative;
    }
    .modal-confirm h4 {
        text-align: center;
        font-size: 26px;
        margin: 30px 0 -10px;
    }
    .modal-confirm .close {
        position: absolute;
        top: -5px;
        right: -2px;
    }
    .modal-confirm .modal-body {
        color: #999;
    }
    .modal-confirm .modal-footer {
        border: none;
        text-align: center;     
        border-radius: 5px;
        font-size: 13px;
        padding: 10px 15px 25px;
    }
    .modal-confirm .modal-footer a {
        color: #999;
    }       
    .modal-confirm .icon-box {
        width: 80px;
        height: 80px;
        margin: 0 auto;
        border-radius: 50%;
        z-index: 9;
        text-align: center;
        border: 3px solid #f15e5e;
    }
    .modal-confirm .icon-box i {
        color: #f15e5e;
        font-size: 46px;
        display: inline-block;
        margin-top: 13px;
    }
    .modal-confirm .btn {
        color: #fff;
        border-radius: 4px;
        background: #60c7c1;
        text-decoration: none;
        transition: all 0.4s;
        line-height: normal;
        min-width: 120px;
        border: none;
        min-height: 40px;
        border-radius: 3px;
        margin: 0 5px;
        outline: none !important;
    }
    .modal-confirm .btn-info {
        background: #c1c1c1;
    }
    .modal-confirm .btn-info:hover, .modal-confirm .btn-info:focus {
        background: #a8a8a8;
    }
    .modal-confirm .btn-danger {
        background: #f15e5e;
    }
    .modal-confirm .btn-danger:hover, .modal-confirm .btn-danger:focus {
        background: #ee3535;
    }
    .trigger-btn {
        display: inline-block;
        margin: 100px auto;
    }

</style>

@endsection


@section('content_dashboard')
        
<div class="dashboard-header clearfix">
        <div class="row">
            <div class="col-sm-12 col-md-6"><h4>Mi Propiedad</h4></div>
            <div class="col-sm-12 col-md-6">
                <div class="breadcrumb-nav">
                    <ul>
                        <li>
                            <a href="{{ url('/') }}">Inicio</a>
                        </li>
                        <li>
                            <a href="{{ url('panel') }}">Panel</a>
                        </li>
                        <li class="active">Mis Propiedades</li>
                    </ul>
                </div>
            </div>
        </div>
</div>
    @if (session('typemsg'))
        @if (session('typemsg') == 'success')
            <div class="alert alert-success" role="alert">
             <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                <span aria-hidden="true">&times;</span>
             </button>
                <strong>{{ session('message') }}</strong>
            </div>
        @endif
        @if (session('typemsg') == 'error' )
            <div class="alert alert-danger" role="alert">
                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                 </button>
                <strong>{{ session('message') }}</strong>
            </div>
        @endif  
    @endif  
    <div class="dashboard-list">
        <h3>Lista de Mis Propiedades</h3>
        <table class="manage-table">
            <tbody>
            @foreach ($properties as $property)
                <tr class="responsive-table">
                    <td class="listing-photoo">
                        @foreach ($property->multimedia->where('main', 1) as $imageCover)
                            <img src="{{ asset('img/realestates/main/'.$imageCover->url.'') }}" alt="listing-photo" class="img-fluid">
                        @endforeach
                        
                                                
                    </td>
                    <td class="title-container">
                        <h2><a href="{{ route('propiedades.show', $property->id) }}">{{ $property->name  }}</a></h2>
                        <h5 class="d-none d-xl-block d-lg-block d-md-block"><i class="flaticon-pin"></i> {{ $property->address  }} </h5>
                        <h6 class="table-property-price">{{ $property->price .'/ '. $property->type_of_realestate->name  }} </h6>
                    </td>
                    <td class="expire-date">{{ $property->type_of_realestate->name }}</td>
                    <td class="action">
                        <a href="{{ route('propiedades.edit', $property->id) }}"><i class="fa fa-pencil"></i> Editar</a>
                        <a href="{{ route('propiedades.show', $property->id) }}"><i class="fa  fa-eye-slash"></i> Ver</a>
                        <a href="{{url('Multimedia/'.$property->id.'')}}"><i class="fa  fa-camera"></i> Cargar Imagenes</a>
                        <a  id="modalShow" data-toggle="modal" data-propid="{{ $property->id }}" data-target="#modalDelete"class="delete"><i class="fa fa-remove"></i> Eliminar</a>
                    </td>
                </tr>
             
            @endforeach
            </tbody>
        </table>
    </div>

@include('frontend.dashboard.modals.delete')
@endsection


@section('script')
<script>
     $('#modalDelete').on('show.bs.modal', function (event) {
        $("#modalDelete").css("margin", "100px");
        var button = $(event.relatedTarget) 
        var prop_id = button.data('propid') 
        var modal = $(this)
         modal.find('.modal-footer #prop_id').val(prop_id);
    })



</script>

@endsection
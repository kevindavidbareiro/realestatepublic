@extends('frontend.dashboard.masterDashboard')

@section('stylus')


@endsection


@section('content_dashboard')
        
                        <div class="dashboard-header clearfix">
                            <div class="row">
                                <div class="col-sm-12 col-md-6"><h4>Mi Perfil</h4></div>
                                <div class="col-sm-12 col-md-6">
                                    <div class="breadcrumb-nav">
                                        <ul>
                                            <li>
                                                <a href="{{ url('/') }}">Inicio</a>
                                            </li>
                                            <li>
                                                <a href="{{ url('panel') }}">Panel</a>
                                            </li>
                                            <li class="active">Mi Perfil</li>
                                        </ul>
                                    </div>
                                </div>
                            </div>
                        </div>
                        @if (session('typemsg'))
                            @if (session('typemsg') == 'success')
                                <div class="alert alert-success" role="alert">
                                 <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                    <span aria-hidden="true">&times;</span>
                                 </button>
                                    <strong>{{ session('message') }}</strong>
                                </div>
                            @endif
                            @if (session('typemsg') == 'error' )
                                <div class="alert alert-danger" role="alert">
                                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                        <span aria-hidden="true">&times;</span>
                                     </button>
                                    <strong>{{ session('message') }}</strong>
                                </div>
                            @endif  
                        @endif  
                        <div class="dashboard-list">
                            <h3 class="heading">Detalles</h3>
                            <div class="dashboard-message contact-2 bdr clearfix">
                                <div class="row">
                                    <div class="col-lg-3 col-md-3">
                                        <!-- Edit profile photo -->
                                        <form action="{{ url('/usuario/perfil') }}" method="POST" enctype="multipart/form-data">
                                            @csrf
                                        <div class="edit-profile-photo">
                                            <img src="@if ($user[0]->imgProfile)
                                                 {{ asset('img/user/'.$user[0]->imgProfile.'/') }}
                                            @else
                                                {{ asset('img/black-logo.png') }}
                                            @endif" alt="profile-photo" class="img-fluid">
                                            <div class="change-photo-btn">
                                                <div class="photoUpload">
                                                    <span><i class="fa fa-upload"></i></span>
                                                    <input type="file" class="upload" name="avatar">
                                                </div>
                                            </div>
                                        </div>

                                    </div>
                                    <div class="col-lg-9 col-md-9">
                                        
                                            <div class="row">
                                                <div class="col-lg-6 col-md-6">
                                                    <div class="form-group name">
                                                        <label>Nombre</label>
                                                        <input type="text" name="name" class="form-control input-text  @if($errors->has('name')) is-invalid @endif" id="name"  @if (old('name')) value="{{ old('name') }}" @else value="{{ $user[0]->name }}"@endif placeholder="Nombre">
                                                    </div>
                                                </div>
                                                <div class="col-lg-6 col-md-6">
                                                    <div class="form-group email">
                                                        <label>Email</label>
                                                        <input type="email" name="email" class="form-control"  placeholder="Email" value="{{ $user[0]->email }}" readonly="readonly">
                                                    </div>
                                                </div>
                                                <div class="col-lg-6 col-md-6">
                                                    <div class="form-group number">
                                                        <label>Celular</label>
                                                        <input type="text" name="celular" class="form-control" placeholder="Celular" value="@if ( count($userCelular) > 0){{ $userCelular[0]->contact }}@endif">
                                                    </div>
                                                </div>
                                                <div class="col-lg-6 col-md-6">
                                                    <div class="form-group number">
                                                        <label>Telefono</label>
                                                        <input type="text" name="phone" class="form-control" placeholder="Telefono" value="@if ( count($userTelephone) > 0){{ $userTelephone[0]->contact }} @endif">
                                                    </div>
                                                </div>
                                                
                                                <div class="col-lg-12 col-md-12 col-sm-12">
                                                    <div class="form-group message">
                                                        <label>Informacion Personal</label>
                                                        <textarea class="form-control" name="infoPersonal" placeholder="Informacion Personal">@if ($userData){{ $userData }} @endif</textarea>
                                                    </div>
                                                </div>
                                                <div class="col-lg-12">
                                                    <div class="send-btn">
                                                        <button type="submit" class="btn btn-md button-theme">Guardar Cambios</button>
                                                    </div>
                                                </div>
                                            </div>
                                        </form>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-6">
                                <div class="dashboard-list">
                                    <h3 class="heading">Cambiar contraseña</h3>
                                    <div class="dashboard-message contact-2">
                                        <form action="#" method="GET" enctype="multipart/form-data">
                                            <div class="row">
                                                <div class="col-lg-12">
                                                    <div class="form-group name">
                                                        <label>Contraseña Actual</label>
                                                        <input type="password" name="current-password" class="form-control" placeholder="Contraseña Actual">
                                                    </div>
                                                </div>
                                                <div class="col-lg-12">
                                                    <div class="form-group email">
                                                        <label>Nueva Contraseña</label>
                                                        <input type="password" name="new-password" class="form-control" placeholder="Nueva Contraseña">
                                                    </div>
                                                </div>
                                                <div class="col-lg-12">
                                                    <div class="form-group subject">
                                                        <label>Confirmar Nueva Contraseña</label>
                                                        <input type="password" name="confirm-new-password" class="form-control" placeholder="Confirmar Nueva Contraseña">
                                                    </div>
                                                </div>
                                                <div class="col-lg-12">
                                                    <div class="send-btn">
                                                        <button type="submit" class="btn btn-md button-theme">Cambiar Contraseña</button>
                                                    </div>
                                                </div>
                                            </div>
                                        </form>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="dashboard-list">
                                    <h3 class="heading">Redes Sociales</h3>
                                    <div class="dashboard-message contact-2">
                                        <form action="#" method="GET" enctype="multipart/form-data">
                                            <div class="row">
                                                <div class="col-lg-12">
                                                    <div class="form-group name">
                                                        <label>Facebook</label>
                                                        <input type="text" name="facebook" class="form-control" placeholder="https://www.facebook.com" value="@if ( count($userFacebook) > 0){{ $userFacebook[0]->contact }} @endif">
                                                    </div>
                                                </div>
                                                <div class="col-lg-12">
                                                    <div class="form-group subject">
                                                        <label>Instagram</label>
                                                        <input type="text" name="vkontakte" class="form-control" placeholder="https://instagram.com">
                                                    </div>
                                                </div>
                                                <div class="col-lg-12">
                                                    <div class="form-group number">
                                                        <label>Whatsapp</label>
                                                        <input type="email" name="whatsapp" class="form-control" placeholder="https://www.whatsapp.com">
                                                    </div>
                                                </div>
                                                <div class="col-lg-12">
                                                    <div class="send-btn">
                                                        <button type="submit" class="btn btn-md button-theme">Guardat Cambios</button>
                                                    </div>
                                                </div>
                                            </div>
                                        </form>
                                    </div>
                                </div>
                            </div>
                        </div>

@endsection


@section('script')


@endsection
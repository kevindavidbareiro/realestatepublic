@extends('frontend.dashboard.masterDashboard')

@section('stylus')
<style>

</style>

@endsection


@section('content_dashboard')
<div class="dashboard-header clearfix">
                        <div class="row">
                            <div class="col-sm-12 col-md-6"><h4>Mensajes</h4></div>
                            <div class="col-sm-12 col-md-6">
                                <div class="breadcrumb-nav">
                                    <ul>
                                        <li>
                                            <a href="{{ url('/') }}">Inicio</a>
                                        </li>
                                        <li>
                                            <a href="{{ url('panel') }}">Panel</a>
                                        </li>
                                        <li class="active">Mensajes</li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                    </div>
                    @if (session('typemsg'))
                        @if (session('typemsg') == 'success')
                                <div class="alert alert-success" role="alert">
                                 <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                    <span aria-hidden="true">&times;</span>
                                 </button>
                                    <strong>{{ session('message') }}</strong>
                                </div>
                            @endif
                            @if (session('typemsg') == 'error' )
                                <div class="alert alert-danger" role="alert">
                                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                        <span aria-hidden="true">&times;</span>
                                     </button>
                                    <strong>{{ session('message') }}</strong>
                                </div>
                            @endif  
                        @endif  
                    <div class="submit-address dashboard-list">
                            <h4>Lista de mensajes</h4>
                            @foreach ($comments as $comment)
                            <div class="row pad-20">
                                <div class="col-lg-12">
                                    <div class="comment">
                                        <div class="comment-author">
                                            <a href="#">
                                                <img src="{{ asset('img/user/'.$comment->user->imgProfile.'/') }}" alt="comments-user">
                                            </a>
                                        </div>
                                        <div class="comment-content">
                                            <div class="comment-meta">
                                                <h5>
                                                    {{ $comment->user->name }}
                                                </h5>
                                                <div class="comment-meta">
                                                    {{ $comment->created_at }}<a id="showModal"href="#replyModal" data-toggle="modal"  data-target="#replyModal" data-comment="{{ $comment->comment }}" data-idcomment="{{ $comment->id }}">Responder</a>
                                                </div>
                                            </div>
                                            <p>{{ $comment->comment }}</p>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            @endforeach
                    </div>
@include('frontend.dashboard.modals.reply')
@endsection


@section('script')
<script >

    $('#replyModal').on('show.bs.modal', function (event) {
        $("#replyModal").css("margin", "100px");
        var button = $(event.relatedTarget) 
        var comment = button.data('comment')
        var commentId = button.data('idcomment')
        var modal = $(this)
        modal.find('.modal-body #commentId').val(commentId);
        $('#comment').text(comment);
    })


</script>
@endsection
<!DOCTYPE html>
<html lang="zxx">
<head>
    <title>Imperio Inmoviliario</title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta charset="utf-8">

    <!-- External CSS libraries -->
    <link rel="stylesheet" type="text/css" href="{{asset('templante/HTML/css/bootstrap.min.css')}}">
    <link rel="stylesheet" type="text/css" href="{{asset('templante/HTML/css/animate.min.css')}}">
    <link rel="stylesheet" type="text/css" href="{{asset('templante/HTML/css/bootstrap-submenu.css')}}">

    <link rel="stylesheet" type="text/css" href="{{asset('templante/HTML/css/bootstrap-select.min.css')}}">
    <link rel="stylesheet" type="text/css" href="{{asset('templante/HTML/css/magnific-popup.css')}}">
    <link rel="stylesheet" href="{{asset('templante/HTML/css/leaflet.css')}}" type="text/css">
    <link rel="stylesheet" href="{{asset('templante/HTML/css/map.css')}}" type="text/css">
    <link rel="stylesheet" type="text/css" href="{{asset('templante/HTML/fonts/font-awesome/css/font-awesome.min.css')}}">
    <link rel="stylesheet" href="https://fonts.googleapis.com/icon?family=Material+Icons">
    <link rel="stylesheet" type="text/css" href="{{asset('templante/HTML/fonts/flaticon/font/flaticon.css')}}">
    <link rel="stylesheet" type="text/css" href="{{asset('templante/HTML/fonts/linearicons/style.css')}}">
    <link rel="stylesheet" type="text/css"  href="{{asset('templante/HTML/css/jquery.mCustomScrollbar.css')}}">
    <!-- <link rel="stylesheet" type="text/css"  href="{{asset('templante/HTML/css/dropzone.css')}}"> -->
    <link rel="stylesheet" type="text/css"  href="{{asset('templante/HTML/css/slick.css')}}">
    <link rel="stylesheet" type="text/css" href="{{asset('css/style.css')}}">
    <!-- Custom stylesheet -->
    <link rel="stylesheet" type="text/css" href="{{asset('templante/HTML/css/style.css')}}">
    <link rel="stylesheet" type="text/css" id="style_sheet" href="{{asset('templante/HTML/css/skins/default.css')}}">

    <!-- Favicon icon -->
    <link rel="shortcut icon" href="{{asset('templante/HTML/img/favicon.ico')}}" type="image/x-icon" >

    <!-- Google fonts -->
    <link rel="stylesheet" type="text/css" href="https://fonts.googleapis.com/css?family=Raleway:300,400,500,600,300,700">

    <!-- IE10 viewport hack for Surface/desktop Windows 8 bug -->
    <link rel="stylesheet" type="text/css" href="{{asset('templante/HTML/css/ie10-viewport-bug-workaround.css')}}">

    <!-- Just for debugging purposes. Don't actually copy these 2 lines! -->
    <!--[if lt IE 9]><script  src="js/ie8-responsive-file-warning.js"></script><![endif]-->
    <script  src="{{asset('templante/HTML/js/ie-emulation-modes-warning.js')}}"></script>

    @yield('stylus')
    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
    <script  src="js/html5shiv.min.js"></script>
    <script  src="js/respond.min.js"></script>
    <![endif]-->
</head>
<body>
<div class="page_loader"></div>
<!-- Main header start -->
<header class="main-header header-2 fixed-header">
    <div class="container-fluid">
        <nav class="navbar navbar-expand-lg navbar-light">
            <a class="navbar-brand pad-0" href="/">
                <img class="logo-realestate" src="{{asset('img/logo.png')}}" alt="logo  Imperio Inmobiliairio">
            </a>
            <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
                <span class="navbar-toggler-icon"></span>
            </button>
            <div class="collapse navbar-collapse" id="navbarSupportedContent">
                <ul class="navbar-nav ml-auto d-lg-none d-xl-none">
                    <li class="nav-item dropdown active">
                        <a href="{{ url('panel') }}" class="nav-link">Panel</a>
                    </li>
                    <li class="nav-item dropdown">
                        <a href="{{ url('mensajes') }}" class="nav-link">Mensajes</a>
                    </li>
                    <li class="nav-item dropdown">
                        <a href="{{ url('reservas') }}" class="nav-link">Reservas</a>
                    </li>
                    <!-- <li class="nav-item dropdown">
                        <a href="my-properties.html" class="nav-link">My Properties</a>
                    </li>
                    <li class="nav-item dropdown">
                        <a href="my-invoices.html" class="nav-link">My Invoices</a>
                    </li> -->
                    <li class="nav-item dropdown">
                        <a href="{{ url('propiedades') }}" class="nav-link">Mis Propiedades</a>
                    </li>
                    <li class="nav-item dropdown">
                        <a href="{{ url('propiedades/create')}}" class="nav-link">Cargar Propiedad</a>
                    </li>
                    <li class="nav-item dropdown">
                        <a href="{{ url('usuario/perfil') }}" class="nav-link">Mi Perfil</a>
                    </li>
                    <li class="nav-item dropdown">
                        <a href="{{ url('logout') }}" class="nav-link">Cerrar Sesión</a>
                    </li>
                </ul>
                <div class="navbar-buttons ml-auto d-none d-xl-block d-lg-block">
                    <ul>
                        <li>
                            <div class="dropdown btns">
                                <a class="dropdown-toggle" data-toggle="dropdown">
                                    <img src="@if (auth()->user()->imgProfile)
                                                 {{ asset('img/user/'.auth()->user()->imgProfile.'/') }}
                                            @else
                                                {{ asset('img/black-logo.png') }}
                                            @endif" alt="avatar">
                                    {{ auth()->user()->name }}
                                </a>
                                <div class="dropdown-menu">
                                    <a class="dropdown-item" href="{{ url('panel') }}">Panel</a>
                                    <a class="dropdown-item" href="{{ url('mensajes') }}">Mensajes</a>
                                    <a class="dropdown-item" href="{{ url('reservas') }}">Reservas</a>
                                    <a class="dropdown-item" href="{{ url('usuario/perfil') }}">Mi Perfil</a>
                                    <a class="dropdown-item" href="{{ url('logout') }}">Cerrar Sesión</a>
                                </div>
                            </div>
                        </li>
                        <li>
                            <a class="btn btn-theme btn-md" href="{{ url('propiedades/create')}}">Cargar Propiedad</a>
                        </li>
                    </ul>
                </div>
            </div>
        </nav>
    </div>
</header>
<!-- Main header end -->

<!-- Dashbord start -->
<div class="dashboard">
    <div class="container-fluid">
        <div class="row">
            <div class="col-lg-3 col-md-12 col-sm-12 col-pad">
                <div class="dashboard-nav d-none d-xl-block d-lg-block">
                    <div class="dashboard-inner">
                        <h4>Inicio</h4>
                        <ul>
                            <li class="active"><a href="{{ url('panel') }}"><i class="flaticon-dashboard"></i>Panel</a></li>
                            <li><a href="{{ url('mensajes') }}"><i class="flaticon-mail"></i> Mensajes <span class="nav-tag">{{ $countComments }}</span></a></li>
                            <li><a href="{{ url('reservas') }}"><i class="flaticon-calendar"></i>Reservas</a></li>
                        </ul>
                        <h4>Listas</h4>
                        <ul>
                            <li><a href="{{ url('propiedades') }}"><i class="flaticon-apartment-1"></i>Mis Propiedades</a></li>
                            <!--<li><a href="my-invoices.html"><i class="flaticon-bill"></i>My Invoices</a></li>
                            <li><a href="favorited-properties.html"><i class="flaticon-heart"></i>Favorited Properties</a></li>-->
                            <li><a href="{{ url('propiedades/create')}}"><i class="flaticon-plus"></i>Subir Propiedad</a></li>
                        </ul>
                        <h4>Perfil</h4>
                        <ul>
                            <li><a href="{{ url('miPerfil') }}"><i class="flaticon-people"></i>Mi Perfil</a></li>
                            <li><a href="{{ url('logout') }}"><i class="flaticon-logout"></i>Cerrar Sesión</a></li>
                        </ul>
                    </div>
                </div>
            </div>
            <div class="col-lg-9 col-md-12 col-sm-12 col-pad">
                <div class="content-area5">
                    <div class="dashboard-content">
                        @yield('content_dashboard')
                        
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- Dashbord end -->

<!-- Full Page Search -->
<div id="full-page-search">
    <button type="button" class="close">×</button>
    <form action="index.html#">
        <input type="search" value="" placeholder="type keyword(s) here" />
        <button type="submit" class="btn btn-sm button-theme">Search</button>
    </form>
</div>

<script src="{{asset('templante/HTML/js/jquery-2.2.0.min.js')}}"></script>
<script src="{{asset('templante/HTML/js/popper.min.js')}}"></script>
<script src="{{asset('templante/HTML/js/bootstrap.min.js')}}"></script>
<script  src="{{asset('templante/HTML/js/bootstrap-submenu.js')}}"></script>
<script  src="{{asset('templante/HTML/js/rangeslider.js')}}"></script>
<script  src="{{asset('templante/HTML/js/jquery.mb.YTPlayer.js')}}"></script>
<script  src="{{asset('templante/HTML/js/bootstrap-select.min.js')}}"></script>
<script  src="{{asset('templante/HTML/js/jquery.easing.1.3.js')}}"></script>
<script  src="{{asset('templante/HTML/js/jquery.scrollUp.js')}}"></script>
<script  src="{{asset('templante/HTML/js/jquery.mCustomScrollbar.concat.min.js')}}"></script>
<script  src="{{asset('templante/HTML/js/leaflet.js')}}"></script>
<script  src="{{asset('templante/HTML/js/leaflet-providers.js')}}"></script>
<script  src="{{asset('templante/HTML/js/leaflet.markercluster.js')}}"></script>
<script  src="{{asset('templante/HTML/js/dropzone.js')}}"></script> 
<script  src="{{asset('templante/HTML/js/slick.min.js')}}"></script>
<script  src="{{asset('templante/HTML/js/jquery.filterizr.js')}}"></script>
<script  src="{{asset('templante/HTML/js/jquery.magnific-popup.min.js')}}"></script>
<script  src="{{asset('templante/HTML/js/jquery.countdown.js')}}"></script>
<script  src="{{asset('templante/HTML/js/maps.js')}}"></script>
<script  src="{{asset('templante/HTML/js/app.js')}}"></script>

<!-- IE10 viewport hack for Surface/desktop Windows 8 bug -->
<script  src="{{asset('templante/HTML/js/ie10-viewport-bug-workaround.js')}}"></script>
<!-- Custom javascript -->
<script  src="{{asset('templante/HTML/js/ie10-viewport-bug-workaround.js')}}"></script>
@yield('script')
@yield('script-dropzone')
</body>
</html>
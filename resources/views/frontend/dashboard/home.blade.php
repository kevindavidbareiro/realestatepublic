@extends('frontend.dashboard.masterDashboard')

@section('stylus')


@endsection


@section('content_dashboard')
        
<div class="dashboard-header clearfix">
    <div class="row">
        <div class="col-sm-12 col-md-6"><h4>Hola , {{ auth()->user()->name }}</h4></div>
        <div class="col-sm-12 col-md-6">
            <div class="breadcrumb-nav">
                <ul>
                    <li>
                        <a href="{{ url('/') }}">Inicio</a>
                    </li>
                    <li>
                        <a href="#" class="active">Panel</a>
                    </li>
                </ul>
            </div>
        </div>
    </div>
</div>

<div class="alert alert-success alert-2" role="alert">
        <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <strong> SU LISTADO SU LISTADO HA SIDO APROBADO!</strong>
    </div>
    <div class="row">
        <div class="col-lg-3 col-md-3 col-sm-6">
            <div class="ui-item bg-success">
                <div class="left">
                    <h4>242</h4>
                    <p>Usuarios Activos</p>
                </div>
                <div class="right">
                    <i class="fa fa-map-marker"></i>
                </div>
            </div>
        </div>
        <div class="col-lg-3 col-md-3 col-sm-6">
            <div class="ui-item bg-warning">
                <div class="left">
                    <h4>425</h4>
                    <p>Listados de vistas</p>
                </div>
                <div class="right">
                    <i class="fa fa-eye"></i>
                </div>
            </div>
        </div>
        <div class="col-lg-3 col-md-3 col-sm-6">
            <div class="ui-item bg-active">
                <div class="left">
                    <h4>786</h4>
                    <p>Comentarios</p>
                </div>
                <div class="right">
                    <i class="fa fa-comments-o"></i>
                </div>
            </div>
        </div>
        <div class="col-lg-3 col-md-3 col-sm-6">
            <div class="ui-item bg-dark">
                <div class="left">
                    <h4>42</h4>
                    <p>Me Gustas</p>
                </div>
                <div class="right">
                    <i class="fa fa-heart-o"></i>
                </div>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-lg-6 col-md-6">
            <div class="dashboard-list">
                <div class="dashboard-message bdr clearfix ">
                    <div class="tab-box-2">
                        <div class="clearfix mb-30 comments-tr">
                            <span>Comentarios</span>
                            <ul class="nav nav-pills float-right" id="pills-tab" role="tablist">
                                <li class="nav-item">
                                    <a class="nav-link active show" id="pills-profile-tab" data-toggle="pill" href="#pills-profile" role="tab" aria-controls="pills-profile" aria-selected="false">Pendientes</a>
                                </li>
                                <li class="nav-item">
                                    <a class="nav-link" id="pills-contact-tab" data-toggle="pill" href="#pills-contact" role="tab" aria-controls="pills-contact" aria-selected="true">Vistos</a>
                                </li>
                            </ul>
                        </div>
                        <div class="tab-content" id="pills-tabContent">
                            <div class="tab-pane fade" id="pills-contact" role="tabpanel" aria-labelledby="pills-contact-tab">
                                <div class="comment">
                                    <div class="comment-author">
                                        <a href="#">
                                            <img src="http://placehold.it/60x60" alt="comments-user">
                                        </a>
                                    </div>
                                    <div class="comment-content">
                                        <div class="comment-meta">
                                            <h5>Graciela</h5>
                                            <div class="comment-meta">
                                                8:42 PM 1/28/2017<a href="#">Respuesta</a>
                                            </div>
                                        </div>
                                        <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Donec luctus tincidunt aliquam. Aliquam gravida massa at sem </p>
                                    </div>
                                </div>
                                <div class="comment">
                                    <div class="comment-author">
                                        <a href="#"><img src="http://placehold.it/60x60" alt="comments-user"></a>
                                    </div>
                                    <div class="comment-content">
                                        <div class="comment-meta">
                                            <h5>
                                                Veronica
                                            </h5>
                                            <div class="comment-meta">
                                                10:42 PM 1/28/2017<a href="#">Respuesta</a>
                                            </div>
                                        </div>
                                        <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Donec luctus tincidunt aliquam. Aliquam gravida massa at sem </p>
                                    </div>
                                </div>
                                <div class="comment">
                                    <div class="comment-author">
                                        <a href="#">
                                            <img src="http://placehold.it/60x60" alt="comments-user">
                                        </a>
                                    </div>
                                    <div class="comment-content">
                                        <div class="comment-meta">
                                            <h5>Graciela</h5>
                                            <div class="comment-meta">
                                                8:42 PM 1/28/2017<a href="#">Respuesta</a>
                                            </div>
                                        </div>
                                        <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Donec luctus tincidunt aliquam. Aliquam gravida massa at sem </p>
                                    </div>
                                </div>
                            </div>
                            <div class="tab-pane fade active show" id="pills-profile" role="tabpanel" aria-labelledby="pills-profile-tab">
                            <div class="comment">
                                    <div class="comment-author">
                                        <a href="#">
                                            <img src="http://placehold.it/60x60" alt="comments-user">
                                        </a>
                                    </div>
                                    <div class="comment-content">
                                        <div class="comment-meta">
                                            <h5>Graciela</h5>
                                            <div class="comment-meta">
                                                8:42 PM 1/28/2017<a href="#">Respuesta</a>
                                            </div>
                                        </div>
                                        <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Donec luctus tincidunt aliquam. Aliquam gravida massa at sem </p>
                                    </div>
                                </div>
                                <div class="comment">
                                    <div class="comment-author">
                                        <a href="#">
                                            <img src="http://placehold.it/60x60" alt="comments-user">
                                        </a>
                                    </div>
                                    <div class="comment-content">
                                        <div class="comment-meta">
                                            <h5>Graciela</h5>
                                            <div class="comment-meta">
                                                8:42 PM 1/28/2017<a href="#">Respuesta</a>
                                            </div>
                                        </div>
                                        <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Donec luctus tincidunt aliquam. Aliquam gravida massa at sem </p>
                                    </div>
                                </div>
                                <div class="comment">
                                    <div class="comment-author">
                                        <a href="#">
                                            <img src="http://placehold.it/60x60" alt="comments-user">
                                        </a>
                                    </div>
                                    <div class="comment-content">
                                        <div class="comment-meta">
                                            <h5>Graciela</h5>
                                            <div class="comment-meta">
                                                8:42 PM 1/28/2017<a href="#">Respuesta</a>
                                            </div>
                                        </div>
                                        <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Donec luctus tincidunt aliquam. Aliquam gravida massa at sem </p>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-lg-6 col-md-6">
            <div class="dashboard-list">
                <div class="dashboard-message bdr clearfix ">
                    <div class="tab-box-2">
                        <div class="clearfix mb-30 comments-tr">
                            <span>Comentarios</span>
                            <ul class="nav nav-pills float-right" id="pills-tab" role="tablist">
                                <li class="nav-item">
                                    <a class="nav-link active show" id="pills-profile-tab" data-toggle="pill" href="#pills-profile" role="tab" aria-controls="pills-profile" aria-selected="false">Pendientes</a>
                                </li>
                                <li class="nav-item">
                                    <a class="nav-link" id="pills-contact-tab" data-toggle="pill" href="#pills-contact" role="tab" aria-controls="pills-contact" aria-selected="true">Vistos</a>
                                </li>
                            </ul>
                        </div>
                        <div class="tab-content" id="pills-tabContent">
                            <div class="tab-pane fade" id="pills-contact" role="tabpanel" aria-labelledby="pills-contact-tab">
                                <div class="comment">
                                    <div class="comment-author">
                                        <a href="#">
                                            <img src="http://placehold.it/60x60" alt="comments-user">
                                        </a>
                                    </div>
                                    <div class="comment-content">
                                        <div class="comment-meta">
                                            <h5>Graciela</h5>
                                            <div class="comment-meta">
                                                8:42 PM 1/28/2017<a href="#">Respuesta</a>
                                            </div>
                                        </div>
                                        <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Donec luctus tincidunt aliquam. Aliquam gravida massa at sem </p>
                                    </div>
                                </div>
                                <div class="comment">
                                    <div class="comment-author">
                                        <a href="#"><img src="http://placehold.it/60x60" alt="comments-user"></a>
                                    </div>
                                    <div class="comment-content">
                                        <div class="comment-meta">
                                            <h5>
                                                Veronica
                                            </h5>
                                            <div class="comment-meta">
                                                10:42 PM 1/28/2017<a href="#">Respuesta</a>
                                            </div>
                                        </div>
                                        <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Donec luctus tincidunt aliquam. Aliquam gravida massa at sem </p>
                                    </div>
                                </div>
                                <div class="comment">
                                    <div class="comment-author">
                                        <a href="#">
                                            <img src="http://placehold.it/60x60" alt="comments-user">
                                        </a>
                                    </div>
                                    <div class="comment-content">
                                        <div class="comment-meta">
                                            <h5>Graciela</h5>
                                            <div class="comment-meta">
                                                8:42 PM 1/28/2017<a href="#">Respuesta</a>
                                            </div>
                                        </div>
                                        <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Donec luctus tincidunt aliquam. Aliquam gravida massa at sem </p>
                                    </div>
                                </div>
                            </div>
                            <div class="tab-pane fade active show" id="pills-profile" role="tabpanel" aria-labelledby="pills-profile-tab">
                            <div class="comment">
                                    <div class="comment-author">
                                        <a href="#">
                                            <img src="http://placehold.it/60x60" alt="comments-user">
                                        </a>
                                    </div>
                                    <div class="comment-content">
                                        <div class="comment-meta">
                                            <h5>Graciela</h5>
                                            <div class="comment-meta">
                                                8:42 PM 1/28/2017<a href="#">Respuesta</a>
                                            </div>
                                        </div>
                                        <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Donec luctus tincidunt aliquam. Aliquam gravida massa at sem </p>
                                    </div>
                                </div>
                                <div class="comment">
                                    <div class="comment-author">
                                        <a href="#">
                                            <img src="http://placehold.it/60x60" alt="comments-user">
                                        </a>
                                    </div>
                                    <div class="comment-content">
                                        <div class="comment-meta">
                                            <h5>Graciela</h5>
                                            <div class="comment-meta">
                                                8:42 PM 1/28/2017<a href="#">Respuesta</a>
                                            </div>
                                        </div>
                                        <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Donec luctus tincidunt aliquam. Aliquam gravida massa at sem </p>
                                    </div>
                                </div>
                                <div class="comment">
                                    <div class="comment-author">
                                        <a href="#">
                                            <img src="http://placehold.it/60x60" alt="comments-user">
                                        </a>
                                    </div>
                                    <div class="comment-content">
                                        <div class="comment-meta">
                                            <h5>Graciela</h5>
                                            <div class="comment-meta">
                                                8:42 PM 1/28/2017<a href="#">Respuesta</a>
                                            </div>
                                        </div>
                                        <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Donec luctus tincidunt aliquam. Aliquam gravida massa at sem </p>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
       
    </div>

@endsection


@section('script')


@endsection
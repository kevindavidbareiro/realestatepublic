@extends('frontend.master')

@section('stylus')


@endsection


@section('content_frontend')

@include('frontend.section.searcherAndUpload')
@include('frontend.section.featuredRealEstate')
@include('frontend.section.featuredCompanies')
@include('frontend.section.popularCities')
@include('frontend.section.partners')

@endsection
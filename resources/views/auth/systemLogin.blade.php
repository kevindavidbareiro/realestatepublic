<!DOCTYPE html>
<html lang="en" >
	<!-- begin::Head -->
	<head>
		<meta charset="utf-8" />
		<title>
			Access | System
		</title>
		<meta name="description" content="Latest updates and statistic charts">
		<meta http-equiv="X-UA-Compatible" content="IE=edge">
		<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
		<!--begin::Web font -->
		<script src="https://ajax.googleapis.com/ajax/libs/webfont/1.6.16/webfont.js"></script>
		<script>
          WebFont.load({
            google: {"families":["Poppins:300,400,500,600,700","Roboto:300,400,500,600,700"]},
            active: function() {
                sessionStorage.fonts = true;
            }
          });
		</script>
		<link rel="stylesheet" type="text/css" href="{{asset('templanteBack/metronic/default/assets/vendors/base/vendors.bundle.css')}}" />
		<link rel="stylesheet" type="text/css" href="{{asset('templanteBack/metronic/default/assets/demo/default/base/style.bundle.css')}}" />
		<link rel="stylesheet" type="text/css" href="{{asset('css/style.css')}}">
	</head>
    <body class="m--skin- m-header--fixed m-header--fixed-mobile m-aside-left--enabled m-aside-left--skin-dark m-aside-left--offcanvas m-footer--push m-aside--offcanvas-default"  >
		<!-- begin:: Page -->
		<div class="m-grid m-grid--hor m-grid--root m-page">
			<div class="m-grid__item m-grid__item--fluid m-grid m-grid--hor m-login m-login--signin m-login--2 m-login-2--skin-2" id="m_login" style="background-image: url(../../../assets/app/media/img//bg/bg-3.jpg);">
				<div class="m-grid__item m-grid__item--fluid	m-login__wrapper">
					<div class="m-login__container">
						<div class="m-login__logo">
							<a href="#">
								<img class="logo-realestate" src="{{asset('img/logo.png')}}"/>
							</a>
						</div>
						<div class="m-login__signin">
							<div class="m-login__head">
								<h3 class="m-login__title">
									Admin
								</h3>
							</div>
                            <form method="POST" class="m-login__form m-form" action="{{ route('system.login.submit') }}">
                                @csrf
								<div class="form-group m-form__group">
                                    <input id="email" type="email" class="form-control m-input" placeholder="Email" name="email" value="{{ old('email') }}" required autocomplete="off" autofocus/>
                                    @error('email')
                                        <span class="invalid-feedback" role="alert">
                                            <strong>{{ $message }}</strong>
                                        </span>
                                    @enderror
								</div>
								<div class="form-group m-form__group">
									<input class="form-control m-input m-login__form-input--last" type="password" placeholder="Contraseña" name="password" required autocomplete="current-password"/>
                                    @error('password')
                                        <span class="invalid-feedback" role="alert">
                                            <strong>{{ $message }}</strong>
                                        </span>
                                    @enderror
								</div>	
								@foreach ($errors->all() as $error)
									<div class="alert alert-danger" role="alert">
										<strong>
											Mensaje
										</strong>
										<li>{{ $error }}</li>
									</div>
								@endforeach
								<div class="m-login__form-action">
									<button type="submit" id="m_login_signin_submit" class="btn btn-danger m-btn m-btn--pill m-btn--custom m-btn--air m-login__btn m-login__btn--danger">
										Iniciar Sesion
									</button>
								</div>
							</form>
						</div>
					</div>
				</div>
			</div>
		</div>
	</body>
</html>
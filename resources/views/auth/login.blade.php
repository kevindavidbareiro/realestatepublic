@extends('frontend.master')
@section('stylus')
<style>
    .sticky-header .navbar-expand-lg .navbar-nav .nav-link {
        color: #515151;
    }

</style>
@endsection

@section('content_frontend')
<div class="contact-section overview-bgi" style="padding-top: 80px;">
        <div class="row">
            <div class="col-lg-12">
                <!-- Form content box start -->
                <div class="form-content-box">
                    <!-- details -->
                    <div class="details">
                        <!-- Logo -->
                        <a href="index.html">
                            <img src="img/black-logo.png" class="cm-logo" alt="lOGO INMOBILIARIA">
                        </a>
                        <!-- Name -->
                        <h3>Inciar Sesion</h3>
                        <!-- Form start -->
                        @if ($errors->any())
                            <div class="alert alert-danger">
                                <ul>
                                    @foreach ($errors->all() as $error)
                                        <li>{{ $error }}</li>
                                    @endforeach
                                </ul>
                            </div>
                        @endif
                        <form action="{{ url('login') }}" method="POST">
                        	@csrf
                            <div class="form-group">
                                <input type="email" name="email" class="input-text" placeholder="correo@email.com">
                            </div>
                            <div class="form-group">
                                <input type="password" name="password" class="input-text" placeholder="Password">
                            </div>
                            <div class="checkbox">
                                <div class="ez-checkbox pull-left">
                                    <label>
                                        <input type="checkbox" class="ez-hide">
                                        Recordar
                                    </label>
                                </div>
                                <a href="forgot-password.html" class="link-not-important pull-right">Recuperar Contraseña</a>
                                <div class="clearfix"></div>
                            </div>
                            <div class="form-group mb-0">
                                <button type="submit" class="btn-md button-theme btn-block">Iniciar Sesion</button>
                            </div>
                        </form>
                        <!-- Social List -->
                        <ul class="social-list clearfix">
                            <li><a href="#" class="facebook-bg"><i class="fa fa-facebook"></i></a></li>
                            <li><a href="#" class="twitter-bg"><i class="fa fa-twitter"></i></a></li>
                            <li><a href="#" class="google-bg"><i class="fa fa-google-plus"></i></a></li>
                            <li><a href="#" class="linkedin-bg"><i class="fa fa-linkedin"></i></a></li>
                        </ul>
                    </div>
                    <!-- Footer -->
                    <div class="footer">
                        <span>Todavia No Te Registraste? <a href="{{ url('register') }}">Crear Cuenta</a></span>
                    </div>
                </div>
                <!-- Form content box end -->
            </div>
        </div>
</div>
@endsection

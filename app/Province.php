<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\Realestate;
use App\Country;
use App\Localitie;

class Province extends Model
{
    
    public function realestate(){
        return $this->hasMany(Realestate::class);
    }

    public function comment(){
        return $this->belongsTo(Comment::class);
    }


    public function country(){
        return $this->belongsTo(Country::class);
    }

    
    public function localitie(){
        return $this->belongsTo(Localitie::class);
    }


    
}

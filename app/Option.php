<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Option extends Model
{
    public function realestate_option()
    {
        return $this->belongsToMany('App\Realestate', 'realestate_options', 'option_id', 'realestate_id');
    } 
}

<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\TypeOfContact;
use App\User;

class Contact extends Model
{

    public function typeofcontacts(){
        return $this->hasMany(TypeOfContact::class);
    }

    public function users(){
        return $this->belongsTo(User::class);
    }
}

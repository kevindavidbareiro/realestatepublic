<?php

namespace App;
use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;

 

class System extends Authenticatable

{

    use Notifiable;

    protected $guard = 'system';

    protected $fillable = [
        'name', 'email', 'password','rol_id',
    ];

    protected $hidden = [
        'password', 'remember_token',
    ];

}
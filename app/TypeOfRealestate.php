<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class TypeOfRealestate extends Model
{
    public function realestate(){
        return $this->hasMany(Realestate::class);
    }
}

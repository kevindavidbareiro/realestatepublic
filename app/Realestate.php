<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

use App\Citie;
use App\Comment;
use App\Countrie;
use App\Province;
use App\Localitie;
use App\State;
use App\Video;
use App\Multimedia;
use User;


class Realestate extends Model
{
    public function comment(){
        return $this->hasMany(Comment::class);
    }

    public function video(){
        return $this->hasMany(Video::class);
    }

    public function multimedia(){
        return $this->hasMany(Multimedia::class);
    }

    public function countries(){
        return $this->belongsTo(Countrie::class);
    }

    public function provinces(){
        return $this->belongsTo(Province::class);
    }

    public function localities(){
        return $this->belongsTo(Localitie::class);
    }

    public function cities(){
        return $this->belongsTo(Citie::class);
    }

    public function states(){
        return $this->belongsTo(State::class);
    }

    public function type_of_realestate(){
        return $this->belongsTo(TypeOfRealestate::class);
    }

    public function option()
    {
        return $this->belongsToMany('App\Option', 'realestate_options', 'realestate_id', 'option_id');
    }   

    public function user(){
        return $this->belongsTo(User::class);
    }
}

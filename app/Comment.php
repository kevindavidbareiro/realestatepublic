<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\User;
use App\Realestate;
use App\Reply;

class Comment extends Model
{
    //
    public function user(){
        return $this->belongsTo(User::class);
    }

    public function realestate(){
        return $this->belongsTo(Realestate::class);
    }
    public function reply(){
    	return $this->hasMany(Reply::class);
    }
}

<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\Realestate;

class State extends Model
{
    public function realestate(){
        return $this->belongsTo(Realestate::class);
    }

}

<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\Realestate;
class Multimedia extends Model
{
    public function realestates(){
        return $this->belongsTo(Realestate::class);
    }
}

<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\Contact;

class TypeOfContact extends Model
{
    //
    public function contact(){
        return $this->belongsTo(Contact::class);
    }
}

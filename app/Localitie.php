<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\Realestate;
use App\Province;
use App\Citie;

class Localitie extends Model
{
    //
    public function realestate(){
        return $this->hasMany(Realestate::class);
    }

    public function citie(){
        return $this->belongsTo(Citie::class);
    }

    public function provinces(){
        return $this->hasMany(Province::class);
    }
    

}

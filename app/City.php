<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

use App\Realestate;
use App\Localitie;

class City extends Model
{
    //
    public function realestate(){
        return $this->hasMany(Realestate::class);
    }

    public function localities(){
        return $this->hasMany(Localitie::class);
    }
}

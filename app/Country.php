<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\Realestate;
use App\Province;

class Country extends Model
{
    //

    public function realestate(){
        return $this->hasMany(Realestate::class);
    }

    public function province(){
        return $this->hasMany(Province::class);
    }
}

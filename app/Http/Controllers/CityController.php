<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\City;

class CityController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //$cities = City::take(4)->orderBy('id','desc')->get();
        $cities = DB::table('cities')->take(4)
        ->join('localities', 'cities.locality_id', '=', 'localities.id')
        ->select('cities.name','cities.id','cities.locality_id','localities.name as localities')
        ->orderBy('id', 'desc')
        ->get();
        return response()->json($cities,200);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $city = new City;
        $city->name        = $request->barrio;
        $city->locality_id = $request->localidad_id;
        if($city->save()){
            return response()->json(['message'=>'Save cities','status'=>'ok'],200,[]);
        }else{
            return response()->json(['message'=>'Error save country','status'=>'error'],200,[]);
        } 
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function search(Request $request)
    {   
        $cities = DB::table('cities')
        ->join('localities', 'cities.locality_id', '=', 'localities.id')
        ->select('cities.name','cities.id','cities.locality_id','localities.name as localities')
        ->where('cities.name','like','%'.$request->city.'%')
        ->orderBy('id', 'desc')
        ->get();
        return response()->json($cities,200);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(Request $request)
    {
        $idbarrio    = $request->idbarrio;
        $locality_id = $request->localidad_id;
        $barrio_nombre      = $request->barrio;

        $barrio              = City::find($idbarrio);
        $barrio->name        =  $barrio_nombre;
        $barrio->locality_id = $locality_id;
    
       //print_r($barrio->locality_id);
        if($barrio->save())
        {
            return response()->json(['message'=>'Update city ok','status'=>'ok'],200,[]);
        }else{
            return response()->json(['message'=>'Error user ok','status'=>'error'],200,[]);
        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request)
    {
        $cities = City::find($request->idbarrio);
        if($cities->delete()){
            return response()->json(['message'=>'Delete cities','status'=>'ok'],200,[]);
        }else{
            return response()->json(['message'=>'Error save country','status'=>'error'],200,[]);
        }
    }
}

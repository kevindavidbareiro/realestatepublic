<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Alertify;
use App\Realestate;
use App\Option;
use App\Country;
use App\Province;
use App\Localitie;
use App\City;
use App\State;
use App\TypeOfRealestate;
use App\Realestate_option;
use App\User;
use App\Contact;
use App\Comment;

class PropertyController extends Controller
{
    private $properties;
     public function __construct()
      {
        $this->middleware('auth', ['except' => ['show','propertyList']] );
      }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {  
        $properties = Realestate::where('user_id', auth()->id())->get();
        $countComments = $this->countComment() ;

        return view('frontend.dashboard.property.index', compact('properties','countComments'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $countComments     = $this->countComment() ;
        $idUser            = auth()->id();
        $user              = User::find($idUser);
        $options           = Option::all();
        $countries         = Country::all();
        $provinces         = Province::all();
        $localities        = Localitie::all();
        $cities            = City::all();
        $states            = State::all();
        $typeOfRealestates = TypeOfRealestate::all();
        $celular           = $user->contact()->where('type_of_contact_id', 1)->get();


        
        return view('frontend.dashboard.property.create', compact('options','countries','provinces','localities','cities','states','typeOfRealestates','celular','countComments'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {

        $data = request()->validate([
            'propertyName'        => 'required',
            'price'               => 'required',
            'area'                => 'required',
            'bedrooms'            => 'required',
            'bathrooms'           => 'required',
            'address'             => 'required',
            'city_id'             => 'required',
            'country_id'          => 'required',
            'province_id'         => 'required',
            'state_id'            => 'required',
            'detailedInformation' => 'required',
            'name'                => 'required',
            'email'               => 'required',
            'phone'               => 'required',
            
        ], [
            'propertyName.required'  => 'Porfavor cargar un nombre',
            'price.required'         => 'Porfavor cargar un precio',
            'area.required'          => 'Porfavor cargar el área',
            'bedrooms.required'      => 'Porfavor cargar número de habitaciones',
            'bathrooms.required'     => 'Porfavor cargar número de baños',
            'address.required'       => 'Porfavor cargar dirección',
            'city_id.required'       => 'Porfavor cargar barrio',
            'country.required'       => 'Porfavor cargar pais',
            'province.required'      => 'Porfavor cargar provincia',
            'state.required'         => 'Porfavor cargar localida / estado',
            'detailedInformation'    => 'Porfavor cargar información detallada',
            'name.required'          => 'Porfavor cargar nombre',
            'email.required'         => 'Porfavor cargar email',
            'phone.required'         => 'Porfavor cargar teléfono',
            
            
        ]);

        $idUser                            = auth()->id();
        // modelo realestate
        $realestate                        = new Realestate;
        $realestate->name                  = $request->propertyName;
        $realestate->price                 = $request->price;
        $realestate->area                  = $request->area;
        $realestate->bedrooms              = $request->bedrooms;
        $realestate->bathrooms             = $request->bathrooms;
        $realestate->address               = $request->address;
        $realestate->detailedInformation   = $request->detailedInformation;   
        $realestate->state_id              = $request->state_id;
        $realestate->type_of_realestate_id = $request->typeOfRealestate_id;
        $realestate->latitude              = $request->latitude;
        $realestate->longitude             = $request->longitude;
        $realestate->country_id            = $request->country_id;
        $realestate->province_id           = $request->province_id;
        $realestate->locality_id           = $request->localitie_id;
        $realestate->city_id               = $request->city_id;
        $realestate->user_id               = $idUser;
        //  modeo contact
        $user                              = User::findorFail($idUser);
        $userCelular                       = New Contact;
        $userCelular->contact              = $request->phone;
        $userCelular->type_of_contact_id   = 1;
        $user->contact()->save($userCelular);
        
        if($realestate->save())
        {
            // Despues de guardar la propiedad, guardamos las especificaciones
            if (isset($request->specifics)) {
                $realestate_lastInsert                = Realestate::where('user_id', $idUser )->get()->last();
                $specifics                            = $request->specifics;

                foreach ($specifics as $specific) {
                    $realestate_option                = New Realestate_option;
                    $realestate_option->realestate_id = $realestate_lastInsert->id; 
                    $realestate_option->option_id     = $specific;
                    $realestate_option->save();
                }
                
            }

            return redirect('Multimedia/'.$realestate->id.'')
                    ->with('typemsg', 'success')
                    ->with('message', 'Item creado correctamente.');
        }
        else {
            return redirect('frontend.dashboard.submitProperties')
                    ->with('typemsg', 'error')
                    ->with('message', 'Upps hubo un problema al guardar el Inmueble.');
        }

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show(Request $request,$id)
    {
         $myProperty          = Realestate::findorFail($id);

         // Cargo el el array asociado a la sesion con la propiedades que va visitando el usuario
         session()->push('realestateId', ['id' => $myProperty->id, 'name' => $myProperty->name, 'price' => $myProperty->price, 'created_at' => $myProperty->created_at,'imageCover' => $myProperty->multimedia->where('main', 1) ]);
         
         $userId                 = $myProperty->user_id;
         $informationPersonal    = User::where('id', $userId)->get();
         $multimedia             = $this->getMultimedia($id);
         $propertyVideo          = $myProperty->video()->get();
         $Comments                = Comment::where('user_id',auth()->id())->where('realestate_id', $myProperty->id)->get();
         // Redes Sociales del Usuario y  celular
         $user                   = User::where('id', $userId)->get();
         $userCelular            = $user[0]->contact()->where('type_of_contact_id', 1)->get();
         $userFacebook           = $user[0]->contact()->where('type_of_contact_id', 4)->get();
         $userInstagram          = $user[0]->contact()->where('type_of_contact_id', 5)->get();
         // Propiedades visitadas recientemente
         $lastVisitedRealEstates = $this->lastVisitedRealEstates();
         // Propiedades Similares ubicadas en la misma localidad o provincia
         $similarProperties      = Realestate::where('locality_id',1)->orWhere('province_id',1)->get();

         $countMultimedia        = count($multimedia);
         $countProperties        = count($myProperty->option);

         return view('frontend.propertyDetails', compact('myProperty','informationPersonal','multimedia','countMultimedia','countProperties','userCelular', 'userFacebook','propertyVideo','userInstagram','lastVisitedRealEstates','similarProperties','Comments'));

    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $idUser            = auth()->id();
        $user              = User::find($idUser);
    	$property 		   = Realestate::findorFail($id);
        $options           = Option::all();
        $countries         = Country::all();
        $provinces         = Province::all();
        $localities        = Localitie::all();
        $cities            = City::all();
        $states            = State::all();
        $typeOfRealestates = TypeOfRealestate::all();
        $celular           = $user->contact()->where('type_of_contact_id', 1)->get();

        return view('frontend.dashboard.property.edit', compact('property','options','countries','provinces','localities','cities','states','typeOfRealestates','celular'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {   


        $data = request()->validate([
            'propertyName'        => 'required',
            'price'               => 'required',
            'area'                => 'required',
            'bedrooms'            => 'required',
            'bathrooms'           => 'required',
            'address'             => 'required',
            'city_id'             => 'required',
            'country_id'          => 'required',
            'province_id'         => 'required',
            'state_id'            => 'required',
            'detailedInformation' => 'required',
            'name'                => 'required',
            'email'               => 'required',
            'phone'               => 'required',
            
        ], [
            'propertyName.required'  => 'Porfavor cargar un nombre',
            'price.required'         => 'Porfavor cargar un precio',
            'area.required'          => 'Porfavor cargar el área',
            'bedrooms.required'      => 'Porfavor cargar número de habitaciones',
            'bathrooms.required'     => 'Porfavor cargar número de baños',
            'address.required'       => 'Porfavor cargar dirección',
            'city_id.required'       => 'Porfavor cargar barrio',
            'country.required'       => 'Porfavor cargar pais',
            'province.required'      => 'Porfavor cargar provincia',
            'state.required'         => 'Porfavor cargar localida / estado',
            'detailedInformation'    => 'Porfavor cargar información detallada',
            'name.required'          => 'Porfavor cargar nombre',
            'email.required'         => 'Porfavor cargar email',
            'phone.required'         => 'Porfavor cargar teléfono',
            
            
        ]);
        
        $idUser                            = auth()->id();
        
        $realestate                        = Realestate::findOrFail($id);
        $realestate->name                  = $request->propertyName;
        $realestate->price                 = $request->price;
        $realestate->area                  = $request->area;
        $realestate->bedrooms              = $request->bedrooms;
        $realestate->bathrooms             = $request->bathrooms;
        $realestate->address               = $request->address;
        $realestate->detailedInformation   = $request->detailedInformation;   
        $realestate->state_id              = $request->state_id;
        $realestate->type_of_realestate_id = $request->typeOfRealestate_id;
        $realestate->latitude              = $request->latitude;
        $realestate->longitude             = $request->longitude;
        $realestate->country_id            = $request->country_id;
        $realestate->province_id           = $request->province_id;
        $realestate->locality_id           = $request->localitie_id;
        $realestate->city_id               = $request->city_id;
        $realestate->user_id               = $idUser;
        
        if($realestate->save())
        {
            if (isset($request->specifics)) {
                $realestate_id = $id; 
                $deleteOptions = Realestate_option::where('realestate_id', $id)->delete();
                $specifics     = $request->specifics;


                foreach ($specifics as $specific) {
                    $realestate_option                = New Realestate_option;
                    $realestate_option->realestate_id = $realestate_id;
                    $realestate_option->option_id     = $specific;
                    $realestate_option->save();

                }
                
            }

            return redirect('propiedades')
                    ->with('typemsg', 'success')
                    ->with('message', 'La Propiedad se a modificado correctamente. ');
        }
        else {
            return redirect('propiedades')
                    ->with('typemsg', 'error')
                    ->with('message', 'Upps hubo un problema al modificar el Inmueble.');
        }

    }
    

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request)
    {
        $property_id = $request->property_id;
        $property    = Realestate::findorFail($property_id);
        if($property->delete())
        {
            return redirect('propiedades')
                ->with('typemsg', 'success')
                ->with('message', 'Propiedad borrada correctamente.');
        }
        else {
            return redirect('propiedades')
                ->with('typemsg', 'error')
                ->with('message', 'Upps al parecer hubo un error al borrar la propiedad.');
        }
    }
    // Metodo para guardar los comentarios enviados
    public function commentStore (Request $request,$id)
    {
        $propertyComent                = New Comment;
        $propertyComent->comment       = $request->comment;
        $propertyComent->realestate_id = $id;
        $propertyComent->user_id       = auth()->id();
        $propertyComent->answered      = 0;  

        if ($propertyComent->save()) {
            return redirect()->back()
                             ->with('typemsg', 'success')
                             ->with('message', 'Mensaje Enviado Correctamente.');
        }
        else {
            return redirect()->back()
                             ->with('typemsg', 'error')
                             ->with('message', 'Hubo un error al enviar el mensaje. Intenta nuevamente.');
        }

    }

    // Metodo que retorna la ultimas 5 imagenes cargadsa de una propiedad
    public function getMultimedia($id)
    {
        $realestate = Realestate::findorFail($id);
        $multimedia = $realestate->multimedia()->latest()->take(5)->get();

        return $multimedia;

    }

    
    // metodo que retorna la imagen de portada de una propiedad
    public function getCoverImage($id)
    {
        $realestate = Realestate::findorFail($id);
        $coverImage = $realestate->multimedia()->where('main',1)->get();

        return $coverImage;

    }
    // Metodo que retorna las ultimas propiedades visitadas
    public function lastVisitedRealEstates ()
    {
        // Elimino las propiedades repetidas si es que las hay
        $lastRealEstates = array_unique(session('realestateId'), SORT_REGULAR);

        return $lastRealEstates;

    }

     public function countComment(){

        // Obtengo todos los inmuebles publicado por el usuario logueado
        $realestates = Realestate::where('user_id', auth()->id())->get();
        // defino el array que contendra los id de los inmuebles que tiene publicado
        $realestatesId = array();
        // lleno el array con los id 
        foreach ($realestates as $realestate) {
            array_push($realestatesId, $realestate->id);
        }
        // Rescato los comentarios correspondientes a esos inmuebles que todavia no  fueron contestados y los cuento
        $commentsCount  = Comment::select('*')
                    ->whereIn('realestate_id', $realestatesId)->where('answered', '=', '0')
                    ->count();
        return $commentsCount;

    }
   
}

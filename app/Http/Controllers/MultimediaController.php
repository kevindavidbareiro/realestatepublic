<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\File;
//use Illuminate\Http\UploadedFile;

use App\Http\Helpers\Helpers;
use App\Multimedia;
use App\Realestate;
use Image;


class MultimediaController extends Controller
{    
    const MESSAGE_SUCCESS = 1;
    const MESSAGE_ERROR   = 2;

    public function index(){
      
    } 


    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $RealestateMultimedia =  Realestate::with('multimedia')->find($id);
        $images = [];
        foreach ($RealestateMultimedia['multimedia'] as $img) {
           
            $images[] = [$img->id,$img->url];
            
        }

        //busco la imagen principal anterior para cambiar el campo main
        $main = Multimedia::where("realestate_id","=",$id)
                                    ->where("main","=",1)
                                    ->get();
        
        return view('frontend.dashboard.multimedia2')
                    ->with('idRealestate', $id)
                    ->with('images', $images)
                    ->with('main', $main);
    }
    public function storeMain(Request $request ,$id , $name){
        
        
        //obtengo la ruta de la imagen original
        $urlImgstart = public_path() . '/img/realestates/'.$name;
        //obtengo la ruta de la imagen final
        $urlImgEnd   = public_path() . '/img/realestates/main/';
        //creo una nueva imagen
        $imgMain     = Image::make($urlImgstart);
        //la redimenciono
        $imgMain->fit(540, 360);
        //guardo imagen en archivo
        $imgMain->save($urlImgEnd . $name);
        

        //busco la nueva imagen principal para cambiar el campo main
        $multimediaMain = Multimedia::find($id);
        $RealestateId   = $multimediaMain->realestate_id;

        
        //busco la imagen principal anterior para cambiar el campo main
        $PreviousMain = Multimedia::where("realestate_id","=",$RealestateId)
                                    ->where("main","=",1)
                                    ->get();
        

        if (count($PreviousMain) != 0) {
            $PreviousMain[0]->main   = 0;
            $multimediaMain->main    = 1;
            $name      = $PreviousMain[0]->url;
            $images    = File::files(public_path() . '/img/realestates/main');
            
            $deleteImg = array_filter($images, function ($var) use ($name) {
                return preg_match("/\b$name\b/i", $var);
            });

            if ($PreviousMain[0]->save() && $multimediaMain->save() && File::delete($deleteImg)) {
                return 'ok';
            }
        }

        $multimediaMain->main    = 1;
        if ($multimediaMain->save()){
            return 'ok';
        }
        
    }


   
    public function store(Request $request){

        $idRealestate   = $request->idRealestate;
        $path_img = public_path() . '/img/realestates/';
        
        foreach ($request->file('file') as $file) {
            
            $originalName   = $file->getClientOriginalName();
            $type           = explode(".", $originalName);
            $Cant           = count($type);
            $name           = Helpers::generateRandomString();
            $imgName        = $idRealestate .'-'.$name.'.'.$type[$Cant-1];
            $img            = Image::make($file);
            //la redimenciono
            $img->fit(730, 486);
            //guardo imagen en archivo
            $img->save($path_img . $imgName);

            
            //Image::make($file)->save($path_img . $imgName);
            
            $Multimedia = new Multimedia;

            $Multimedia->url           = $imgName;
            $Multimedia->realestate_id = $idRealestate;
            $Multimedia->save();
            
            
        }

        return redirect('Multimedia/'.$idRealestate.'')
            ->with('message_type', self::MESSAGE_SUCCESS)
            ->with('message', 'LA IMAGEN SE AGREGÓ CORRECTAMENTE.');
    }
    
    public function edit($id)
    {
        
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
       
        
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $multimedia = Multimedia::find($id);
        $name      = $multimedia->name;
        $images    = File::files(public_path() . '/img/');
        $deleteImg = array_filter($images, function ($var) use ($name) {
            return preg_match("/\b$name\b/i", $var);
        });

        if ($multimedia->delete() and File::delete($deleteImg)) {
            return 'ok';
        }
        return 'no';
        
    }

}

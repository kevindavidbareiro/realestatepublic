<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Province;

class ProvinceController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $province = DB::table('provinces')->take(4)
        ->join('countries', 'provinces.country_id', '=', 'countries.id')
        ->select('provinces.name','provinces.id','provinces.country_id','countries.name as countries')
        ->orderBy('id', 'desc')
        ->get();
        return response()->json($province,200);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function list()
    {
        $province = Province::all();
        return response()->json($province,200);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $province              = new Province;
        $province->name        = $request->provincia;
        $province->country_id = $request->pais_id;
        if($province->save()){
            return response()->json(['message'=>'Save province','status'=>'ok'],200,[]);
        }else{
            return response()->json(['message'=>'Error save province','status'=>'error'],200,[]);
        } 
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(Request $request)
    {
        $province              = Province::find($request->idprovincia);
        $province->name        = $request->provincia;
        $province->country_id  = $request->pais_id;

        if($province->save()){
            return response()->json(['message'=>'Update province','status'=>'ok'],200,[]);
        }else{
            return response()->json(['message'=>'Error province','status'=>'error'],200,[]);
        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request)
    {
        $province = Province::find($request->idprovincia);
        if($province->delete()){
            return response()->json(['message'=>'Delete province','status'=>'ok'],200,[]);
        }else{
            return response()->json(['message'=>'Error save country','status'=>'error'],200,[]);
        }
    }
}

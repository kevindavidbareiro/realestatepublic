<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Province;
use App\Localitie;
use App\City;

class LocationController extends Controller
{
    

    public function getProvincesByCountry($id)
    {
        $provinces = Province::where('country_id', '=', $id)->get();

        return response()->json([
            'provinces' => $provinces
        ]);

    }

    public function getLocalitiesByProvince($id)
    {
        $localities = Localitie::where('province_id', '=', $id)->get();

        return response()->json([
            'localities' => $localities
        ]);

    }

    public function getCitiesByLocalitie($id)
    {
        $cities = City::where('locality_id', '=', $id)->get();

        return response()->json([
            'cities' => $cities
        ]);

    }

}  
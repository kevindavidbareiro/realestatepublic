<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        // sesion donde se iran almacenando las propiedades visitadas por el usuario
        session()->put('realestateId', array());
        alertify()->success('Probando Alertify directamente desde web.php');

        return view('home');
    }
}

<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;
use Illuminate\Auth\Events\Registered;
use App\User;

class UserController extends Controller
{
	 public function __construct()
    {
    	$this->middleware('guest',['except' => ['logout']]);
    }



    public function login()
    {
        return view('frontend.user.login');
    }
    
    public function authenticate(Request $request)
    {
        
        if (Auth::attempt(['email' => $request->email, 'password' => $request->password])) {
            return redirect('/');
        }
        else {
            return redirect('/loginUser')
                ->with('errors','El Usuario o la Contraseña son Incorrectos.');
        }
    }

    public function logout()
    {
        Auth::logout();
        
        return redirect('/');
    }

    public function showRegistrationForm()
    {
        return view('frontend.user.register');
    }

    protected function validator(array $data)
    {
        return Validator::make($data, [
            'name' => ['required', 'string', 'max:255'],
            'email' => ['required', 'string', 'email', 'max:255', 'unique:users'],
            'password' => ['required', 'string', 'min:6', 'confirmed'],
        ], [
            'name.required'      => 'El campo nombre es obligatorio',
            'name.string'        => 'El campo nombre solo acepta cadena de textos',
            'name.max'           => 'El campo nombre tiene que tener una longitud maxima de 255 caracteres',
            'email.required'     => 'El campo email es obligatorio',
            'email.string'       => 'El campo email solo acepta cadena de textos',
            'email.max'          => 'El campo email tiene que tener una longitud maxima de 255 caracteres',
            'email.unique'       => 'El email ya se encuentra registrado',
            'password.required'  => 'El campo contraseña es obligatorio',
            'password.string'    => 'El campo contraseña solo acepta cadena de textos',
            'password.min'       => 'La contraseña debe contener 6 caracteres como minimo',
            'password.confirmed' => 'La contraseñas no coinciden',

            
        ]);
    }


    protected function create(array $data)
    {
        return User::create([
            'name' => $data['name'],
            'email' => $data['email'],
            'password' => Hash::make($data['password']),
        ]);
    }



    public function register(Request $request)
    {
        $this->validator($request->all())->validate();

        event(new Registered($user = $this->create($request->all())));

        $this->guard()->login($user);

        return redirect('/');
    }

    
    protected function guard()
    {
        return Auth::guard();
    }

    

}

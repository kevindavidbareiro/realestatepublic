<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Realestate;
use App\Country;
use App\Province;
use App\Localitie;
use App\City;
use App\State;
use App\TypeOfRealestate;
use App\Realestate_option;
use App\User;

class SearchesController extends Controller
{
    public function index(Request $request){
        $state     = $request->city_id;
        $rooms     = $request->rooms_id;
        $country   = $request->country_id;
        $province  = $request->province_id;
        $localitie = $request->localitie_id;
        $city      = $request->city_id;
        
        //oneCity
        if ($city != 'all'){
            $listRealestates = $this->oneCity($city,$rooms);
        }
        
        
        //allCity of localitie
        if ($city == 'all' and $localitie != 'all'){
            $listRealestates = $this->allCities($localitie,$rooms);
        
        }

        //alllocalitie of province
        if ($localitie == 'all' and $province != 'all'){
            $listRealestates = $this->allLocalities($province,$rooms);
        }

        $typeOfRealestates = TypeOfRealestate::all();
        $countries         = Country::all();
        $provinces         = Province::all();

        return view('frontend.properties-list')
                    ->with('typeOfRealestates',$typeOfRealestates)
                    ->with('listRealestates',$listRealestates)
                    ->with('countries',$countries)
                    ->with('provinces',$provinces);
    }

    public function oneCity ($city , $rooms)
    {
        $realestates = Realestate::where(['city_id'=>$city,'bedrooms'=>$rooms])->get();

        return $realestates;
    }
    
    
    public function allCities ($localitie , $rooms)
    {
        $realestates = Realestate::where(['locality_id'=>$localitie,'bedrooms'=>$rooms])->get();

        return $realestates;
    }

    public function allLocalities ($province , $rooms)
    {
        $realestates = Realestate::where(['province_id'=>$province,'bedrooms'=>$rooms])->get();

        return $realestates;
    }


}

<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Realestate;
use App\User;
use App\Country;
use App\Province;
use App\Localitie;
use App\City;
use App\State;
use App\TypeOfRealestate;


class FrontendController extends Controller
{    
    
  
   
    
    //Funcion que envia los modelos al Frontend
    public function index(){
        
        $realestate        = Realestate::all();     
        $user              = User::all();
        $countries         = Country::all();
        $provinces         = Province::all();
        $localities        = Localitie::all();
        $cities            = City::all();
        $states            = State::all();
        $typeOfRealestates = TypeOfRealestate::all();


        return view('frontend.home')
            ->with('realestate', $realestate)
            ->with('user',$user)
            ->with('countries',$countries)
            ->with('provinces',$provinces)
            ->with('localities',$localities)
            ->with('cities',$cities)
            ->with('states',$states)
            ->with('typeOfRealestates',$typeOfRealestates);
                    
    }

    
    public function LastFeactures(){
        
   }

   
    
}
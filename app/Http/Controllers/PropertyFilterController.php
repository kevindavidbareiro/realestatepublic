<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Realestate;
use App\TypeOfRealestate;
use App\Country;
use App\Province;

class PropertyFilterController extends Controller
{
    // Metodo que devuelve la vista del listado de propiedades para el usuario que esta navegando
    public function propertyList()
    {
        $realestates= Realestate::paginate(10);
        $typeOfRealestates = TypeOfRealestate::all();
        $countries         = Country::all();
        $provinces         = Province::all();
        // $bedRooms		   = DB::table('realestates')->select('bedrooms')->distinct()->orderBy('bedrooms','asc')				 ->get();
        // $bathRooms		   = DB::table('realestates')->select('bathrooms')->distinct()->orderBy('bathrooms','asc')				 ->get();
        
        return view('frontend.properties-list', compact('realestates','typeOfRealestates','countries','provinces')) ;
    }
}

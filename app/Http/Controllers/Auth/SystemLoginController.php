<?php

 

namespace App\Http\Controllers\Auth;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Auth;
 

class SystemLoginController extends Controller
{

    
    public function __construct(){
        $this->middleware('guest:system',['except'=>['logout']]);
    }


    public function showLoginForm(){
       return view('auth.systemLogin');
    }


    public function login(Request $request){
        $this->validate($request,[
            'email'=> 'required|email',
            'password'=> 'required|min:6',
        ]);

        if(Auth::guard('system')->attempt(['email' => $request->email,'password'=>$request->password],$request->remember)){
            #code
            return redirect()->intended(route('system.dashboard'));
        }

        return redirect()->back()->withInput($request->only('email','remember'));
    }


    public function logout(){
        Auth::guard('system')->logout();
        return redirect(route('system.login'));
    }

}
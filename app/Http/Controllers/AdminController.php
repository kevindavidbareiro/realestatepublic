<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Auth;
use App\Realestate;
use App\User;
use App\Contact;
use App\Comment;
use Image;


class AdminController extends Controller
{    



    public function panel(){
        $countComments = $this->countComment() ;
        return view('frontend.dashboard.home', compact('countComments'));
                
    }

    public function messages(){
        $countComments = $this->countComment() ;
        return view('frontend.dashboard.message',compact('countComments'));
                
    }
    public function bookings(){
        $countComments = $this->countComment() ;
        return view('frontend.dashboard.bookings',compact('countComments'));
                
    }

    public function myProfile(){
        //dd(session('realestateId'));
        $countComments = $this->countComment() ;
        $userId        = auth()->id();
        $user          = User::where('id', $userId)->get();
        $userData      = $user[0]->data;
        $userCelular   = $user[0]->contact()->where('type_of_contact_id', 1)->get();
        $userTelephone = $user[0]->contact()->where('type_of_contact_id', 2)->get();
        $userFacebook  = $user[0]->contact()->where('type_of_contact_id', 4)->get();
        $userInstagram = $user[0]->contact()->where('type_of_contact_id', 5)->get();
        $userWhatsapp  = $user[0]->contact()->where('type_of_contact_id', 6)->get();
        
        return view('frontend.user.profile', compact('user','userData','userCelular', 'userTelephone','userFacebook','userInstagram','userWhatsapp','countComments'));
                
    }

    public function storeProfile(Request $request){

        $data = request()->validate([
            'name'         => 'required',
            'celular'      => 'numeric|nullable',
            'phone'        => 'numeric|nullable',
            'infoPersonal' => 'string|nullable',
            
            
        ], [
            'name.required'       => 'Porfavor cargar el nombre',
            'celular.numeric'     => 'Porfavor cargar solo numeros',
            'phone.numeric'       => 'Porfavor cargar solo numeros',
            'infoPersonal.string' => 'Porfavor cargar solamente texto',
            
            
        ]);



        $userId     = auth()->id();
        $user       = User::findOrFail($userId);
        $user->name = $request->name;
        $user->data = $request->infoPersonal;

        //  Verificación para saber si se cargo un avatar
        if($request->hasFile('avatar')) {
            $image     = $request->file('avatar');
            // Asingo el nombre original del archivo
            $imageName = $image->getClientOriginalName();
            // Guardo en la carpeta public/img/user
            $directory = public_path('/img/user/');
            $imageUrl  = $directory.$imageName;
            // Redimenciono la imagen
            Image::make($image)->resize(198, 165)->save($imageUrl);
            // Borro el avatar anterior
            $userCurrentImg    = $user->imgProfile;
            $imageCurrenUrl    = $directory.$userCurrentImg;
            if ($userCurrentImg) {
                $imageCurrenDelete = unlink($imageCurrenUrl);
            }
            // Guardo el nombre de la imagen en la BD
            $user->imgProfile = $imageName;
        } 
        
        if ($request->celular) {
            $deleteCelular = Contact::where('user_id', $userId)->where('type_of_contact_id', 1)->delete();
            $userCelular                     = New Contact;
            $userCelular->contact            = $request->celular;
            $userCelular->type_of_contact_id = 1;
            $user->contact()->save($userCelular);
         }

         if ($request->phone) {
            $deletePhone = Contact::where('user_id', $userId)->where('type_of_contact_id', 2)->delete();
            $userPhone                     = New Contact;
            $userPhone->contact            = $request->phone;
            $userPhone->type_of_contact_id = 2;
            $user->contact()->save($userPhone);
         }


         if ($user->save()) {
             return redirect()->back()
                    ->with('success', 'Informacion de Perfil Actualizada Correctamente. ');
         }

         else {
            return redirect()->back()
                    ->with('error', 'Hubo un problema al modificar el usuario.');
        }

        
    }

    public function changePassword(Request $request){

        $data = request()->validate([
            'current_password' => 'required',
            'new_password'     => 'required|string|min:6|confirmed',
            
            
        ], [
            'current_password.required' => 'Porfavor escriba su contraseña actual',
            'new_password.required'     => 'Porfavor escriba su contraseña nueva',
            'new_password.min'          => 'La nueva contraseña debe poseer minimo 6 caracteres',
            'new_password.confirmed'    => 'Las contraseñas nuevas no coinciden',
            
            
            
        ]);
        $currentPassword = $request->current_password;
        $newPassword     = $request->new_password;

        // Comprobacion si la contraseña escrita coincide con la guardada en la BD
        if (!(Hash::check($currentPassword,Auth::user()->password))) {
            return redirect()->back()
                             ->with('error', "La Contraseña Proporcionada no coincide con nuestro registro");
        }

        // Evita que la contraseña nueva coincida con la que esta guardada en la BD
        if ($currentPassword == $request->new_password) {
            return redirect()->back()
                             ->with('error', "La nueva contraseña no puede coincidir con la Actual");
        }

        // Si pasa las dos comprobacion entonces actualiza la nueva contraseña
        $user = Auth::user();
        $user->password = bcrypt($request->new_password);
        $user->save();
            
            return redirect()->back()->with("success","Contraseña cambiada Exitosamente");
        
 

    }

    public function socialNetwork(Request $request){

        $userId = auth()->id();
        $user   = User::findOrFail($userId);

        if ($request->facebook or $request->facebook == null) {
            $deleteFacebook = Contact::where('user_id', $userId)->where('type_of_contact_id', 4)->delete();
            if(isset($request->facebook))
            {
                $userFacebook                     = New Contact;
                $userFacebook->contact            = $request->facebook;
                $userFacebook->type_of_contact_id = 4;
                $user->contact()->save($userFacebook);
            }
            
         }

        if ($request->instagram or $request->instagram == null) {
            $deleteInstagram = Contact::where('user_id', $userId)->where('type_of_contact_id', 5)->delete();
            if(isset($request->instagram))
            {
                $userInstagram                     = New Contact;
                $userInstagram->contact            = $request->instagram;
                $userInstagram->type_of_contact_id = 5;
                $user->contact()->save($userInstagram);
            }
            
         }

        if ($request->whatsapp or $request->instagram == null) {
            $deleteWhatsapp = Contact::where('user_id', $userId)->where('type_of_contact_id', 6)->delete();
            if(isset($request->whatsapp))
            {
                $userWhatsapp                    = New Contact;
                $userWhatsapp->contact            = $request->whatsapp;
                $userWhatsapp->type_of_contact_id = 6;
                $user->contact()->save($userWhatsapp);
            }
            
        }

        return redirect()->back()->with("success","Redes Sociales Actualizadas Correctamente");

    }

    public function countComment(){

        // Obtengo todos los inmuebles publicado por el usuario logueado
        $realestates = Realestate::where('user_id', auth()->id())->get();
        // defino el array que contendra los id de los inmuebles que tiene publicado
        $realestatesId = array();
        // lleno el array con los id 
        foreach ($realestates as $realestate) {
            array_push($realestatesId, $realestate->id);
        }
        // Rescato los comentarios correspondientes a esos inmuebles que todavia no  fueron contestados y los cuento
        $commentsCount  = Comment::select('*')
                    ->whereIn('realestate_id', $realestatesId)->where('answered', '=', '0')
                    ->count();
        return $commentsCount;

    }


   

  
    
}
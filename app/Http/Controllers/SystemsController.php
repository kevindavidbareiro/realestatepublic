<?php

namespace App\Http\Controllers;

use App\Http\Requests;
use Illuminate\Http\Request;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use App\System;

class SystemsController extends Controller
{
    
    use AuthenticatesUsers;

    public function __construct(){
        $this->middleware('auth:system');
    }
    
    //
    public function index(){
        $usuarios = System::all();
        return view('backend.master.users')->with('users',$usuarios);
    }

    public function indexSetting(){
        
        return view('backend.master.setting');
    }


    public function getUsers(){
        $roles = System::all();
        return response()->json($roles,200);
    }

    public function update(Request $request){
        $id    = $request->id;
        $user  = $request->user;
        $email =  $request->email;
        $password = $request->password;

        //echo $id." ".$user." ".$email." ".$password;

        $system = System::findOrFail($id);
        $system->name = $user;
        $system->email =$email;
        $system->rol_id =2;
    
        if (!empty($password)) {
            $system->password = Hash::make($password);
        }   

        if($system->save())
        {
            return response()->json(['message'=>'Update user ok','status'=>'ok'],200,[]);
        }else{
            return response()->json(['message'=>'Error user ok','status'=>'error'],200,[]);
        }
    }


    public function guardarUsers(Request $request){
        
        $system = new System;
        $system->name          = $request->user;
        $system->email         = $request->email;
        $system->password      = Hash::make($request->password);
        $system->rol_id        = $request->rol_id;

        if($system->save())
        {
            return response()->json(['message'=>'Update user ok','status'=>'ok'],200,[]);
        }else{
            return response()->json(['message'=>'Error user ok','status'=>'error'],200,[]);
        }
    }
    
}

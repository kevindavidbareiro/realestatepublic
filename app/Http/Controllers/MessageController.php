<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Comment;
use App\Reply;
use App\Realestate;

class MessageController extends Controller
{
    public function index()
    {	
        $countComments = $this->countComment() ;
        // Obtengo todos los inmuebles publicado por el usuario logueado
    	$realestates = Realestate::where('user_id', auth()->id())->get();
    	// defino el array que contendra los id de los inmuebles que tiene publicado
    	$realestatesId = array();
    	// lleno el array con los id 
    	foreach ($realestates as $realestate) {
    		array_push($realestatesId, $realestate->id);
    	}
    	// Rescato los comentarios correspondientes a esos inmuebles
    	$comments  = Comment::select('*')
                    ->whereIn('realestate_id', $realestatesId)
                    ->get();
                
    	return view('frontend.dashboard.message',compact('comments','countComments'));
            
	}

	public function replyStore(Request $request)
	{
		$reply 			   	  = New Reply;
		$reply->reply 	   	  = $request->reply;
		$reply->comment_id 	  = $request->comment_id;
		$reply->user_id    	  = auth()->id();

		if ($reply->save()) {
			$comment 	      = Comment::findOrFail($request->comment_id);
			$comment->answered = 1;
			$comment->save();
			return redirect()->back()
                    ->with('typemsg', 'success')
                    ->with('message', 'Mensaje respondido correctamente. ');
        }
        else
        {
        	  return redirect()->back()
                             ->with('typemsg', 'error')
                             ->with('message', 'Hubo un error al responder el mensaje. Intenta nuevamente.');
        }


	}

    public function countComment(){

        // Obtengo todos los inmuebles publicado por el usuario logueado
        $realestates = Realestate::where('user_id', auth()->id())->get();
        // defino el array que contendra los id de los inmuebles que tiene publicado
        $realestatesId = array();
        // lleno el array con los id 
        foreach ($realestates as $realestate) {
            array_push($realestatesId, $realestate->id);
        }
        // Rescato los comentarios correspondientes a esos inmuebles que todavia no  fueron contestados y los cuento
        $commentsCount  = Comment::select('*')
                    ->whereIn('realestate_id', $realestatesId)->where('answered', '=', '0')
                    ->count();
        return $commentsCount;

    }
}

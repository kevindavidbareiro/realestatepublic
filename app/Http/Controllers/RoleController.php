<?php

namespace App\Http\Controllers;

use App\Http\Requests;
use Illuminate\Http\Request;
use App\Role;

class RoleController extends Controller{

    public function getRoles(){
        $roles = Role::all();
        return response()->json($roles,200);
    }
}
<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Localitie;
class LocalitieController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */

    
    public function index()
    {
        $localites = DB::table('localities')->take(4)
        ->join('provinces','localities.province_id','=','provinces.id')
        ->select('localities.id','localities.name','localities.province_id','provinces.name as province')
        ->orderBy('id', 'desc')
        ->get();

        return response()->json($localites,200);
    }


    public function list(){
        $localites = DB::table('localities')
        ->join('provinces','localities.province_id','=','provinces.id')
        ->select('localities.id','localities.name','provinces.name as province')
        ->orderBy('id', 'desc')
        ->get();
        return response()->json($localites,200);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $localites = new Localitie;
        $localites->name = $request->localities;
        $localites->province_id = $request->idprovincia;

        if($localites->save()){
            return response()->json(['message'=>'Save localities','status'=>'ok'],200,[]);
        }else{
            return response()->json(['message'=>'Error save localities','status'=>'error'],200,[]);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function search(Request $request)
    {
        $localites = DB::table('localities')
        ->join('provinces','localities.province_id','=','provinces.id')
        ->select('localities.id','localities.name','provinces.name as province')
        ->where('localities.name','like','%'.$request->localities.'%')
        ->orderBy('id','desc')
        ->get();

        return response()->json($localites,200);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(Request $request)
    {
        $localites  = Localitie::find($request->idlocalities);
        $localites->name        = $request->localities;
        $localites->province_id = $request->idprovincia;

        if($localites->save()){
            return response()->json(['message'=>'Update localities','status'=>'ok'],200,[]);
        }else{
            return response()->json(['message'=>'Error locaties','status'=>'error'],200,[]);
        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request)
    {
        $localites = Localitie::find($request->idlocalidad);
        if($localites->delete()){
            return response()->json(['message'=>'Delete localities','status'=>'ok'],200,[]);
        }else{
            return response()->json(['message'=>'Error locaties','status'=>'error'],200,[]);
        }
    }
}

<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/



Route::get('/','FrontendController@index');

//Controladores temporales para presentacion
Route::get('/panel', 'AdminController@panel');
Route::get('/reservas', 'AdminController@bookings');
Route::get('/subirPropiedad', 'AdminController@submitProperties');
Route::get('/miPerfil', 'AdminController@myProfile');
Route::get('/listproporties', 'PropertyFilterController@propertyList');



Route::get('/listproporties', 'PropertyFilterController@propertyList');


Route::get('/contacto', 'AdminController@contacto');

Route::resource('/propiedades', 'PropertyController');

Route::resource('Multimedia', 'MultimediaController');
Route::get('/Main', 'MultimediaController@indexMain');
Route::post('/imageMain/{id}/{name}', 'MultimediaController@storeMain');


//ruta de de carga de combos
Route::get('/{id}/get_provinces', 'LocationController@getProvincesByCountry');
Route::get('/{id}/get_localities', 'LocationController@getLocalitiesByProvince');
Route::get('/{id}/get_cities', 'LocationController@getCitiesByLocalitie');



Auth::routes();

// Rutas de usuaruaio para Registrarse, loguearse, desloguearse y editar el perfil
Route::get('/loginUser', 'UserController@login');
Route::post('/login', 'UserController@authenticate' );
Route::get('/logout', 'UserController@logout');
Route::get('/register', 'UserController@showRegistrationForm');
Route::post('/register', 'UserController@register' );
Route::get('/usuario/perfil', 'AdminController@myProfile')->name('user.perfil');;
Route::post('/usuario/perfil', 'AdminController@storeProfile');
Route::post('/usuario/changepass', 'AdminController@changePassword');
Route::post('/usuario/social', 'AdminController@socialNetwork');
// Ruta para procesar los comentarios enviados
Route::post('/propiedades/comment/{propiedad}', 'PropertyController@commentStore');
// Ruta princiipal de los mensajes
Route::get('/mensajes', 'MessageController@index');
Route::post('/mensajes/respuesta', 'MessageController@replyStore');
 //searches
Route::post('/searches-list', 'SearchesController@index')->name('system.searches-list');

//System Groups
Route::prefix('system')->group(function(){
    Route::get('/login','Auth\SystemLoginController@showLoginForm')->name('system.login');
    Route::post('/login','Auth\SystemLoginController@login')->name('system.login.submit');
    Route::post('/system','SystemsController@guardarUsers')->name('system.users.save');
    Route::get('/','SystemsController@index')->name('system.dashboard');
    Route::get('/roles','RoleController@getRoles')->name('system.roles');
    Route::get('/users','SystemsController@getUsers')->name('system.users');
    Route::get('/list-users','SystemsController@index')->name('system.list-users');
    Route::get('/setting','SystemsController@indexSetting')->name('system.setting');
    //country
    Route::get('/country', 'CountryController@index')->name('system.country');
    Route::post('/country', 'CountryController@store')->name('system.country.save');
    Route::put('/country', 'CountryController@edit')->name('system.country.update');
    //cities
    Route::get('/cities', 'CityController@index')->name('system.cities');
    Route::get('/cities/search', 'CityController@search')->name('system.cities.search');
    Route::post('/cities', 'CityController@store')->name('system.cities.save');
    Route::put('/cities', 'CityController@edit')->name('system.cities.update');
    Route::delete('/cities', 'CityController@destroy')->name('system.cities.delete');
    //provinces
    Route::get('/province','ProvinceController@index')->name('system.province');
    Route::get('/province/list','ProvinceController@list')->name('system.province.list');
    Route::post('/province','ProvinceController@store')->name('system.province.save');
    Route::put('/province','ProvinceController@edit')->name('system.province.update');
    Route::delete('/province','ProvinceController@destroy')->name('system.province.delete');
    //localities
    Route::get('/localities','LocalitieController@index')->name('system.localities');
    Route::get('/localities/list','LocalitieController@list')->name('system.localities.list');
    Route::get('/localities/search', 'LocalitieController@search')->name('system.localities.search');
    Route::post('/localities','LocalitieController@store')->name('system.localities.save');
    Route::put('/localities','LocalitieController@edit')->name('system.localities.update');
    Route::delete('/localities','LocalitieController@destroy')->name('system.localities.delete');
    Route::get('/logout','Auth\SystemLoginController@logout')->name('system.logout');
    Route::put('/udpate','SystemsController@update')->name('system.update');
   
});


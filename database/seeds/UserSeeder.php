<?php

use Illuminate\Database\Seeder;

class UserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('users')->insert([
            'name'          => 'Mati',
            'email'         => 'elmati@gmail.com',
            'password'      => bcrypt('mathias'),
            'data'          => 'Usuario de Prueba 1',
        ]);

        DB::table('users')->insert([
            'name'          => 'Alexis',
            'email'         => 'elbuscaminitas@gmail.com',
            'password'      => bcrypt('alexis'),
            'data'          => 'Usuario de Prueba 2',
        ]);

        DB::table('users')->insert([
            'name'          => 'Dani',
            'email'         => 'crak@gmail.com',
            'password'      => bcrypt('dani'),
            'data'          => 'Usuario de Prueba 3',
        ]);
    }
}

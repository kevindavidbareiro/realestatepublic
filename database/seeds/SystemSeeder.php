<?php

use Illuminate\Database\Seeder;

class SystemSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('systems')->insert([
            'name'          => 'Sistema',
            'email'         => 'sistema@gmail.com',
            'password'      => bcrypt('secret'),
            'rol_id'        => 1,
        ]);

        DB::table('systems')->insert([
            'name'          => 'Invitado',
            'email'         => 'invitado@gmail.com',
            'password'      => bcrypt('invitado'),
            'rol_id'        => 2,
        ]);

       
    }
}

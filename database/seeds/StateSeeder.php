<?php

use Illuminate\Database\Seeder;

class StateSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('states')->insert([
            'name'          => 'Venta',   
        ]);

        DB::table('states')->insert([
            'name'          => 'Alquiler',   
        ]);

        DB::table('states')->insert([
            'name'          => 'Alquilado',   
        ]);

        DB::table('states')->insert([
            'name'          => 'Vendido',   
        ]);

       
    }
}

<?php

use Illuminate\Database\Seeder;

class LocalitySeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('localities')->insert([
            'name'          => 'Formosa',
            'province_id'   => '1',
        ]);
        DB::table('localities')->insert([
            'name'          => 'Clorinda',
            'province_id'   => '1',
        ]);
        DB::table('localities')->insert([
            'name'          => 'Resistencia',
            'province_id'   => '2',
        ]);
        DB::table('localities')->insert([
            'name'          => 'Goya',
            'province_id'   => '5',
        ]);
        
    }
}

<?php

use Illuminate\Database\Seeder;

class TypeOfContactSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('type_of_contacts')->insert([
            'name'          => 'Celular',
        ]);

        DB::table('type_of_contacts')->insert([
            'name'          => 'Telefono',
        ]);

        DB::table('type_of_contacts')->insert([
            'name'          => 'Email',
        ]);

        DB::table('type_of_contacts')->insert([
            'name'          => 'Facebook',
        ]);
        DB::table('type_of_contacts')->insert([
            'name'          => 'Instagram',
        ]);
        DB::table('type_of_contacts')->insert([
            'name'          => 'WhatsApp',
        ]);
        
    }
} 
<?php

use Illuminate\Database\Seeder;

class ContactSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('contacts')->insert([
            'contact'           => '3704258596',
            'type_of_contact_id'   => '1',
            'user_id'           => '1',
        ]);

        DB::table('contacts')->insert([
            'contact'           => 'mathi@gmail.com',
            'type_of_contact_id'   => '3',
            'user_id'           => '1',
        ]);

        DB::table('contacts')->insert([
            'contact'           => 'faceboock@Mathias',
            'type_of_contact_id'   => '4',
            'user_id'           => '1',
        ]);

        DB::table('contacts')->insert([
            'contact'           => '3704252356',
            'type_of_contact_id'   => '1',
            'user_id'           => '2',
        ]);

        DB::table('contacts')->insert([
            'contact'           => 'alexis@gmail.com',
            'type_of_contact_id'   => '3',
            'user_id'           => '2',
        ]);

        DB::table('contacts')->insert([
            'contact'           => 'faceboock@Alexis',
            'type_of_contact_id'   => '4',
            'user_id'           => '2',
        ]);

        DB::table('contacts')->insert([
            'contact'           => '3704259847',
            'type_of_contact_id'   => '1',
            'user_id'           => '3',
        ]);

        DB::table('contacts')->insert([
            'contact'           => 'dani@gmail.com',
            'type_of_contact_id'   => '3',
            'user_id'           => '3',
        ]);

        
        
    }
} 
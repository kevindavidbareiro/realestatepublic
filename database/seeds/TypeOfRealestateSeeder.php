<?php

use Illuminate\Database\Seeder;

class TypeOfRealestateSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('type_of_realestates')->insert([
            'name'  => 'Casa',   
        ]);

        DB::table('type_of_realestates')->insert([
            'name'   => 'Dpto',   
        ]);

        DB::table('type_of_realestates')->insert([
            'name'   => 'Local',   
        ]);

        

       
    }
}

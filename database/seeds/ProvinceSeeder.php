<?php

use Illuminate\Database\Seeder;

class ProvinceSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('provinces')->insert([
            'name'          => 'Formosa',
            'country_id'    => 1,
        ]);
        DB::table('provinces')->insert([
            'name'          => 'Chaco',
            'country_id'    => 1,
        ]);
        DB::table('provinces')->insert([
            'name'          => 'Asunción',
            'country_id'    => 2,
        ]);
        DB::table('provinces')->insert([
            'name'          => 'Concepción',
            'country_id'    => 2,
        ]);
        DB::table('provinces')->insert([
            'name'          => 'Corrientes',
            'country_id'    => 1,
        ]);
        DB::table('provinces')->insert([
            'name'          => 'Montevideo',
            'country_id'    => 3,
        ]);
        DB::table('provinces')->insert([
            'name'          => 'Colonia',
            'country_id'    => 3,
        ]);

       

        

       
    }
}

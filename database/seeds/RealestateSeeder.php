<?php

use Illuminate\Database\Seeder;

class RealestateSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('realestates')->insert([
            'name'          => 'Casa de Mati',
            'price'        => 3500000,
            'area'          => 49,
            'bedrooms'      => 2,
            'bathrooms'     => 4,
            'address'        => 'Coronel bogado xxx',
            'country_id'    => 1,
            'province_id'   => 1,
            'locality_id' => 1,
            'city_id'     => 1,
            'user_id'       => 1,
            'state_id'      => 1,
            'type_of_realestate_id' => 1,

        ]);

        DB::table('realestates')->insert([
            'name'          => 'Casa de Mati 2',
            'price'        => 2200000,
            'area'          => 30,
            'bedrooms'      => 1,
            'bathrooms'     => 3,
            'address'        => 'Jujuy xxx',
            'country_id'    => 1,
            'province_id'   => 1,
            'locality_id'      => 2,
            'city_id'     => 1,
            'user_id'       => 1,
            'state_id'      => 2,
            'type_of_realestate_id' => 2,

        ]);

        DB::table('realestates')->insert([
            'name'          => 'Casa de dani',
            'price'        => 7700000,
            'area'          => 80,
            'bedrooms'      => 4,
            'bathrooms'     => 8,
            'address'        => 'Master in the sopa xxx',
            'country_id'    => 2,
            'province_id'   => 1,
            'user_id'       => 3,
            'state_id'      => 1,
            'type_of_realestate_id' => 1,

        ]);

       

       
    }
}

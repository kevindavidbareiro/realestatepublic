<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        // $this->call(UsersTableSeeder::class);
        $this->call(TypeOfContactSeeder::class);
        $this->call(UserSeeder::class);
        $this->call(ContactSeeder::class);
        $this->call(RoleSeeder::class);
        $this->call(SystemSeeder::class);
        $this->call(CountrySeeder::class);
        $this->call(ProvinceSeeder::class);
        $this->call(LocalitySeeder::class);
        $this->call(CitySeeder::class);
        $this->call(StateSeeder::class);
        $this->call(TypeOfRealestateSeeder::class);
        $this->call(OptionSeeder::class);
        $this->call(RealestateSeeder::class);
        $this->call(RealestateOptionSeeder::class);

    }
}

<?php

use Illuminate\Database\Seeder;

class OptionSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('options')->insert([
            'name'    => 'Estacionamiento gratis',//1
        ]);

        DB::table('options')->insert([
            'name'    => 'Estacionamiento pago',//2
        ]);

        DB::table('options')->insert([
            'name'    => 'Aire acondicionado',//3
        ]);

        DB::table('options')->insert([
            'name'    => 'Piscina',//4
        ]);

        DB::table('options')->insert([
            'name'    => 'Cuarto de lavado',//5
        ]);

        DB::table('options')->insert([
            'name'    => 'Calefacción central',//6
        ]);

        DB::table('options')->insert([
            'name'    => 'Lugares para sentarse',//7
        ]);

        DB::table('options')->insert([
            'name'    => 'Mascotas',//8
        ]);

        DB::table('options')->insert([
            'name'    => 'Niños',//9
        ]);
        DB::table('options')->insert([
            'name'    => 'Parrillero',//10
        ]);
        DB::table('options')->insert([
            'name'    => 'Balcon',//11
        ]);
        DB::table('options')->insert([
            'name'    => 'Amoblado',//12
        ]);
        DB::table('options')->insert([
            'name'    => 'No se Permite Fumar',//13
        ]);
        DB::table('options')->insert([
            'name'    => 'Vecinos Amables',//13
        ]);


        
        
        
    }
} 
<?php

use Illuminate\Database\Seeder;

class RealestateOptionSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('realestate_options')->insert([
            'realestate_id'  => 1,
            'option_id'      => 1,
            
        ]);

        DB::table('realestate_options')->insert([
            'realestate_id'  => 1,
            'option_id'      => 3,
            
        ]);

        DB::table('realestate_options')->insert([
            'realestate_id'  => 1,
            'option_id'      => 5,
            
        ]);

        DB::table('realestate_options')->insert([
            'realestate_id'  => 1,
            'option_id'      => 8,
            
        ]);
        DB::table('realestate_options')->insert([
            'realestate_id'  => 1,
            'option_id'      => 9,
            
        ]);

        DB::table('realestate_options')->insert([
            'realestate_id'  => 2,
            'option_id'      => 2,
            
        ]);
        DB::table('realestate_options')->insert([
            'realestate_id'  => 2,
            'option_id'      => 3,
            
        ]);

        DB::table('realestate_options')->insert([
            'realestate_id'  => 2,
            'option_id'      => 9,
            
        ]);

        DB::table('realestate_options')->insert([
            'realestate_id'  => 3,
            'option_id'      => 2,
            
        ]);

        DB::table('realestate_options')->insert([
            'realestate_id'  => 3,
            'option_id'      => 3,
            
        ]);
        DB::table('realestate_options')->insert([
            'realestate_id'  => 3,
            'option_id'      => 4,
            
        ]);

        DB::table('realestate_options')->insert([
            'realestate_id'  => 3,
            'option_id'      => 5,
            
        ]);

        DB::table('realestate_options')->insert([
            'realestate_id'  => 3,
            'option_id'      => 7,
            
        ]);

        
       
    }
}

<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class RealestateOption extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {

        Schema::create('realestate_options', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->biginteger('realestate_id')->unsigned();
            $table->biginteger('option_id')->unsigned();
            $table->timestamps();
            $table->foreign("realestate_id")
                  ->references("id")->on("realestates")
                  ->onDelete("cascade");
            $table->foreign("option_id")
                  ->references("id")->on("options")
                  ->onDelete("cascade");



        });


    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
